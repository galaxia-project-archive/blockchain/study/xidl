/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <vector>

#include "Xi/Serialization/Serializer.hpp"

namespace Xi {
namespace Serialization {

template <typename _ValueT>
Result<void> serialize(std::vector<_ValueT>& value, const Tag& name, Serializer& serializer) {
  size_t size = value.size();
  XI_ERROR_PROPAGATE_CATCH(serializer.beginVector(size, name));
  if (serializer.isInputMode()) {
    value.resize(size);
  }
  for (size_t i = 0; i < size; ++i) {
    XI_ERROR_PROPAGATE_CATCH(serializer(value[i], Tag::Null));
  }
  XI_ERROR_PROPAGATE_CATCH(serializer.endVector());
  XI_SUCCEED()
}

}  // namespace Serialization
}  // namespace Xi
