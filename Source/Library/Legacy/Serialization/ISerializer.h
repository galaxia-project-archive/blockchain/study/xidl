﻿// Copyright (c) 2012-2017, The CryptoNote developers, The Bytecoin developers
//
// This file is part of Bytecoin.
//
// Bytecoin is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bytecoin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Bytecoin.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <string>
#include <cstdint>
#include <optional>

#include <Xi/Byte.hh>
#include <Xi/TypeTrait/IsDetected.hpp>
#include <Xi/Serialization/Legacy/Legacy.hpp>
#include <Common/StringView.h>

#include "Serialization/TypeTag.hpp"

#if !defined(NDEBUG)
#define XI_DEBUG_SERIALIZATION
#endif

namespace CryptoNote {

XI_DECLARE_SMART_POINTER_CLASS(ISerializer)

class ISerializer {
 public:
  enum SerializerType { INPUT, OUTPUT };
  enum FormatType { HumanReadable, Machinery };

  virtual ~ISerializer() {
  }

  virtual SerializerType type() const = 0;
  virtual FormatType format() const = 0;

  [[nodiscard]] virtual bool beginObject(Common::StringView name) = 0;
  [[nodiscard]] virtual bool endObject() = 0;
  [[nodiscard]] virtual bool beginArray(size_t& size, Common::StringView name) = 0;
  [[nodiscard]] virtual bool beginStaticArray(const size_t size, Common::StringView name) = 0;
  [[nodiscard]] virtual bool endArray() = 0;

  [[nodiscard]] virtual bool operator()(uint8_t& value, Common::StringView name) = 0;
  [[nodiscard]] virtual bool operator()(int16_t& value, Common::StringView name) = 0;
  [[nodiscard]] virtual bool operator()(uint16_t& value, Common::StringView name) = 0;
  [[nodiscard]] virtual bool operator()(int32_t& value, Common::StringView name) = 0;
  [[nodiscard]] virtual bool operator()(uint32_t& value, Common::StringView name) = 0;
  [[nodiscard]] virtual bool operator()(int64_t& value, Common::StringView name) = 0;
  [[nodiscard]] virtual bool operator()(uint64_t& value, Common::StringView name) = 0;
  [[nodiscard]] virtual bool operator()(double& value, Common::StringView name) = 0;
  [[nodiscard]] virtual bool operator()(bool& value, Common::StringView name) = 0;
  [[nodiscard]] virtual bool operator()(std::string& value, Common::StringView name) = 0;

  // read/write binary block
  [[nodiscard]] virtual bool binary(void* value, size_t size, Common::StringView name) = 0;
  [[nodiscard]] virtual bool binary(std::string& value, Common::StringView name) = 0;
  [[nodiscard]] virtual bool binary(Xi::ByteVector& value, Common::StringView name) = 0;

  // optionals
  [[nodiscard]] virtual bool maybe(bool& value, Common::StringView name) = 0;

  // variants
  [[nodiscard]] virtual bool typeTag(TypeTag& tag, Common::StringView name) = 0;

  // flags
  [[nodiscard]] virtual bool flag(std::vector<TypeTag>& flag, Common::StringView name) = 0;

  [[nodiscard]] bool isInput() const {
    return type() == INPUT;
  }
  [[nodiscard]] bool isOutput() const {
    return type() == OUTPUT;
  }

  [[nodiscard]] bool isHumanReadable() const {
    return format() == HumanReadable;
  }
  [[nodiscard]] bool isMachinery() const {
    return format() == Machinery;
  }

  template <typename T>
  [[nodiscard]] bool operator()(T& value, Common::StringView name);

  template <typename _T>
  [[nodiscard]] bool optional(bool expected, std::optional<_T>& value, Common::StringView name);
  template <typename _T>
  [[nodiscard]] bool optional(bool expected, std::vector<_T>& value, Common::StringView name);
};

template <typename T>
using cryptonote_serialize_member_expression_t = decltype(std::declval<T&>().serialize(std::declval<ISerializer&>()));
template <typename T>
static inline bool has_cryptonote_serialize_member_expression_v =
    Xi::is_detected_v<cryptonote_serialize_member_expression_t, T>;

template <typename T, std::enable_if_t<has_cryptonote_serialize_member_expression_v<T>, int> = 0>
[[nodiscard]] bool serialize(T& value, Common::StringView name, ISerializer& serializer) {
  XI_RETURN_EC_IF_NOT(serializer.beginObject(name), false);
  XI_RETURN_EC_IF_NOT(serialize(value, serializer), false);
  XI_RETURN_EC_IF_NOT(serializer.endObject(), false);
  return true;
}

// clang-format off
template<
  typename _ValueT,
  std::enable_if_t<
        Xi::Serialization::has_extern_serialization_expression_v<_ValueT>
    ||  Xi::Serialization::has_complex_serialization_expression_v<_ValueT>
  ,int> = 0
>
// clang-format on
[[nodiscard]] inline bool serialize(_ValueT& value, Common::StringView name, CryptoNote::ISerializer& serializer) {
  if (serializer.isInput()) {
    Xi::Serialization::Legacy::InputSerializer input{serializer};
    Xi::Serialization::Serializer wrapper{input};
    const auto ec = wrapper(value, Xi::Serialization::Tag{Xi::Serialization::Tag::Null.binary(),
                                                          std::string{name.getData(), name.getSize()}});
    return !ec.isError();
  } else if (serializer.isOutput()) {
    Xi::Serialization::Legacy::OutputSerializer output{serializer};
    Xi::Serialization::Serializer wrapper{output};
    const auto ec = wrapper(value, Xi::Serialization::Tag{Xi::Serialization::Tag::Null.binary(),
                                                          std::string{name.getData(), name.getSize()}});
    return !ec.isError();
  } else {
    XI_EXCEPTIONAL(Xi::InvalidVariantTypeError)
  }
}

template <typename T>
[[nodiscard]] bool ISerializer::operator()(T& value, Common::StringView name) {
  return serialize(value, name, *this);
}

struct Null {
  /* */
};

template <>
[[nodiscard]] inline bool ISerializer::operator()<Null>(Null&, Common::StringView name) {
  bool hasValue = true;
  XI_RETURN_EC_IF_NOT(maybe(hasValue, name), false);
  return !hasValue;
}

template <typename T>
[[nodiscard]] bool ISerializer::optional(bool expected, std::optional<T>& value, Common::StringView name) {
  if (isInput()) {
    if (expected) {
      value.emplace();
      return (*this)(*value, name);
    } else {
      value = std::nullopt;
      XI_RETURN_SC(true);
    }
  } else if (isOutput()) {
    if (expected) {
      XI_RETURN_EC_IF_NOT(value.has_value(), false);
      return (*this)(*value, name);
    } else {
      XI_RETURN_EC_IF(value.has_value(), false);
      XI_RETURN_SC(true);
    }
  } else {
    XI_RETURN_EC(false);
  }
}

template <typename T>
[[nodiscard]] bool ISerializer::optional(bool expected, std::vector<T>& value, Common::StringView name) {
  if (isInput()) {
    if (expected) {
      return (*this)(value, name);
    } else {
      value.clear();
      XI_RETURN_SC(true);
    }
  } else if (isOutput()) {
    if (expected) {
      XI_RETURN_EC_IF(value.empty(), false);
      return (*this)(value, name);
    } else {
      XI_RETURN_EC_IF_NOT(value.empty(), false);
      XI_RETURN_SC(true);
    }
  } else {
    XI_RETURN_EC(false);
  }
}

template <typename T>
[[nodiscard]] bool serialize(T& value, ISerializer& serializer) {
  return value.serialize(serializer);
}

#ifdef __clang__
template <>
[[nodiscard]] inline bool ISerializer::operator()(size_t& value, Common::StringView name) {
  return operator()(*reinterpret_cast<uint64_t*>(&value), name);
}
#endif

}  // namespace CryptoNote

#define KV_BEGIN_SERIALIZATION [[nodiscard]] bool serialize(::CryptoNote::ISerializer& s) {
#define KV_END_SERIALIZATION \
  return true;               \
  }

#if defined(XI_DEBUG_SERIALIZATION)
#include <iostream>

#define KV_MEMBER(member)                                                                                      \
  if (!s(member, #member)) {                                                                                   \
    std::cout << "[" << __FILE__ << ":" << __LINE__ << "] member serialization failed: " #member << std::endl; \
    return false;                                                                                              \
  }

#define KV_MEMBER_RENAME(MEMBER, NAME)                                                                         \
  if (!s(MEMBER, #NAME)) {                                                                                     \
    std::cout << "[" << __FILE__ << ":" << __LINE__ << "] member serialization failed: " #MEMBER << std::endl; \
    return false;                                                                                              \
  }

#define KV_BASE(BASE_CLASS)                                                                                 \
  if (!this->BASE_CLASS::serialize(s)) {                                                                    \
    std::cout << "[" << __FILE__ << ":" << __LINE__ << "] base class serialization failed: " << #BASE_CLASS \
              << std::endl;                                                                                 \
    return false;                                                                                           \
  }

#define KV_PUSH_VIRTUAL_OBJECT(NAME)                                                                           \
  if (!s.beginObject(#NAME)) {                                                                                 \
    std::cout << "[" << __FILE__ << ":" << __LINE__ << "] virtual object push serialization failed: " << #NAME \
              << std::endl;                                                                                    \
    return false;                                                                                              \
  }

#define KV_POP_VIRTUAL_OBJECT()                                                                                   \
  if (!s.endObject()) {                                                                                           \
    std::cout << "[" << __FILE__ << ":" << __LINE__ << "] virtual object pop serialization failed." << std::endl; \
    return false;                                                                                                 \
  }
#else
#define KV_MEMBER(member)    \
  if (!s(member, #member)) { \
    return false;            \
  }

#define KV_MEMBER_RENAME(MEMBER, NAME) \
  if (!s(MEMBER, #NAME)) {             \
    return false;                      \
  }

#define KV_BASE(BASE_CLASS)              \
  if (!this->BASE_CLASS::serialize(s)) { \
    return false;                        \
  }

#define KV_PUSH_VIRTUAL_OBJECT(NAME) \
  if (!s.beginObject(#NAME)) {       \
    return false;                    \
  }

#define KV_POP_VIRTUAL_OBJECT() \
  if (!s.endObject()) {         \
    return false;               \
  }

#endif
