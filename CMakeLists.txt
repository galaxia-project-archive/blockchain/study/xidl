﻿ # ============================================================================================== #
 #                                                                                                #
 #                                     Galaxia Blockchain                                         #
 #                                                                                                #
 # ---------------------------------------------------------------------------------------------- #
 # This file is part of the Xi framework.                                                         #
 # ---------------------------------------------------------------------------------------------- #
 #                                                                                                #
 # Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
 #                                                                                                #
 # This program is free software: you can redistribute it and/or modify it under the terms of the #
 # GNU General Public License as published by the Free Software Foundation, either version 3 of   #
 # the License, or (at your option) any later version.                                            #
 #                                                                                                #
 # This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
 # without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
 # See the GNU General Public License for more details.                                           #
 #                                                                                                #
 # You should have received a copy of the GNU General Public License along with this program.     #
 # If not, see <https://www.gnu.org/licenses/>.                                                   #
 #                                                                                                #
 # ============================================================================================== #

cmake_minimum_required(VERSION 3.14.0)

include(CMake/Xi/Xi.cmake)

cmake_policy(SET CMP0048 NEW)
project(
  ${XI_PROJECT_NAME}
  
  DESCRIPTION
    ${XI_PROJECT_DESCRIPTION}

  HOMEPAGE_URL
    ${XI_PROJECT_HOMEPAGE}

  VERSION
    ${XI_PROJECT_VERSION}

  LANGUAGES 
    C 
    CXX
)

xi_include(
  GLOB_RECURSE
    Resource/*
    Source/Application/*
    Source/Library/*
    Source/Tool/*
)
