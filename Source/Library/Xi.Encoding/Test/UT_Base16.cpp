/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>
#include <Xi/Encoding/Base16.hh>

#include <string>

#define XI_TESTSUITE T_Xi_Encoding_Base16

namespace {
const std::vector<std::pair<std::string, std::string>> Oracle = {{
    {"", ""},
    {"f", "66"},
    {"fo", "666F"},
    {"foo", "666F6F"},
    {"foob", "666F6F62"},
    {"fooba", "666F6F6261"},
    {"foobar", "666F6F626172"},
}};
}  // namespace

TEST(XI_TESTSUITE, Encode) {
  using namespace ::testing;
  using namespace Xi::Encoding::Base16;

  for (const auto& oracle : Oracle) {
    const auto encoded = encode(Xi::asConstByteSpan(oracle.first));
    EXPECT_EQ(encoded, oracle.second);
  }
}

TEST(XI_TESTSUITE, Decode) {
  using namespace ::testing;
  using namespace Xi::Encoding::Base16;

  for (const auto& oracle : Oracle) {
    const auto decoded = decode(oracle.second);
    ASSERT_FALSE(decoded.isError());
    const std::string decodedStr{reinterpret_cast<const char*>(decoded->data()), decoded->size()};
    EXPECT_EQ(decodedStr, oracle.first);
  }
}
