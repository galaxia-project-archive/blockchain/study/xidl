/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string>

#include <Xi/Global.hh>
#include <Xi/Serialization/Serialization.hpp>

namespace Xi {
namespace VersionInfo {

class Compiler {
 public:
  static const Compiler& project();

 public:
  Compiler() = default;
  XI_DEFAULT_COPY(Compiler);
  XI_DEFAULT_MOVE(Compiler);
  ~Compiler() = default;

  const std::string& platform() const;
  const std::string& endianess() const;

  const std::string& cFlags() const;
  const std::string& cIdentifier() const;
  const std::string& cVersion() const;

  const std::string& cxxFlags() const;
  const std::string& cxxIdentifier() const;
  const std::string& cxxVersion() const;

  XI_SERIALIZATION_COMPLEX_EXTERN()

 private:
  friend class CompilerBuilder;

 private:
  std::string m_platform;
  std::string m_endianess;

  std::string m_cFlags;
  std::string m_cIdentifier;
  std::string m_cVersion;

  std::string m_cxxFlags;
  std::string m_cxxIdentifier;
  std::string m_cxxVersion;
};

class CompilerBuilder {
 public:
  CompilerBuilder() = default;
  XI_DELETE_COPY(CompilerBuilder);
  XI_DELETE_MOVE(CompilerBuilder);
  ~CompilerBuilder() = default;

  CompilerBuilder& withPlatform(const std::string& platform);
  CompilerBuilder& withEndianess(const std::string& endianess);

  CompilerBuilder& withCFlags(const std::string& flags);
  CompilerBuilder& withCIdentifier(const std::string& identifier);
  CompilerBuilder& withCVersion(const std::string& version);

  CompilerBuilder& withCxxFlags(const std::string& flags);
  CompilerBuilder& withCxxIdentifier(const std::string& identifier);
  CompilerBuilder& withCxxVersion(const std::string& version);

  Compiler build() const;

 private:
  Compiler m_info;
};

}  // namespace VersionInfo
}  // namespace Xi
