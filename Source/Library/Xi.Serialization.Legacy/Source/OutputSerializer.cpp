/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Serialization/Legacy/OutputSerializer.hpp"

#include <Serialization/ISerializer.h>

#include "Xi/Serialization/Legacy/LegacyError.hpp"

namespace Xi {
namespace Serialization {
namespace Legacy {

OutputSerializer::OutputSerializer(CryptoNote::ISerializer &out) : m_serializer{out} {
  /* */
}

Format OutputSerializer::format() const {
  return m_serializer.isHumanReadable() ? Format::HumanReadable : Format::Binary;
}

Result<void> OutputSerializer::writeInt8(int8_t, const Tag &) {
  XI_FAIL(LegacyError::Unsupported);
}

Result<void> OutputSerializer::writeUInt8(uint8_t value, const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer(value, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> OutputSerializer::writeInt16(int16_t value, const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer(value, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> OutputSerializer::writeUInt16(uint16_t value, const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer(value, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> OutputSerializer::writeInt32(int32_t value, const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer(value, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> OutputSerializer::writeUInt32(uint32_t value, const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer(value, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> OutputSerializer::writeInt64(int64_t value, const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer(value, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> OutputSerializer::writeUInt64(uint64_t value, const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer(value, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> OutputSerializer::writeBoolean(bool value, const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer(value, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> OutputSerializer::writeFloat(float, const Tag &) {
  XI_FAIL(LegacyError::Unsupported);
}

Result<void> OutputSerializer::writeDouble(double value, const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer(value, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> OutputSerializer::writeString(const std::string_view value, const Tag &nameTag) {
  std::string str{value.data(), value.size()};
  XI_FAIL_IF_NOT(m_serializer(str, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> OutputSerializer::writeBinary(ConstByteSpan value, const Tag &nameTag) {
  size_t size = value.size();
  XI_FAIL_IF_NOT(m_serializer(size, nameTag.text()), LegacyError::Internal);
  XI_FAIL_IF_NOT(m_serializer.binary(const_cast<Byte *>(value.data()), value.size(), nameTag.text()),
                 LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> OutputSerializer::writeBlob(ConstByteSpan value, const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer.binary(const_cast<Byte *>(value.data()), value.size(), nameTag.text()),
                 LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> OutputSerializer::writeTag(const Tag &value, const Tag &nameTag) {
  CryptoNote::TypeTag tag{value.binary(), value.text()};
  XI_FAIL_IF_NOT(m_serializer.typeTag(tag, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> OutputSerializer::writeFlag(const TagVector &tag, const Tag &nameTag) {
  std::vector<CryptoNote::TypeTag> tags{};
  tags.reserve(tag.size());
  for (const auto &iTag : tag) {
    tags.emplace_back(iTag.binary(), iTag.text());
  }
  XI_FAIL_IF_NOT(m_serializer.flag(tags, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> OutputSerializer::beginWriteComplex(const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer.beginObject(nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> OutputSerializer::endWriteComplex() {
  XI_FAIL_IF_NOT(m_serializer.endObject(), LegacyError::Internal);
  XI_SUCCEED()
}

Result<void> OutputSerializer::beginWriteVector(size_t size, const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer.beginArray(size, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED()
}

Result<void> OutputSerializer::endWriteVector() {
  XI_FAIL_IF_NOT(m_serializer.endArray(), LegacyError::Internal);
  XI_SUCCEED()
}

Result<void> OutputSerializer::beginWriteArray(size_t size, const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer.beginStaticArray(size, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED()
}

Result<void> OutputSerializer::endWriteArray() {
  XI_FAIL_IF_NOT(m_serializer.endArray(), LegacyError::Internal);
  XI_SUCCEED()
}

Result<void> OutputSerializer::writeNull(const Tag &nameTag) {
  bool maybe = false;
  XI_FAIL_IF_NOT(m_serializer.maybe(maybe, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> OutputSerializer::writeNotNull(const Tag &nameTag) {
  bool maybe = true;
  XI_FAIL_IF_NOT(m_serializer.maybe(maybe, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

}  // namespace Legacy
}  // namespace Serialization
}  // namespace Xi
