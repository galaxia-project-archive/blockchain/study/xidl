# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_SOURCE_APPLICATION_XI_BENCHMARK)
  return()
endif()
set(CMAKE_XI_SOURCE_APPLICATION_XI_BENCHMARK TRUE)

xi_include(Xi/Make/Application)
xi_include(Package/CxxOpts)
xi_include(Extern/CpuFeatures)
xi_include(Source/Library/Xi/App)
xi_include(Source/Library/Legacy/Serialization)
xi_include(Source/Library/Xi/ProofOfWork)

xi_make_application(
  Xi.Benchmark

  LIBRARIES
    Xi::App

    Legacy::Serialization

    cxxopts::cxxopts
    cpufeatures::cpufeatures
)
