/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>
#include <Xi/TypeTrait/IsDetected.hpp>

namespace Xi {
namespace Serialization {

class Serializer;
class Tag;

template <typename _ValueT, typename... _Ts>
using extern_serialization_expression_t = decltype(serialize(std::declval<_ValueT &>(), std::declval<const Tag &>(),
                                                             std::declval<Serializer &>(), std::declval<_Ts>()...));
template <typename _ValueT, typename... _Ts>
static inline constexpr bool has_extern_serialization_expression_v =
    is_detected_v<extern_serialization_expression_t, _ValueT, _Ts...>;

template <typename _ValueT, typename... _Ts>
using complex_serialization_expression_t =
    decltype(std::declval<_ValueT>().serialize(std::declval<Serializer &>(), std::declval<_Ts>()...));
template <typename _ValueT, typename... _Ts>
static inline constexpr bool has_complex_serialization_expression_v =
    is_detected_v<complex_serialization_expression_t, _ValueT, _Ts...>;

}  // namespace Serialization
}  // namespace Xi
