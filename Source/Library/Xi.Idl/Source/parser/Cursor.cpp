/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Parser/Cursor.hpp"

#include <sstream>
#include <utility>
#include <cassert>

namespace Xi {
namespace Idl {
namespace Parser {

Cursor::Cursor(Stream::InputStream &stream) : m_stream{stream}, m_pos{0, 0} {
  /* */
}

bool Cursor::isVoid(const char ch) const {
  switch (ch) {
    case '\n':
    case '\r':
    case '\t':
    case ' ':
      return true;

    default:
      return false;
  }
}

bool Cursor::isSymbol(const char ch) const {
  switch (ch) {
    case '#':
    case '(':
    case ')':
    case '{':
    case '}':
    case '[':
    case ']':
    case '.':
    case '+':
    case '*':
    case ';':
    case ',':
    case '=':
    case '?':
    case '@':
    case '%':
    case '!':
    case '<':
    case '>':
    case '\'':
      return true;

    default:
      return false;
  }
}

bool Cursor::isMultilineSymbol(const char ch) const {
  switch (ch) {
    case '<':
    case '>':
      return true;

    default:
      return false;
  }
}

Result<Token> Cursor::readToken() {
  auto isEOS = m_stream.isEndOfStream();
  XI_ERROR_PROPAGATE(isEOS)
  if (*isEOS) {
    XI_SUCCEED(makeEndOfStreamToken(currentPosition()));
  }

  Token reval{"", currentPosition(), currentPosition()};
  do {
    auto next = m_stream.peek();
    XI_ERROR_PROPAGATE(next)
    auto cnext = static_cast<char>(*next);

    if (!reval.isEmpty()) {
      const auto ccurrent = reval.value.back();
      if (isSymbol(ccurrent) != isSymbol(cnext)) {
        break;
      } else if (isSymbol(ccurrent) || isSymbol(cnext)) {
        if (reval.value.size() > 1) {
          break;
        } else if (ccurrent == cnext) {
          break;
        } else if (ccurrent != '<' && cnext != '>') {
          break;
        }
      } else if (Token::isSpace(ccurrent) != Token::isSpace(cnext)) {
        break;
      } else if (Token::isNewLine(ccurrent) != Token::isNewLine(cnext)) {
        break;
      }
    }

    reval.value.push_back(cnext);
    XI_ERROR_PROPAGATE_CATCH(m_stream.take())
    if (Token::isNewLine(cnext)) {
      newLine();
    } else {
      advanceColumn();
    }
    isEOS = m_stream.isEndOfStream();
    XI_ERROR_PROPAGATE(isEOS)
  } while (!*isEOS);

  reval.end = currentPosition();
  XI_SUCCEED(std::move(reval))
}

void Cursor::newLine() {
  currentPosition().line += 1;
  resetLine();
}

void Cursor::resetLine() {
  currentPosition().column = 0;
}

void Cursor::advanceColumn(size_t offset) {
  currentPosition().column += offset;
}

const SourcePosition &Cursor::currentPosition() const {
  return m_pos;
}

SourcePosition &Cursor::currentPosition() {
  return m_pos;
}

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi
