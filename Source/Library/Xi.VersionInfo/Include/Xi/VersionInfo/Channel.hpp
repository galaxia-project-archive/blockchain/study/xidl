/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string>

#include <Xi/Result.hpp>
#include <Xi/ErrorCode.hpp>
#include <Xi/Serialization/Enum.hpp>

namespace Xi {
namespace VersionInfo {

XI_ERROR_CODE_BEGIN(Channel)
XI_ERROR_CODE_VALUE(Unknown, 0x0001)
XI_ERROR_CODE_END(Channel, "ChannelError")

enum struct Channel {
  Stable = 0x0001,
  Beta = 0x0002,
  Edge = 0x0003,
  Clutter = 0x0004,
};

XI_SERIALIZATION_ENUM(Channel)

std::string stringify(const Channel channel);
Result<Channel> fromString(const std::string& str);

}  // namespace VersionInfo
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::VersionInfo, Channel)

XI_SERIALIZATION_ENUM_RANGE(Xi::VersionInfo::Channel, Stable, Clutter)
XI_SERIALIZATION_ENUM_TAG(Xi::VersionInfo::Channel, Stable, "stable")
XI_SERIALIZATION_ENUM_TAG(Xi::VersionInfo::Channel, Beta, "beta")
XI_SERIALIZATION_ENUM_TAG(Xi::VersionInfo::Channel, Edge, "edge")
XI_SERIALIZATION_ENUM_TAG(Xi::VersionInfo::Channel, Clutter, "clutter")
