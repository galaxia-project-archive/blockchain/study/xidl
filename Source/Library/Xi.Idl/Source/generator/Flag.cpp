/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/Flag.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

const std::string &Flag::name() const {
  return m_name;
}

SharedConstNamespace Flag::namespace_() const {
  return m_namespace;
}

const Documentation &Flag::documentation() const {
  return m_doc;
}

const std::vector<FlagEntry> Flag::entries() const {
  return m_entries;
}

void Flag::setName(const std::string &name_) {
  m_name = name_;
}

void Flag::setNamespace(SharedConstNamespace ns) {
  m_namespace = ns;
}

void Flag::setDocumentation(const Documentation &doc_) {
  m_doc = doc_;
}

std::vector<FlagEntry> &Flag::entries() {
  return m_entries;
}

FlagBuilder::FlagBuilder() : m_flag{result_success, Flag{}} {
  /* */
}

FlagBuilder &FlagBuilder::withName(const std::string &name) {
  if (!m_flag.isError()) {
    m_flag->setName(name);
  }
  return *this;
}

FlagBuilder &FlagBuilder::withNamespace(SharedConstNamespace ns) {
  if (!m_flag.isError()) {
    m_flag->setNamespace(ns);
  }
  return *this;
}

FlagBuilder &FlagBuilder::withDocumentation(const Documentation &doc) {
  if (!m_flag.isError()) {
    m_flag->setDocumentation(doc);
  }
  return *this;
}

FlagBuilder &FlagBuilder::withEntry(const FlagEntry &entry) {
  if (!m_flag.isError()) {
    m_flag->entries().emplace_back(entry);
  }
  return *this;
}

Result<Flag> FlagBuilder::build() {
  return m_flag;
}

Result<Type> makeType(const Flag &flag) {
  return TypeBuilder{}
      .withKind(Type::Kind::Flag)
      .withName(flag.name())
      .withNamespace(flag.namespace_())
      .withIsOptional(false)
      .withData(flag)
      .build();
}

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi
