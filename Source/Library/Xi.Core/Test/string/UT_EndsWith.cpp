/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/String/EndsWith.hpp>

#define XI_TEST_SUITE Xi_Algorithm_String_EndsWith

TEST(XI_TEST_SUITE, EmptyCases) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_TRUE(endsWith("", ""));
  EXPECT_TRUE(endsWith("X", ""));
  EXPECT_FALSE(endsWith("", "C"));
}

TEST(XI_TEST_SUITE, Match) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_TRUE(endsWith("__^", "^"));
  EXPECT_TRUE(endsWith("__^", "_^"));
  EXPECT_TRUE(endsWith("__^", "__^"));
}

TEST(XI_TEST_SUITE, NoMatch) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_FALSE(endsWith("__^", "_"));
  EXPECT_FALSE(endsWith("__^", "^_"));
  EXPECT_FALSE(endsWith("__^", "_^_"));
}
