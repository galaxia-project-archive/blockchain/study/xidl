# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_COMPILER_MSVC)
    return()
endif()
set(CMAKE_XI_COMPILER_MSVC TRUE)

xi_include(Xi/Log)
xi_include(Xi/Option/TreatWarningAsError)
xi_include(Xi/Option/Breakpad)
xi_include(Xi/Compiler/Flag/Add)
xi_include(Xi/Compiler/Check)
xi_include(Xi/Compiler/MSVC/Sdk)
xi_include(Xi/Compiler/MSVC/Parallel)

xi_compiler_check(
  REQUIRED 19.10
  UPGRADE 19.15
  RECOMMENDED 19.23
)

xi_compiler_flag_add("/FS")
xi_compiler_flag_add("/W4")
xi_compiler_flag_add("/D_CRT_SECURE_NO_WARNINGS")

# Required by the legacy logging interface
# REPLACE
xi_compiler_flag_add("/wd4239")

if(XI_TREAT_WARNING_AS_ERROR)
  xi_compiler_flag_add("/WX")
endif()

if(XI_BUILD_BREAKPAD)
  if(CMAKE_BUILD_TYPE MATCHES RELEASE OR CMAKE_BUILD_TYPE MATCHES MINSIZEREL)
    xi_compiler_flag_add("/DEBUG")
  endif()
endif()
