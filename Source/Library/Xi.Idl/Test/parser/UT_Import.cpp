/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/Testing/Result.hpp>
#include <Xi/Idl/Parser/Import.hpp>

#define XI_TEST_SUITE Xi_Idl_Parser_Import

class XI_TEST_SUITE : public ::testing::Test {
 public:
  Xi::Idl::Parser::Import import;
  std::shared_ptr<Xi::Idl::Parser::ImportAutomaton> automaton;

  XI_TEST_SUITE() : import{}, automaton{nullptr} {
    /* */
  }

  void process(const std::string& content) {
    import = Xi::Idl::Parser::Import{/* */};
    automaton = Xi::Idl::Parser::makeImportAutomaton(import);
    auto ec = automaton->process(content);
    ASSERT_THAT(ec, Xi::Testing::IsSuccess());
  }
};

TEST_F(XI_TEST_SUITE, WellFormed) {
  process("import '../Crypto/Hash.xidl';");
  EXPECT_EQ(import.filepath, "../Crypto/Hash.xidl");
  EXPECT_EQ(import.alias, "");

  process("import '$( )?\t\n,Crypto.xidl  ' as Crypto;");
  EXPECT_EQ(import.filepath, "$( )?\t\n,Crypto.xidl  ");
  EXPECT_EQ(import.alias, "Crypto");
}
