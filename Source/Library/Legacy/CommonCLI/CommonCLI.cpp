﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "CommonCLI.h"

#include <sstream>
#include <iostream>

#include <Xi/Extern/Push.hh>
#include <fmt/format.h>
#include <rang.hpp>
#include <Xi/Extern/Pop.hh>

#include <Xi/Log/Log.hpp>
#include <Xi/VersionInfo/VersionInfo.hpp>
#include <Xi/Resource/Resource.hpp>
#include <Xi/CrashHandler/Config.hpp>
#include <Xi/CrashHandler/CrashHandler.hpp>
#include <Xi/CrashHandler/Config/Config.hh>
#include <Xi/Serialization/Console/Console.hpp>
#include <Resource/License/License.hpp>

#include <Common/Util.h>

XI_LOGGER("Cli")

namespace {
Xi::CrashHandler::Config BreakpadConfig;
}

void CommonCLI::printShortVersion(std::ostream& out) {
  const auto& versionInfo = Xi::VersionInfo::VersionInfo::project();
  out << rang::fg::cyan << rang::style::underline << " Xi" << rang::fg::reset;
  out << " - ";
  out << (isDevVersion() ? rang::fg::yellow : rang::fg::green);
  out << fmt::format("v{}", versionInfo.version());
  out << rang::fg::reset << "\n";
}

void CommonCLI::printSoftwareHeader() {
  printShortVersion(std::cout);
  std::cout << "\tThis software is distributed under the General Public License v3.0\n";
  std::cout << "\tAdditional Copyright(s) may apply, please see the included LICENSE file for more information.\n";
  std::cout << std::endl;

  if (isDevVersion()) {
    printDevWarning();
  }
}

bool CommonCLI::isDevVersion() {
  using namespace Xi::VersionInfo;
  const auto channel = Version::project().channel();
  if (!channel.has_value()) {
    return true;
  }
  return !(*channel == Channel::Stable || *channel == Channel::Beta);
}

void CommonCLI::printDevWarning() {
  std::cout << rang::fg::yellow;
  std::cout << R"(
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!   ATTENTION   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   !                                                                                       !
   ! You are running a development version! The program may contain bugs or is not         !
   ! compatible with the main network. In case you accidentally ran into this version and  !
   ! do not want to use it to for testing purposes you should visit our GitLab page        !
   !     https://gitlab.com/galaxia-project/blockchain                                     !
   ! . Or if you want to build xi yourself make sure you are building from the master      !
   ! branch.                                                                               !
   !                                                                                       !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!   ATTENTION   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
)";
  std::cout << rang::fg::reset << std::endl;
}

void CommonCLI::printInsecureClientWarning() {
  std::cout << rang::fg::yellow;
  std::cout << R"(
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!   WARNING   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   !                                                                                       !
   ! Your client is not set up to be the most secure. To increase your security please     !
   ! read our tutorial on setting up SSL correctly.                                        !
   !     https://doc.galaxia-project.com/ssl/                                              !
   !                                                                                       !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!   WARNING   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
)";
  std::cout << rang::fg::reset << std::endl;
}

void CommonCLI::printInsecureServerWarning() {
  std::cout << rang::fg::yellow;
  std::cout << R"(
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!   WARNING   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   !                                                                                       !
   ! Your server is not set up to be the most secure. To increase your security please     !
   ! read our tutorial on setting up SSL correctly.                                        !
   !     https://doc.galaxia-project.com/ssl/                                              !
   !                                                                                       !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!   WARNING   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
)";
  std::cout << rang::fg::reset << std::endl;
}

void CommonCLI::emplaceCLIOptions(cxxopts::Options& options) {
  // clang-format off
  options.add_options("general")
      ("help", "Display this help message")
      ("version", "Output software version information")
      ("vversion", "Output verbose software version information")
      ("osversion", "Output Operating System version information");

  if constexpr(Xi::CrashHandler::is_enabled) {
    options.add_options("breakpad")
        ("breakpad-enable", "Enables creation of crash dumps to help developers reconstruct bugs occuring in release builds.",
         cxxopts::value<bool>(BreakpadConfig.IsEnabled)->implicit_value("true")->default_value("false"))

        ("breakpad-out", "Output directory for storing crash dumps",
         cxxopts::value<std::string>(BreakpadConfig.OutputPath)->default_value(BreakpadConfig.OutputPath))

        ("breakpad-server", "breakpad server for uploading crash dumps",
         cxxopts::value<std::string>(BreakpadConfig.Server)->default_value(BreakpadConfig.Server))

        ("breakpad-upload", "Enables auto upload of crash dumps to the galaxia project breakpad server.",
         cxxopts::value<bool>(BreakpadConfig.IsUploadEnabled)->implicit_value("true")->default_value("false"));
  } else {
    XI_UNUSED(BreakpadConfig);
  }

  options.add_options("license")
      ("license", "Print the project license and exits.")
      ("third-party","Prints a summary of all third party libraries used by this project.")
      ("third-party-licenses", "Prints every license included by third party libraries used by this project.");

  if(isDevVersion()) {
    options.add_options("development")("dev-mode", "Indicates you are aware of running a development version. "
                                                   "You must provide this flag in order to run the application.");
  }
  // clang-format on
}

bool CommonCLI::handleCLIOptions(const cxxopts::Options& options, const cxxopts::ParseResult& result) {
  using Xi::Resource::embedded;
  using Xi::Serialization::toConsole;
  const auto& versionInfo = Xi::VersionInfo::VersionInfo::project();

  if (result.count("help")) {
    std::cout << options.help({}) << std::endl;
    return true;
  } else if (result.count("version")) {
    std::cout << fmt::format("v{}", versionInfo.version()) << std::endl;
    return true;
  } else if (result.count("vversion")) {
    const auto ec = toConsole(versionInfo);
    XI_ERROR_IF(ec.isError(), "Error on yaml conversion '{}'.", ec.error().message())
    std::cout << std::endl;
    return true;
  } else if (result.count("license")) {
    Resource::License::loadLicense();
    std::cout << embedded("xrc://license/Framework").takeOrThrow().text() << std::endl;
    return true;
  } else if (result.count("third-party")) {
    Resource::License::loadLicense();
    std::cout << embedded("xrc://license/ThridParty").takeOrThrow().text() << std::endl;
    return true;
  } else if (result.count("third-party-licenses")) {
    Resource::License::loadLicense();
    std::cout << embedded("xrc://license/ThridParty.Verbose").takeOrThrow().text() << std::endl;
    return true;
  } else if (result.count("osversion")) {
    std::cout << Tools::get_os_version_string() << std::endl;
    return true;
  } else if (isDevVersion() && (result.count("dev-mode") == 0)) {
    std::cout << "\n You are using a development version and did not provide the --dev-mode flag. Exiting..."
              << std::endl;
    return true;
  } else {
    return false;
  }
}

void* CommonCLI::make_crash_dumper(const std::string& applicationId) {
  BreakpadConfig.Application = applicationId;
  if (BreakpadConfig.IsEnabled) {
    return new Xi::CrashHandler::CrashHandler{BreakpadConfig};
  } else {
    return nullptr;
  }
}
