/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/Testing/Result.hpp>
#include <Xi/Idl/Parser/Documentation.hpp>

#define XI_TEST_SUITE Xi_Idl_Parser_Documentation

class XI_TEST_SUITE : public ::testing::Test {
 public:
  Xi::Idl::Parser::Documentation doc;
  std::shared_ptr<Xi::Idl::Parser::DocumentationAutomaton> automaton;

  XI_TEST_SUITE() : doc{}, automaton{nullptr} {
    /* */
  }

  void process(const std::string& content) {
    doc = Xi::Idl::Parser::Documentation{/* */};
    automaton = Xi::Idl::Parser::makeDocumentationAutomaton(doc);
    auto ec = automaton->process(content);
    ASSERT_THAT(ec, Xi::Testing::IsSuccess());
  }
};

TEST_F(XI_TEST_SUITE, SingleLineBriefShorthand) {
  using namespace Xi::Idl::Parser;
  using namespace ::testing;

  process(R"__(
@ My brief doc.
)__");

  ASSERT_THAT(doc, SizeIs(Eq(1)));
  const auto& none = doc[0];
  ASSERT_TRUE(std::holds_alternative<NoDocumentationTag>(none.tag));
  EXPECT_EQ(none.content, "My brief doc.");
}

TEST_F(XI_TEST_SUITE, SingleLineBrief) {
  using namespace Xi::Idl::Parser;
  using namespace ::testing;

  process(R"__(
@brief My brief doc.
)__");

  ASSERT_THAT(doc, SizeIs(Eq(1)));
  const auto& brief = doc[0];
  ASSERT_TRUE(std::holds_alternative<BriefDocumentationTag>(brief.tag));
  EXPECT_EQ(brief.content, "My brief doc.");
}

TEST_F(XI_TEST_SUITE, SingleLineExample) {
  using namespace Xi::Idl::Parser;
  using namespace ::testing;

  process(R"__(
@example F67FD21BD9A9266243865B478013D37F
)__");

  ASSERT_THAT(doc, SizeIs(Eq(1)));
  const auto& example = doc[0];
  ASSERT_TRUE(std::holds_alternative<ExampleDocumentationTag>(example.tag));
  EXPECT_EQ(example.content, "F67FD21BD9A9266243865B478013D37F");
}

TEST_F(XI_TEST_SUITE, Multiline) {
  using namespace Xi::Idl::Parser;
  using namespace ::testing;

  process(R"__(
<@
    I may have no tag.

    @example F67FD21BD9A9266243865B478013D37F

    @detailed

    This discription
    may
    take
    multiple lines.
@>
)__");

  ASSERT_THAT(doc, SizeIs(Eq(3)));
  {
    const auto& entry = doc[0];
    ASSERT_TRUE(std::holds_alternative<NoDocumentationTag>(entry.tag));
    EXPECT_EQ(entry.content, "I may have no tag.");
  }
  {
    const auto& entry = doc[1];
    ASSERT_TRUE(std::holds_alternative<ExampleDocumentationTag>(entry.tag));
    EXPECT_EQ(entry.content, "F67FD21BD9A9266243865B478013D37F");
  }
  {
    const auto& entry = doc[2];
    ASSERT_TRUE(std::holds_alternative<DetailedDocumentationTag>(entry.tag));
    EXPECT_EQ(entry.content, "This discription may take multiple lines.");
  }
}
