/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string>

#include "Xi/Idl/Parser/Automaton.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

class KeywordAutomaton final : public Automaton {
 public:
  std::string result;
  std::string expected;

 private:
  KeywordAutomaton() = default;
  friend std::shared_ptr<KeywordAutomaton> makeKeywordAutomaton(const std::string&);

 protected:
  Result<void> doProcessToken(const Token& token) override;
};

std::shared_ptr<KeywordAutomaton> makeKeywordAutomaton(const std::string& expected = "");

class OptionalKeywordAutomaton final : public ResultAutomaton<bool> {
 private:
  OptionalKeywordAutomaton(const std::string& key, bool& res);
  friend std::shared_ptr<OptionalKeywordAutomaton> makeOptionalKeywordAutomaton(const std::string&, bool&);

 protected:
  Result<void> doProcessToken(const Token& token) override;

 private:
  std::string m_identicator;
};

std::shared_ptr<OptionalKeywordAutomaton> makeOptionalKeywordAutomaton(const std::string& id, bool& res);

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi
