/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Serialization/Binary/OutputSerializer.hpp"

#include <limits>

#include <Xi/Global.hh>
#include <Xi/Endianess/Endianess.hh>
#include <Xi/Encoding/VarInt.hh>

#include "Xi/Serialization/Binary/BinaryError.hpp"

namespace Xi {
namespace Serialization {
namespace Binary {

namespace {
template <typename _IntT>
Result<void> writeVarInt(_IntT value, Stream::OutputStream& stream) {
  ByteArray<Encoding::VarInt::maximumEncodingSize<_IntT>()> buffer;
  const auto encodingSize = Encoding::VarInt::encode(value, buffer);
  XI_ERROR_PROPAGATE(encodingSize)
  XI_ERROR_PROPAGATE_CATCH(stream.writeStrict(ConstByteSpan{buffer.data(), *encodingSize}));
  XI_SUCCEED()
}
template <typename _IntT>
Result<void> writeInt(_IntT value, Stream::OutputStream& stream, const Options& options) {
  value = Endianess::convert(value, options.endianess);
  XI_ERROR_PROPAGATE_CATCH(stream.writeStrict(asByteSpan(&value, sizeof(_IntT))));
  XI_SUCCEED()
}
}  // namespace

OutputSerializer::OutputSerializer(Stream::OutputStream& stream, Options options)
    : m_stream{stream}, m_options{options} {
}

Serialization::Format OutputSerializer::format() const {
  return Format::Binary;
}

Result<void> OutputSerializer::writeInt8(int8_t value, const Tag& nameTag) {
  XI_UNUSED(nameTag);
  return m_stream.writeStrict(asConstByteSpan(reinterpret_cast<Byte*>(&value), 1));
}

Result<void> OutputSerializer::writeUInt8(uint8_t value, const Tag& nameTag) {
  XI_UNUSED(nameTag);
  return m_stream.writeStrict(asConstByteSpan(reinterpret_cast<Byte*>(&value), 1));
}

Result<void> OutputSerializer::writeInt16(int16_t value, const Tag& nameTag) {
  XI_UNUSED(nameTag);
  return hasFlag(m_options.varInt, VarIntUsage::Int16) ? writeVarInt(value, m_stream)
                                                       : writeInt(value, m_stream, m_options);
}

Result<void> OutputSerializer::writeUInt16(uint16_t value, const Tag& nameTag) {
  XI_UNUSED(nameTag);
  return hasFlag(m_options.varInt, VarIntUsage::UInt16) ? writeVarInt(value, m_stream)
                                                        : writeInt(value, m_stream, m_options);
}

Result<void> OutputSerializer::writeInt32(int32_t value, const Tag& nameTag) {
  XI_UNUSED(nameTag);
  return hasFlag(m_options.varInt, VarIntUsage::Int32) ? writeVarInt(value, m_stream)
                                                       : writeInt(value, m_stream, m_options);
}

Result<void> OutputSerializer::writeUInt32(uint32_t value, const Tag& nameTag) {
  XI_UNUSED(nameTag);
  return hasFlag(m_options.varInt, VarIntUsage::UInt32) ? writeVarInt(value, m_stream)
                                                        : writeInt(value, m_stream, m_options);
}

Result<void> OutputSerializer::writeInt64(int64_t value, const Tag& nameTag) {
  XI_UNUSED(nameTag);
  return hasFlag(m_options.varInt, VarIntUsage::Int64) ? writeVarInt(value, m_stream)
                                                       : writeInt(value, m_stream, m_options);
}

Result<void> OutputSerializer::writeUInt64(uint64_t value, const Tag& nameTag) {
  XI_UNUSED(nameTag);
  return hasFlag(m_options.varInt, VarIntUsage::UInt64) ? writeVarInt(value, m_stream)
                                                        : writeInt(value, m_stream, m_options);
}

Result<void> OutputSerializer::writeBoolean(bool value, const Tag& nameTag) {
  XI_UNUSED(nameTag);
  uint8_t nativeValue = value ? 0b01010101 : 0b00101010;
  return writeUInt8(nativeValue, nameTag);
}

Result<void> OutputSerializer::writeFloat(float value, const Tag& nameTag) {
  static_assert(std::numeric_limits<float>::is_iec559,
                "binary floating serialization is not supported on this architecture");
  union {
    float asFloating;
    Byte asBlob[sizeof(float)];
  } data;
  data.asFloating = value;
  return writeBlob(data.asBlob, nameTag);
}

Result<void> OutputSerializer::writeDouble(double value, const Tag& nameTag) {
  static_assert(std::numeric_limits<double>::is_iec559,
                "binary floating serialization is not supported on this architecture");
  union {
    double asFloating;
    Byte asBlob[sizeof(double)];
  } data;
  data.asFloating = value;
  return writeBlob(data.asBlob, nameTag);
}

Result<void> OutputSerializer::writeTag(const Serialization::Tag& value, const Tag& nameTag) {
  XI_FAIL_IF(value.binary() == Tag::NoBinaryTag, BinaryError::NullTag);
  return writeUInt64(value.binary(), nameTag);
}

Result<void> OutputSerializer::writeFlag(const Serialization::TagVector& flag, const Tag& nameTag) {
  uint16_t nativeFlag = 0;
  for (const auto& iFlag : flag) {
    XI_FAIL_IF(iFlag.binary() == Tag::NoBinaryTag, BinaryError::NullTag);
    XI_FAIL_IF(iFlag.binary() > 15, BinaryError::FlagOverflow);
    assert(iFlag.binary() > 0);
    nativeFlag |= (1 << (iFlag.binary() - 1));
  }
  return writeUInt16(nativeFlag, nameTag);
}

Result<void> OutputSerializer::writeString(const std::string_view value, const Tag& nameTag) {
  XI_ERROR_PROPAGATE_CATCH(writeUInt64(value.size(), nameTag));
  return writeBlob(asByteSpan(value), nameTag);
}

Result<void> OutputSerializer::writeBinary(ConstByteSpan value, const Tag& nameTag) {
  XI_ERROR_PROPAGATE_CATCH(writeUInt64(value.size(), nameTag));
  return writeBlob(value, nameTag);
}

Result<void> OutputSerializer::writeBlob(ConstByteSpan value, const Tag& nameTag) {
  XI_UNUSED(nameTag);
  return m_stream.writeStrict(value);
}

Result<void> OutputSerializer::beginWriteComplex(const Tag& nameTag) {
  XI_UNUSED(nameTag);
  XI_SUCCEED();
}

Result<void> OutputSerializer::endWriteComplex() {
  XI_SUCCEED();
}

Result<void> OutputSerializer::beginWriteVector(size_t size, const Tag& nameTag) {
  return writeUInt64(size, nameTag);
}

Result<void> OutputSerializer::endWriteVector() {
  XI_SUCCEED();
}

Result<void> OutputSerializer::beginWriteArray(size_t size, const Tag& nameTag) {
  XI_UNUSED(size, nameTag);
  XI_SUCCEED();
}

Result<void> OutputSerializer::endWriteArray() {
  XI_SUCCEED();
}

Result<void> OutputSerializer::writeNull(const Tag& nameTag) {
  return writeBoolean(false, nameTag);
}

Result<void> OutputSerializer::writeNotNull(const Tag& nameTag) {
  return writeBoolean(true, nameTag);
}

}  // namespace Binary
}  // namespace Serialization
}  // namespace Xi
