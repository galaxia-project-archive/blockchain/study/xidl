/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Stream/CompoundOutputStream.hpp"

#include <utility>

#include <Xi/Exceptions.hpp>

#include "Xi/Stream/StreamError.hpp"

Xi::Stream::CompoundOutputStream::CompoundOutputStream(std::vector<Xi::Stream::UniqueOutputStream> dests) {
  for (auto& dest : dests) {
    XI_EXCEPTIONAL_IF_NOT(NullArgumentError, dest);
    m_dests.emplace_back(std::move(dest));
  }
}

Xi::Result<size_t> Xi::Stream::CompoundOutputStream::write(Xi::ConstByteSpan buffer) {
  XI_SUCCEED_IF(buffer.empty(), 0u);
  XI_SUCCEED_IF(m_dests.empty(), 0u);
  size_t nWritten = 0;
  do {
    const auto iWritten = m_dests.front()->write(buffer);
    XI_ERROR_PROPAGATE(iWritten)
    if (*iWritten == 0) {
      m_dests.pop_front();
      if (nWritten > 0) {
        break;
      } else {
        continue;
      }
    }
    buffer = buffer.slice(nWritten);
    if (buffer.empty()) {
      break;
    } else if (nWritten > 0) {
      break;
    }
  } while (!m_dests.empty());
  return success(nWritten);
}

Xi::Result<void> Xi::Stream::CompoundOutputStream::flush() {
  XI_FAIL_IF(m_dests.empty(), StreamError::EndOfStream);
  return m_dests.front()->flush();
}
