﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Http/ContentEncoding.h"

#include <Xi/Exceptions.hpp>

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Http, ContentEncoding)
XI_ERROR_CODE_DESC(Unknown, "unknown content encoding")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Http {

std::string stringify(Xi::Http::ContentEncoding encoding) {
  switch (encoding) {
    case Http::ContentEncoding::Gzip:
      return "gzip";
    case Http::ContentEncoding::Compress:
      return "compress";
    case Http::ContentEncoding::Deflate:
      return "deflate";
    case Http::ContentEncoding::Identity:
      return "identity";
    case Http::ContentEncoding::Brotli:
      return "br";
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

Result<void> parse(const std::string &str, ContentEncoding &out) {
  if (str == toString(Http::ContentEncoding::Gzip))
    out = Http::ContentEncoding::Gzip;
  else if (str == toString(Http::ContentEncoding::Compress))
    out = Http::ContentEncoding::Compress;
  else if (str == toString(Http::ContentEncoding::Deflate))
    out = Http::ContentEncoding::Deflate;
  else if (str == toString(Http::ContentEncoding::Identity))
    out = Http::ContentEncoding::Identity;
  else if (str == toString(Http::ContentEncoding::Brotli))
    out = Http::ContentEncoding::Brotli;
  else
    XI_FAIL(ContentEncodingError::Unknown)
  XI_SUCCEED()
}

}  // namespace Http
}  // namespace Xi
