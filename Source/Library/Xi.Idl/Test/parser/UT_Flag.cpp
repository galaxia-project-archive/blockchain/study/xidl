/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/Testing/Result.hpp>
#include <Xi/Idl/Parser/Flag.hpp>

#define XI_TEST_SUITE Xi_Idl_Parser_Flag

class XI_TEST_SUITE : public ::testing::Test {
 public:
  Xi::Idl::Parser::Flag flag;
  std::shared_ptr<Xi::Idl::Parser::FlagAutomaton> automaton;

  XI_TEST_SUITE() : flag{}, automaton{nullptr} {
    /* */
  }

  void process(const std::string& content) {
    flag = Xi::Idl::Parser::Flag{/* */};
    automaton = Xi::Idl::Parser::makeFlagAutomaton(flag);
    auto ec = automaton->process(content);
    ASSERT_THAT(ec, Xi::Testing::IsSuccess());
  }
};

TEST_F(XI_TEST_SUITE, WellFormed) {
  using namespace Xi::Idl::Parser;
  using namespace ::testing;

  process(R"__(
flag BlockFeatures {
  StaticReward (1;static_reward);
  ProofOfWork (2;proof_of_work);
  BlockReward (3;block_reward);
}
)__");

  EXPECT_THAT(flag.name, Eq("BlockFeatures"));
  ASSERT_THAT(flag.values, SizeIs(Eq(3)));
  {
    const auto& value = flag.values.at(0);
    EXPECT_THAT(value.name, Eq("StaticReward"));
    EXPECT_THAT(value.tag.text, Eq("static_reward"));
    EXPECT_THAT(value.tag.binary, Eq(1));
  }
  {
    const auto& value = flag.values.at(1);
    EXPECT_THAT(value.name, Eq("ProofOfWork"));
    EXPECT_THAT(value.tag.text, Eq("proof_of_work"));
    EXPECT_THAT(value.tag.binary, Eq(2));
  }
  {
    const auto& value = flag.values.at(2);
    EXPECT_THAT(value.name, Eq("BlockReward"));
    EXPECT_THAT(value.tag.text, Eq("block_reward"));
    EXPECT_THAT(value.tag.binary, Eq(3));
  }
}
