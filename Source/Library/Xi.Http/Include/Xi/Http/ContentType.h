﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string>

#include <Xi/Result.hpp>
#include <Xi/ErrorCode.hpp>
#include <Xi/Span.hpp>
#include <Xi/String/String.hpp>

namespace Xi {
namespace Http {

XI_ERROR_CODE_BEGIN(ContentType)
XI_ERROR_CODE_VALUE(Unknown, 0x0001)
XI_ERROR_CODE_END(ContentType, "Http::ContentTypeError")

/*!
 * \brief The Content-Type entity header is used to indicate the media type of the resource.
 *
 * In responses, a Content-Type header tells the client what the content type of the returned content actually is.
 * Browsers will do MIME sniffing in some cases and will not necessarily follow the value of this header; to prevent
 * this behavior, the header X-Content-Type-Options can be set to nosniff.
 *
 * In requests, (such as POST or PUT), the client tells the server what type of data is actually sent.
 *
 * Documentation is taken from https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Type
 */
enum struct ContentType { Html, Plain, Xml, Json, Text, Binary, Yaml, MultipartFormData };

XI_DECLARE_SPANS(ContentType)

std::string stringify(ContentType status);
Result<void> parse(const std::string& value, ContentType& out);

}  // namespace Http
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Http, ContentType)
