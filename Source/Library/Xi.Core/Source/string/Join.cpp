/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/String/Join.hpp"

#include <numeric>
#include <cstring>

namespace Xi {

std::string join(Span<const std::string> strings, std::string_view token) {
  if (strings.empty()) {
    return "";
  } else if (strings.size() == 1) {
    return strings.front();
  } else {
    std::string reval{};
    auto size = std::accumulate(strings.begin(), strings.end(), (strings.size() - 1) * token.size(),
                                [](const size_t acc, const auto& istring) { return acc + istring.size(); });
    reval.resize(size);
    for (size_t i = 0, offset = 0; i < strings.size(); ++i) {
      const auto& istring = strings[i];
      std::memcpy(std::addressof(reval[offset]), istring.data(), istring.size() * sizeof(std::string::value_type));
      offset += istring.size() + token.size();
    }
    for (size_t i = 0, offset = 0; i < strings.size() - 1; ++i) {
      offset += strings[i].size();
      std::memcpy(std::addressof(reval[offset]), token.data(), token.size() * sizeof(std::string::value_type));
      offset += token.size();
    }
    return reval;
  }
}

}  // namespace Xi
