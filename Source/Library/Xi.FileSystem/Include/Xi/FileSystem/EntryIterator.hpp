/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <iterator>
#include <filesystem>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>

#include "Xi/FileSystem/Entry.hpp"

namespace Xi {
namespace FileSystem {

class EntryIterator final {
 public:
  using iterator_category = std::forward_iterator_tag;
  using value_type = Entry;
  using difference_type = std::ptrdiff_t;
  using pointer = value_type*;
  using reference = value_type&;
  using const_pointer = const value_type*;
  using const_reference = const value_type&;

 public:
  XI_DEFAULT_COPY(EntryIterator);
  XI_DELETE_MOVE(EntryIterator);
  ~EntryIterator() = default;

  value_type operator*() const;
  EntryIterator& operator++();

  bool operator==(const EntryIterator& rhs) const;
  bool operator!=(const EntryIterator& rhs) const;

 private:
  explicit EntryIterator(std::filesystem::directory_iterator iter);
  friend class EntryGenerator;

 private:
  std::filesystem::directory_iterator m_iter;
};

}  // namespace FileSystem
}  // namespace Xi
