param(
    [string]$Name 
)

$Ns = $Name.Split("-")
$NsPath = [string]::Join("\", $Ns)

Write-Host $NsPath

New-Item -ItemType Directory -Path ".\$Name\Include\$NsPath"
New-Item -ItemType Directory -Path ".\$Name\Source"