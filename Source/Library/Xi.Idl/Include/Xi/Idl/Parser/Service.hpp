/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <vector>

#include "Xi/Idl/Parser/Identifier.hpp"
#include "Xi/Idl/Parser/Tag.hpp"
#include "Xi/Idl/Parser/Reference.hpp"
#include "Xi/Idl/Parser/Documentation.hpp"
#include "Xi/Idl/Parser/Automaton.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

struct ServiceCommand {
  Documentation doc;
  Reference contract;
  Tag tag;
};
using ServiceCommandVector = std::vector<ServiceCommand>;

struct Service {
  Documentation doc;
  Identifier name;
  std::vector<ServiceCommand> commands;
};
using ServiceVector = std::vector<Service>;

class ServiceCommandAutomaton final : public ResultAutomaton<ServiceCommand> {
 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<ServiceCommandAutomaton> makeServiceCommandAutomaton(ServiceCommand&);
};

std::shared_ptr<ServiceCommandAutomaton> makeServiceCommandAutomaton(ServiceCommand& result);

class ServiceCommandsAutomaton final : public ResultAutomaton<ServiceCommandVector> {
 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<ServiceCommandsAutomaton> makeServiceCommandsAutomaton(ServiceCommandVector&);

 protected:
  virtual Result<void> doProcessToken(const Token& token) override;
};

std::shared_ptr<ServiceCommandsAutomaton> makeServiceCommandsAutomaton(ServiceCommandVector& result);

class ServiceAutomaton final : public ResultAutomaton<Service> {
 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<ServiceAutomaton> makeServiceAutomaton(Service&);
};

std::shared_ptr<ServiceAutomaton> makeServiceAutomaton(Service& result);

class ServicesAutomaton final : public ResultAutomaton<ServiceVector> {
 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<ServicesAutomaton> makeServicesAutomaton(ServiceVector&);

 protected:
  virtual Result<void> doProcessToken(const Token& token) override;
};

std::shared_ptr<ServicesAutomaton> makeServicesAutomaton(ServiceVector& result);

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi
