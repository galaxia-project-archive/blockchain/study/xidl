/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/Array.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

const Type &Array::type() const {
  return m_type;
}

ArraySize Array::size() const {
  return m_size;
}

void Array::setType(const Type &type_) {
  m_type = type_;
}

void Array::setSize(ArraySize size_) {
  m_size = size_;
}

ArrayBuilder::ArrayBuilder() : m_array{result_success, Array{/* */}} {
  /* */
}

ArrayBuilder &ArrayBuilder::withType(const Type &type) {
  if (!m_array.isError()) {
    m_array->setType(type);
  }
  return *this;
}

ArrayBuilder &ArrayBuilder::withSize(ArraySize size) {
  if (!m_array.isError()) {
    m_array->setSize(size);
  }
  return *this;
}

Result<Array> ArrayBuilder::build() {
  return m_array;
}

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi
