/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Parser/Reference.hpp"

#include <Xi/String/String.hpp>

#include "Xi/Idl/Parser/Keyword.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

std::string Reference::stringify() const {
  return "#" + join(nested, ".");
}

std::shared_ptr<ReferenceAutomaton> makeReferenceAutomaton(Reference &ref) {
  std::shared_ptr<ReferenceAutomaton> reval{new ReferenceAutomaton{ref}};
  reval->chain(makeKeywordAutomaton("#"));
  reval->chain(makeIdentifiersAutomaton(ref.nested));
  return reval;
}

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi
