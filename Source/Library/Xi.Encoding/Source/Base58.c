/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * Previous Work                                                                                  *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * - Copyright 2012-2014 Luke Dashjr <https://github.com/luke-jr/libbase58>                       *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Encoding/Base58.hh"

#include <string.h>
#include <stdio.h>

size_t xi_encoding_base58_encode_max_length(size_t sourceLength)
{
  return sourceLength * 138 / 100 + 1;
}

static const char xi_encoding_base58_encode_lookup[]
    = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";

int xi_encoding_base58_encode(char *encoded, size_t *encodedSize, const xi_byte_t *data, size_t dataLength)
{
  size_t leadingZerosCount = 0;
  while(leadingZerosCount < dataLength && !(data[leadingZerosCount])) {
    leadingZerosCount += 1;
  }

  const size_t size = (dataLength - leadingZerosCount) * 138 / 100 + 1;
  uint8_t* buf = (uint8_t*)malloc(size);
  memset(buf, 0, size);

  int ec = XI_RETURN_CODE_SUCCESS;

  do {
    int carry;
    size_t i, j, high = 0;
    for (i = 0, high = size - 1; i < dataLength; ++i, high = j) {
      for (carry = data[i], j = size - 1; (j > high) || carry; --j) {
        carry += 256 * buf[j];
        buf[j] = carry % 58;
        carry /= 58;
        if (!j) {
          // Otherwise j wraps to maxint which is > high
          break;
        }
      }
    }

    for (j = 0; j < size && !buf[j]; ++j);

    if (*encodedSize < leadingZerosCount + size - j) {
      *encodedSize = leadingZerosCount + size - j;
      ec = XI_RETURN_CODE_NO_SUCCESS;
      break;
    }

    if (leadingZerosCount) {
      memset(encoded, '1', leadingZerosCount);
    }
    for (i = leadingZerosCount; j < size; ++i, ++j) {
      encoded[i] = xi_encoding_base58_encode_lookup[buf[j]];
    }
    *encodedSize = i;
  } while(XI_FALSE);

  free(buf);
  return ec;
}

static const int8_t xi_encoding_base58_decode_lookup[256] =
{
  -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1,
  -1,  0,  1,  2,  3,  4,  5,  6,
   7,  8, -1, -1, -1, -1, -1, -1,
  -1,  9, 10, 11, 12, 13, 14, 15,
  16, -1, 17, 18, 19, 20, 21, -1,
  22, 23, 24, 25, 26, 27, 28, 29,
  30, 31, 32, -1, -1, -1, -1, -1,
  -1, 33, 34, 35, 36, 37, 38, 39,
  40, 41, 42, 43, -1, 44, 45, 46,
  47, 48, 49, 50, 51, 52, 53, 54,
  55, 56, 57, -1, -1, -1, -1, -1,
};

size_t xi_encoding_base58_decode_max_length(size_t encodedLength)
{
  return encodedLength;
}

int xi_encoding_base58_decode(xi_byte_t *decoded, size_t *decodedSize, const char *data, size_t dataLength)
{
  size_t binsz = *decodedSize;
  const unsigned char *b58u = (const unsigned char*)data;
  unsigned char *binu = decoded;
  size_t outisz = (binsz + sizeof(uint32_t) - 1) / sizeof(uint32_t);
  uint32_t* outi = (uint32_t*)malloc(sizeof(uint32_t) * outisz);
  uint64_t t;
  uint32_t c;
  size_t i, j;
  uint8_t bytesleft = binsz % sizeof(uint32_t);
  uint32_t zeromask = bytesleft ? (0xFFFFFFFF << (bytesleft * 8)) : 0;
  unsigned zerocount = 0;

  for (i = 0; i < outisz; ++i) {
    outi[i] = 0;
  }

  // Leading zeros, just count
  for (i = 0; i < dataLength && b58u[i] == '1'; ++i) {
    ++zerocount;
  }

  int ec = XI_RETURN_CODE_SUCCESS;
  for ( ; i < dataLength; ++i)
  {
    if(b58u[i] & 0x80) {
      ec = XI_BASE58_DECODE_NONE_CANONICAL;
      goto __label_xi_encoding_base58_decode_return;
    }
    if(xi_encoding_base58_decode_lookup[b58u[i]] == -1) {
      ec = XI_BASE58_DECODE_INVALID_CHAR;
      goto __label_xi_encoding_base58_decode_return;
    }

    c = (unsigned)xi_encoding_base58_decode_lookup[b58u[i]];
    for (j = outisz; j--; )
    {
      t = ((uint64_t)outi[j]) * 58 + c;
      c = t >> (sizeof (uint32_t) * 8);
      outi[j] = t & 0xFFFFFFFF;
    }
    if(c) {
      ec = XI_BASE58_DECODE_NONE_CANONICAL;
      goto __label_xi_encoding_base58_decode_return;
    }
    if(outi[0] & zeromask) {
      ec = XI_BASE58_DECODE_NONE_CANONICAL;
      goto __label_xi_encoding_base58_decode_return;
    }
  }

  j = 0;
  if (bytesleft) {
    for (i = bytesleft; i > 0; --i) {
      *(binu++) = (outi[0] >> (8 * (i - 1))) & 0xFF;
    }
    ++j;
  }

  for (; j < outisz; ++j)
  {
    for (i = sizeof(*outi); i > 0; --i) {
      *(binu++) = (outi[j] >> (8 * (i - 1))) & 0xFF;
    }
  }

  // Count canonical base58 byte count
  binu = decoded;
  for (i = 0; i < binsz; ++i)
  {
    if (binu[i])
      break;
    --*decodedSize;
  }
  *decodedSize += zerocount;

__label_xi_encoding_base58_decode_return:
  free(outi);
  return ec;
}
