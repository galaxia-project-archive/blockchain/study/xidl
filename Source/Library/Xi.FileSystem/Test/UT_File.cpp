/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/Testing/Result.hpp>
#include <Xi/FileSystem/File.hpp>

#define XI_TEST_SUITE Xi_FileSystem_File

TEST(XI_TEST_SUITE, CreateTemporary) {
  using namespace Xi::Testing;
  using namespace Xi::FileSystem;

  auto tempFile = makeTemporaryFile();
  ASSERT_THAT(tempFile, IsSuccess());

  auto tempExists = tempFile->exists();
  ASSERT_THAT(tempExists, IsSuccess());
  EXPECT_FALSE(*tempExists);

  auto tempRemoval = tempFile->remove();
  EXPECT_THAT(tempRemoval, IsSuccess());

  auto tempTouch = tempFile->touch();
  ASSERT_THAT(tempTouch, IsSuccess());

  tempExists = tempFile->exists();
  ASSERT_THAT(tempExists, IsSuccess());
  EXPECT_TRUE(*tempExists);

  tempRemoval = tempFile->remove();
  EXPECT_THAT(tempRemoval, IsSuccess());
}
