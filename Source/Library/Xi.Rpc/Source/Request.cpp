/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rpc/Request.hpp"

#include <utility>

namespace Xi {
namespace Rpc {

const Serialization::Tag Request::ParameterTag{0x0101, "parameter"};

Request::Request() : RequestPrefix(), m_parameter{makeContractPayload()} {
  /* */
}

Request::Request(RequestPrefix prefix) : Request(std::move(prefix), makeContractPayload()) {
  /* */
}

Request::Request(RequestPrefix prefix, ContractPayload parameter_)
    : RequestPrefix(std::move(prefix)), m_parameter{std::move(parameter_)} {
  /* */
}

ContractPayload &Request::parameter() {
  return m_parameter;
}

const ContractPayload &Request::parameter() const {
  return m_parameter;
}

void Request::setParameter(ContractPayload parameter_) {
  m_parameter = std::move(parameter_);
}

XI_SERIALIZATION_COMPLEX_EXTERN_BEGIN(Request)
XI_SERIALIZATION_BASE(RequestPrefix)
XI_SERIALIZATION_MEMBER(m_parameter, 0x0101, "parameter")
XI_SERIALIZATION_COMPLEX_EXTERN_END

}  // namespace Rpc
}  // namespace Xi
