/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/Testing/Result.hpp>
#include <Xi/Idl/Parser/Type.hpp>

#define XI_TEST_SUITE Xi_Idl_Parser_Type

class XI_TEST_SUITE : public ::testing::Test {
 public:
  Xi::Idl::Parser::Type type;
  std::shared_ptr<Xi::Idl::Parser::TypeAutomaton> automaton;

  XI_TEST_SUITE() : type{}, automaton{nullptr} {
    /* */
  }

  template <typename _TypeT>
  void expectAtomicType() {
    if (const auto atomic = std::get_if<Xi::Idl::Parser::Atomic>(std::addressof(type.base))) {
      EXPECT_TRUE(std::holds_alternative<_TypeT>(*atomic));
    } else {
      FAIL();
    }
  }

  template <typename _TypeT>
  void expectVectorType() {
    if (const auto container = std::get_if<Xi::Idl::Parser::ContainerType>(std::addressof(type.base))) {
      if (const auto vector = std::get_if<Xi::Idl::Parser::Vector>(container)) {
        EXPECT_TRUE(std::holds_alternative<_TypeT>(vector->baseType));
      } else {
        FAIL();
      }
    } else {
      FAIL();
    }
  }

  template <typename _TypeT>
  void expectArrayType() {
    if (const auto container = std::get_if<Xi::Idl::Parser::ContainerType>(std::addressof(type.base))) {
      if (const auto array = std::get_if<Xi::Idl::Parser::Array>(container)) {
        EXPECT_TRUE(std::holds_alternative<_TypeT>(array->baseType));
      } else {
        FAIL();
      }
    } else {
      FAIL();
    }
  }

  void isOptional() {
    EXPECT_TRUE(type.isOptional);
  }
  void isNotOptional() {
    EXPECT_FALSE(type.isOptional);
  }

  void process(const std::string& content) {
    type = Xi::Idl::Parser::Type{/* */};
    automaton = Xi::Idl::Parser::makeTypeAutomaton(type);
    auto ec = automaton->process(content);
    ASSERT_THAT(ec, Xi::Testing::IsSuccess());
  }
};

TEST_F(XI_TEST_SUITE, Atomic) {
  using namespace Xi::Idl::Parser;
  using namespace ::testing;

  process("uint8");
  expectAtomicType<UInt8>();
  isNotOptional();

  process("string?");
  expectAtomicType<String>();
  isOptional();

  process("boolean");
  expectAtomicType<Boolean>();
  isNotOptional();

  process("#io.xiproject.byte?");
  expectAtomicType<Reference>();
  isOptional();

  auto ref = std::get_if<Reference>(std::get_if<Atomic>(std::addressof(type.base)));
  ASSERT_FALSE(ref == nullptr);
  ASSERT_THAT(ref->nested, SizeIs(Eq(3)));
  EXPECT_THAT(ref->nested[0], Eq("io"));
  EXPECT_THAT(ref->nested[1], Eq("xiproject"));
  EXPECT_THAT(ref->nested[2], Eq("byte"));
}

TEST_F(XI_TEST_SUITE, Container) {
  using namespace Xi::Idl::Parser;

  process("uint8[+]");
  expectVectorType<UInt8>();
  isNotOptional();

  process("string[*]");
  expectVectorType<String>();
  isNotOptional();

  process("#io.xiporject.byte[0]?");
  expectArrayType<Reference>();
  isOptional();
}
