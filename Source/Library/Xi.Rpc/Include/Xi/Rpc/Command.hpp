/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string>
#include <memory>
#include <vector>
#include <cinttypes>

#include <Xi/Global.hh>
#include <Xi/Span.hpp>
#include <Xi/Result.hpp>
#include <Xi/String/String.hpp>
#include <Xi/Serialization/Serializer.hpp>

#include "Xi/Rpc/CommandError.hpp"

namespace Xi {
namespace Rpc {

XI_DECLARE_SMART_POINTER_CLASS(Command)

class Command : public std::enable_shared_from_this<Command> {
 public:
  static inline constexpr size_t MaximumDepth = 15;
  static inline constexpr uint16_t MaximumBinaryTag = 0x0FFFF;
  static inline constexpr uint16_t MaximumTextTag = 64;
  static inline constexpr size_t MaximumTextPathSize = (MaximumDepth * (MaximumTextTag + 1));

 public:
  using Binary = std::uint16_t;
  using BinaryPath = std::vector<Binary>;
  XI_DECLARE_SPANS(Binary)
  using Text = std::string;
  using TextPath = std::vector<std::string>;
  XI_DECLARE_SPANS(Text)

  using Path = std::vector<SharedConstCommand>;
  using PathSpan = Span<const SharedConstCommand>;

 public:
  static const Command Null;

  static Result<SharedCommand> parse(const std::string& raw);

 public:
  Binary binary() const;
  const Text& text() const;
  SharedConstCommand prefix() const;

  Path path() const;
  BinaryPath binaryPath() const;
  TextPath textPath() const;

  size_t depth() const;
  std::string stringify() const;

 private:
  Command(Binary binary_, const Text& text_, SharedConstCommand prefix_);

  friend Result<void> serialize(SharedCommand&, const Serialization::Tag&, Serialization::Serializer&);
  friend SharedCommand makeCommand(const Binary, const Text&, SharedConstCommand);

  void path(Path& out) const;
  void path(BinaryPath& out) const;
  void path(TextPath& out) const;

 private:
  Binary m_binary;
  Text m_text;
  SharedConstCommand m_prefix;
};

Result<void> parse(const std::string& str, SharedCommand& command);

Result<void> serialize(SharedCommand& command, const Serialization::Tag& name, Serialization::Serializer& serializer);

CommandError validateCommand(Command::ConstTextSpan path);
CommandError validateCommand(Command::ConstBinarySpan path);

SharedCommand makeCommand(const Command::Text& text, SharedConstCommand parent = nullptr);
SharedCommand makeCommand(Command::ConstTextSpan path);
SharedCommand makeCommand(const Command::Binary binary, SharedConstCommand parent = nullptr);
SharedCommand makeCommand(Command::ConstBinarySpan path);
SharedCommand makeCommand(const Command::Binary binary, const Command::Text& text, SharedConstCommand parent = nullptr);

}  // namespace Rpc
}  // namespace Xi
