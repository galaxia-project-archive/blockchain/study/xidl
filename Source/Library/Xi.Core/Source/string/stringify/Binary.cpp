/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/String/Stringify/Binary.hpp"

#include <sstream>
#include <limits>
#include <type_traits>

namespace Xi {

namespace {
template <typename _ValueT>
std::string toBinaryString(_ValueT value) {
  std::ostringstream builder{};
  if constexpr (std::is_signed_v<_ValueT>) {
    if (value < 0) {
      builder << "-";
      value *= -1;
    }
  }
  builder << "0b";
  for (size_t i = 0; i < sizeof(_ValueT) * 8; ++i) {
    if ((value & (static_cast<_ValueT>(1) << (sizeof(_ValueT) * 8 - i - 1))) > 0) {
      builder << "1";
    } else {
      builder << "0";
    }
  }
  return builder.str();
}
}  // namespace

std::string stringify(const uint8_t value, Stringify::binary_t) {
  return toBinaryString(value);
}

std::string stringify(const uint16_t value, Stringify::binary_t) {
  return toBinaryString(value);
}

std::string stringify(const uint32_t value, Stringify::binary_t) {
  return toBinaryString(value);
}

std::string stringify(const uint64_t value, Stringify::binary_t) {
  return toBinaryString(value);
}

std::string stringify(const int8_t value, Stringify::binary_t) {
  return toBinaryString(value);
}

std::string stringify(const int16_t value, Stringify::binary_t) {
  return toBinaryString(value);
}

std::string stringify(const int32_t value, Stringify::binary_t) {
  return toBinaryString(value);
}

std::string stringify(const int64_t value, Stringify::binary_t) {
  return toBinaryString(value);
}

}  // namespace Xi
