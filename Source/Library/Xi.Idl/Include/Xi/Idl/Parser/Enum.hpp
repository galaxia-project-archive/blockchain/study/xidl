/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <vector>

#include "Xi/Idl/Parser/Identifier.hpp"
#include "Xi/Idl/Parser/Automaton.hpp"
#include "Xi/Idl/Parser/Tag.hpp"
#include "Xi/Idl/Parser/Documentation.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

struct EnumEntry {
  Documentation doc;
  Identifier name;
  Tag tag;
};
using EnumEntryVector = std::vector<EnumEntry>;

struct Enum {
  Identifier name;
  std::vector<EnumEntry> values;
};

class EnumEntryAutomaton final : public ResultAutomaton<EnumEntry> {
 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<EnumEntryAutomaton> makeEnumEntryAutomaton(EnumEntry&);
};

std::shared_ptr<EnumEntryAutomaton> makeEnumEntryAutomaton(EnumEntry& result);

class EnumEntriesAutomaton final : public ResultAutomaton<EnumEntryVector> {
 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<EnumEntriesAutomaton> makeEnumEntriesAutomaton(EnumEntryVector&);

 protected:
  Result<void> doProcessToken(const Token& token) override;
};

std::shared_ptr<EnumEntriesAutomaton> makeEnumEntriesAutomaton(EnumEntryVector& result);

class EnumAutomaton final : public ResultAutomaton<Enum> {
 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<EnumAutomaton> makeEnumAutomaton(Enum&);
};

std::shared_ptr<EnumAutomaton> makeEnumAutomaton(Enum& result);

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi
