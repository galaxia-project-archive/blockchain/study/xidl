/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string>
#include <vector>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>

namespace Xi {
namespace Idl {
namespace Generator {

class Documentation {
 public:
  Documentation() = default;

  const std::string& brief() const;
  const std::string& detailed() const;
  const std::vector<std::string>& examples() const;

 private:
  friend class DocumentationBuilder;

 private:
  std::string m_brief;
  std::string m_detailed;
  std::vector<std::string> m_examples;
};

class DocumentationBuilder {
 public:
  DocumentationBuilder();
  XI_DELETE_COPY(DocumentationBuilder);
  XI_DELETE_MOVE(DocumentationBuilder);
  ~DocumentationBuilder() = default;

 public:
  DocumentationBuilder& withBrief(const std::string& brief);
  DocumentationBuilder& withDetailed(const std::string& detailed);
  DocumentationBuilder& withExample(const std::string& example);

  Result<Documentation> build();

 private:
  Result<Documentation> m_result;
};

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi
