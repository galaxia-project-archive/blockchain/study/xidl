/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rpc/CommandError.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Rpc, Command)
XI_ERROR_CODE_DESC(Null, "command path is empty")

XI_ERROR_CODE_DESC(BinaryNull, "binary command tag is zero")
XI_ERROR_CODE_DESC(BinaryOverflow, "binary command tag is too large")

XI_ERROR_CODE_DESC(TextNull, "text command tag is empty")
XI_ERROR_CODE_DESC(TextOverflow, "text command tag is too large")

XI_ERROR_CODE_DESC(Overflow, "command path is too large")
XI_ERROR_CODE_CATEGORY_END()
