/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/Cpp/Generator.hpp"

#include <utility>
#include <fstream>

#include <Xi/String/String.hpp>
#include <Xi/Log/Log.hpp>
#include <Xi/Stream/OStream.hpp>

#include "Xi/Idl/Generator/Cpp/FileMapper.hpp"
#include "CppGenerator.hpp"

XI_LOGGER("Idl/Generator/Cpp")

namespace Xi {
namespace Idl {
namespace Generator {
namespace Cpp {

struct Generator::_Impl {
  FileMapper mapper;

  explicit _Impl(const FileMapper &fileMapper) : mapper{fileMapper} {
    /* */
  }
};

Generator::Generator() {
  /* */
}

Generator::~Generator() {
  /* */
}

const FileMapper &Generator::fileMapper() const {
  return m_impl->mapper;
}

Result<void> Generator::doOpenNamespace(SharedConstNamespace ns) {
  XI_TRACE("Namespace: {}", ns);
  XI_SUCCEED()
}

Result<void> Generator::doOnPackage(const Package &package) {
  const auto packageType = currentScope().types().search(package.name());
  Include packageInclude{fileMapper().includeFilePath(*packageType), Include::Kind::ThisLibrary};
  const auto currentNamespace = currentScope().currentNamespace();

  XI_TRACE("Building package: {}", package.name());
  auto headerFile = fileMapper().headerFile(*packageType);
  XI_ERROR_PROPAGATE(headerFile)
  XI_TRACE(" -- Header: {}", headerFile);
  auto includeFile = fileMapper().includeFilePath(*packageType);
  XI_ERROR_PROPAGATE(headerFile)
  XI_TRACE(" -- Include: {}", includeFile);

  auto headerStream = headerFile->openTextForWrite(FileSystem::overwrite);
  XI_ERROR_PROPAGATE(headerStream)
  CppGenerator header{headerStream.take()};

  IncludeCollection headerIncludes{};
  headerIncludes.add(Include::Xi::Serialization::Complex);
  if (package.inheritance()) {
    XI_ERROR_PROPAGATE_CATCH(fileMapper().includes(*package.inheritance(), headerIncludes))
  }
  for (const auto &field : package.fields()) {
    XI_ERROR_PROPAGATE_CATCH(fileMapper().includes(field.type(), headerIncludes))
  }

  XI_ERROR_PROPAGATE_CATCH(header.printCopyright())
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
  XI_ERROR_PROPAGATE_CATCH(header.printInclude(headerIncludes))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
  XI_ERROR_PROPAGATE_CATCH(header.printOpenNamespaces(package.namespace_()->path()))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))

  uint64_t memberBinaryOffset = 0;
  {
    Type currentInheritance = *packageType;
    while (currentInheritance.isPackage() && currentInheritance.asPackage().inheritance()) {
      memberBinaryOffset += 0x0100;
      currentInheritance = *currentInheritance.asPackage().inheritance();
    }
  }

  XI_ERROR_PROPAGATE(header.printDocumentation(package.documentation()))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))

  if (package.inheritance()) {
    XI_ERROR_PROPAGATE_CATCH(
        header.printBeginStruct(package.name(), makeTypeSignature(*package.inheritance()), currentNamespace))
  } else {
    XI_ERROR_PROPAGATE_CATCH(header.printBeginStruct(package.name()))
  }
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))
  for (const auto &field : package.fields()) {
    XI_ERROR_PROPAGATE_CATCH(header.printStructMember(field, currentNamespace));
    XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))
  }
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))

  XI_ERROR_PROPAGATE_CATCH(header.print("XI_SERIALIZATION_COMPLEX_BEGIN()"))
  if (package.inheritance()) {
    XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))
    XI_ERROR_PROPAGATE_CATCH(header.print("XI_SERIALIZATION_BASE("))
    XI_ERROR_PROPAGATE_CATCH(header.print(package.inheritance()->name()))
    XI_ERROR_PROPAGATE_CATCH(header.print(")"))
  }
  for (const auto &field : package.fields()) {
    XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))
    XI_ERROR_PROPAGATE_CATCH(header.print("XI_SERIALIZATION_MEMBER("));
    XI_ERROR_PROPAGATE_CATCH(header.printStructMemberName(field));
    XI_ERROR_PROPAGATE_CATCH(header.print(", "));
    XI_ERROR_PROPAGATE_CATCH(header.print(toString(field.tag().binary() + memberBinaryOffset, Stringify::hexadecimal)));
    XI_ERROR_PROPAGATE_CATCH(header.print(", \""));
    XI_ERROR_PROPAGATE_CATCH(header.print(field.tag().text()))
    XI_ERROR_PROPAGATE_CATCH(header.print("\")"));
  }
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))
  XI_ERROR_PROPAGATE_CATCH(header.print("XI_SERIALIZATION_COMPLEX_END"))

  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))
  XI_ERROR_PROPAGATE_CATCH(header.printEndStruct(package.name()))

  for (const auto &entry : package.fields()) {
    XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
    XI_ERROR_PROPAGATE_CATCH(header.printDocumentation(entry.documentation(), package.name(), entry.name()))
  }

  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
  XI_ERROR_PROPAGATE_CATCH(header.printCloseNamespaces(package.namespace_()->path()))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))

  XI_ERROR_PROPAGATE_CATCH(header.stream().flush())
  XI_SUCCEED()
}

Result<void> Generator::doOnVariant(const Variant &variant) {
  const auto variantType = currentScope().types().search(variant.name());
  Include variantInclude{fileMapper().includeFilePath(*variantType), Include::Kind::ThisLibrary};
  const auto currentNamespace = currentScope().currentNamespace();

  XI_TRACE("Building variant: {}", variant.name());
  auto headerFile = fileMapper().headerFile(*variantType);
  XI_ERROR_PROPAGATE(headerFile)
  XI_TRACE(" -- Header: {}", headerFile);
  auto includeFile = fileMapper().includeFilePath(*variantType);
  XI_ERROR_PROPAGATE(headerFile)
  XI_TRACE(" -- Include: {}", includeFile);

  auto headerStream = headerFile->openTextForWrite(FileSystem::overwrite);
  XI_ERROR_PROPAGATE(headerStream)
  CppGenerator header{headerStream.take()};

  IncludeCollection headerIncludes{};
  headerIncludes.add(Include::Xi::Serialization::VariantSerialization);
  for (const auto &field : variant.fields()) {
    XI_ERROR_PROPAGATE_CATCH(fileMapper().includes(field.type(), headerIncludes))
  }

  XI_ERROR_PROPAGATE_CATCH(header.printCopyright())
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
  XI_ERROR_PROPAGATE_CATCH(header.printInclude(headerIncludes))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
  XI_ERROR_PROPAGATE_CATCH(header.printOpenNamespaces(variant.namespace_()->path()))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))

  XI_ERROR_PROPAGATE_CATCH(header.print("XI_SERIALIZATION_VARIANT_INVARIANT("))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))
  XI_ERROR_PROPAGATE_CATCH(header.print(variant.name()))

  for (const auto &field : variant.fields()) {
    XI_ERROR_PROPAGATE_CATCH(header.print(","))
    XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))
    XI_ERROR_PROPAGATE_CATCH(header.printTypeSignature(makeTypeSignature(field.type()), currentNamespace))
  }
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))
  XI_ERROR_PROPAGATE_CATCH(header.print(")"))

  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
  XI_ERROR_PROPAGATE_CATCH(header.printCloseNamespaces(variant.namespace_()->path()))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))

  size_t i = 0;
  for (const auto &field : variant.fields()) {
    XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))
    XI_ERROR_PROPAGATE_CATCH(header.print("XI_SERIALIZATION_VARIANT_TAG("))
    XI_ERROR_PROPAGATE_CATCH(header.printTypeSignature(makeTypeSignature(*variantType)))
    XI_ERROR_PROPAGATE_CATCH(header.print(", "))
    XI_ERROR_PROPAGATE_CATCH(header.print(toString(i++, Stringify::hexadecimal)))
    XI_ERROR_PROPAGATE_CATCH(header.print(", "))
    XI_ERROR_PROPAGATE_CATCH(header.print(toString(field.tag().binary(), Stringify::hexadecimal)))
    XI_ERROR_PROPAGATE_CATCH(header.print(", "))
    XI_ERROR_PROPAGATE_CATCH(header.print("\""))
    XI_ERROR_PROPAGATE_CATCH(header.print(field.tag().text()))
    XI_ERROR_PROPAGATE_CATCH(header.print("\""))
    XI_ERROR_PROPAGATE_CATCH(header.print(")"))
  }

  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))
  XI_ERROR_PROPAGATE_CATCH(header.stream().flush())
  XI_SUCCEED();
}

Result<void> Generator::doOnAlias(const Alias &alias) {
  XI_TRACE("Building alias: {}", alias.name());
  const auto aliasType = currentScope().types().search(alias.name());
  Include aliasInclude{fileMapper().includeFilePath(*aliasType), Include::Kind::ThisLibrary};
  const auto currentNamespace = currentScope().currentNamespace();

  auto headerFile = fileMapper().headerFile(*aliasType);
  XI_ERROR_PROPAGATE(headerFile)
  XI_TRACE(" -- Header: {}", headerFile);
  auto includeFile = fileMapper().includeFilePath(*aliasType);
  XI_ERROR_PROPAGATE(headerFile)
  XI_TRACE(" -- Include: {}", includeFile);

  auto headerStream = headerFile->openTextForWrite(FileSystem::overwrite);
  XI_ERROR_PROPAGATE(headerStream)
  CppGenerator header{headerStream.take()};

  IncludeCollection headerIncludes{};
  XI_ERROR_PROPAGATE_CATCH(fileMapper().includes(alias.type(), headerIncludes))

  XI_ERROR_PROPAGATE_CATCH(header.printCopyright())
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
  XI_ERROR_PROPAGATE_CATCH(header.printInclude(headerIncludes))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
  XI_ERROR_PROPAGATE_CATCH(header.printOpenNamespaces(alias.namespace_()->path()))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))

  XI_ERROR_PROPAGATE_CATCH(header.printDocumentation(alias.documentation()))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))
  XI_ERROR_PROPAGATE_CATCH(header.print("using "))
  XI_ERROR_PROPAGATE_CATCH(header.print(alias.name()))
  XI_ERROR_PROPAGATE_CATCH(header.print(" = "))
  XI_ERROR_PROPAGATE_CATCH(header.printTypeSignature(makeTypeSignature(alias.type()), currentNamespace))
  XI_ERROR_PROPAGATE_CATCH(header.print(";"))

  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
  XI_ERROR_PROPAGATE_CATCH(header.printCloseNamespaces(alias.namespace_()->path()))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))
  XI_ERROR_PROPAGATE_CATCH(header.stream().flush())
  XI_SUCCEED();
}

Result<void> Generator::doOnTypedef(const Typedef &typedef_) {
  XI_TRACE("Building typedef: {}", typedef_.name());
  const auto typedefType = currentScope().types().search(typedef_.name());
  Include typedefInclude{fileMapper().includeFilePath(*typedefType), Include::Kind::ThisLibrary};
  const auto currentNamespace = currentScope().currentNamespace();

  auto headerFile = fileMapper().headerFile(*typedefType);
  XI_ERROR_PROPAGATE(headerFile)
  XI_TRACE(" -- Header: {}", headerFile);
  auto includeFile = fileMapper().includeFilePath(*typedefType);
  XI_ERROR_PROPAGATE(headerFile)
  XI_TRACE(" -- Include: {}", includeFile);

  auto headerStream = headerFile->openTextForWrite(FileSystem::overwrite);
  XI_ERROR_PROPAGATE(headerStream)
  CppGenerator header{headerStream.take()};

  IncludeCollection headerIncludes{};
  XI_ERROR_PROPAGATE_CATCH(fileMapper().includes(typedef_.type(), headerIncludes))

  XI_ERROR_PROPAGATE_CATCH(header.printCopyright())
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
  XI_ERROR_PROPAGATE_CATCH(header.printInclude(headerIncludes))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
  XI_ERROR_PROPAGATE_CATCH(header.printOpenNamespaces(typedef_.namespace_()->path()))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))

  XI_ERROR_PROPAGATE_CATCH(header.printDocumentation(typedef_.documentation()))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))
  XI_ERROR_PROPAGATE_CATCH(header.print("using "))
  XI_ERROR_PROPAGATE_CATCH(header.print(typedef_.name()))
  XI_ERROR_PROPAGATE_CATCH(header.print(" = "))
  XI_ERROR_PROPAGATE_CATCH(header.printTypeSignature(makeTypeSignature(typedef_.type()), currentNamespace))
  XI_ERROR_PROPAGATE_CATCH(header.print(";"))

  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
  XI_ERROR_PROPAGATE_CATCH(header.printCloseNamespaces(typedef_.namespace_()->path()))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))
  XI_ERROR_PROPAGATE_CATCH(header.stream().flush())
  XI_SUCCEED();
}

Result<void> Generator::doOnEnum(const Enum &enum_) {
  XI_TRACE("Building enum: {}", enum_.name());
  const auto enumType = currentScope().types().search(enum_.name());
  Include enumInclude{fileMapper().includeFilePath(*enumType), Include::Kind::ThisLibrary};
  const auto currentNamespace = currentScope().currentNamespace();

  auto headerFile = fileMapper().headerFile(*enumType);
  XI_ERROR_PROPAGATE(headerFile)
  XI_TRACE(" -- Header: {}", headerFile);
  auto includeFile = fileMapper().includeFilePath(*enumType);
  XI_ERROR_PROPAGATE(headerFile)
  XI_TRACE(" -- Include: {}", includeFile);

  auto headerStream = headerFile->openTextForWrite(FileSystem::overwrite);
  XI_ERROR_PROPAGATE(headerStream)
  CppGenerator header{headerStream.take()};

  IncludeCollection headerIncludes{};
  headerIncludes.add(Include::Std::cinttypes);
  headerIncludes.add(Include::Xi::Serialization::EnumSerialization);

  XI_ERROR_PROPAGATE_CATCH(header.printCopyright())
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
  XI_ERROR_PROPAGATE_CATCH(header.printInclude(headerIncludes))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
  XI_ERROR_PROPAGATE_CATCH(header.printOpenNamespaces(enum_.namespace_()->path()))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))

  XI_ERROR_PROPAGATE_CATCH(header.printDocumentation(enum_.documentation()));
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))
  XI_ERROR_PROPAGATE_CATCH(header.print("enum struct "))
  XI_ERROR_PROPAGATE_CATCH(header.print(enum_.name()))
  XI_ERROR_PROPAGATE_CATCH(header.print(" : uint16_t {"))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))
  for (const auto &entry : enum_.entries()) {
    XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))
    XI_ERROR_PROPAGATE_CATCH(header.print(entry.name()))
    XI_ERROR_PROPAGATE_CATCH(header.print(" = "))
    XI_ERROR_PROPAGATE_CATCH(header.print(toString(entry.tag().binary(), Stringify::hexadecimal)))
    XI_ERROR_PROPAGATE_CATCH(header.print(","))
  }
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
  XI_ERROR_PROPAGATE_CATCH(header.print("}; // enum "))
  XI_ERROR_PROPAGATE_CATCH(header.print(enum_.name()))

  for (const auto &entry : enum_.entries()) {
    XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
    XI_ERROR_PROPAGATE_CATCH(header.printDocumentation(entry.documentation(), enum_.name(), entry.name()))
  }

  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
  XI_ERROR_PROPAGATE_CATCH(header.print("XI_SERIALIZATION_ENUM("))
  XI_ERROR_PROPAGATE_CATCH(header.print(enum_.name()))
  XI_ERROR_PROPAGATE_CATCH(header.print(")"))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))

  XI_ERROR_PROPAGATE_CATCH(header.printCloseNamespaces(enum_.namespace_()->path()))

  if (enum_.entries().size() > 0) {
    XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))

    XI_ERROR_PROPAGATE_CATCH(header.print("XI_SERIALIZATION_ENUM_RANGE("))
    XI_ERROR_PROPAGATE_CATCH(header.printTypeSignature(makeTypeSignature(*enumType)))
    XI_ERROR_PROPAGATE_CATCH(header.print(", "))
    XI_ERROR_PROPAGATE_CATCH(header.print(enum_.entries().front().name()))
    XI_ERROR_PROPAGATE_CATCH(header.print(", "))
    XI_ERROR_PROPAGATE_CATCH(header.print(enum_.entries().back().name()))
    XI_ERROR_PROPAGATE_CATCH(header.print(")"))

    for (const auto &entry : enum_.entries()) {
      XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))
      XI_ERROR_PROPAGATE_CATCH(header.print("XI_SERIALIZATION_ENUM_TAG("))
      XI_ERROR_PROPAGATE_CATCH(header.printTypeSignature(makeTypeSignature(*enumType)))
      XI_ERROR_PROPAGATE_CATCH(header.print(", "))
      XI_ERROR_PROPAGATE_CATCH(header.print(entry.name()))
      XI_ERROR_PROPAGATE_CATCH(header.print(", "))
      XI_ERROR_PROPAGATE_CATCH(header.print("\""))
      XI_ERROR_PROPAGATE_CATCH(header.print(entry.tag().text()))
      XI_ERROR_PROPAGATE_CATCH(header.print("\""))
      XI_ERROR_PROPAGATE_CATCH(header.print(")"))
    }
  }

  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))
  XI_ERROR_PROPAGATE_CATCH(header.stream().flush())
  XI_SUCCEED();
}

Result<void> Generator::doOnFlag(const Flag &flag) {
  XI_TRACE("Building flag: {}", flag.name());
  const auto enumType = currentScope().types().search(flag.name());
  Include enumInclude{fileMapper().includeFilePath(*enumType), Include::Kind::ThisLibrary};
  const auto currentNamespace = currentScope().currentNamespace();

  auto headerFile = fileMapper().headerFile(*enumType);
  XI_ERROR_PROPAGATE(headerFile)
  XI_TRACE(" -- Header: {}", headerFile);
  auto includeFile = fileMapper().includeFilePath(*enumType);
  XI_ERROR_PROPAGATE(headerFile)
  XI_TRACE(" -- Include: {}", includeFile);

  auto headerStream = headerFile->openTextForWrite(FileSystem::overwrite);
  XI_ERROR_PROPAGATE(headerStream)
  CppGenerator header{headerStream.take()};

  IncludeCollection headerIncludes{};
  headerIncludes.add(Include::Std::cinttypes);
  headerIncludes.add(Include::Xi::Serialization::FlagSerialization);
  headerIncludes.add(Include::Xi::TypeSafe::Flag);

  XI_ERROR_PROPAGATE_CATCH(header.printCopyright())
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
  XI_ERROR_PROPAGATE_CATCH(header.printInclude(headerIncludes))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
  XI_ERROR_PROPAGATE_CATCH(header.printOpenNamespaces(flag.namespace_()->path()))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))

  XI_ERROR_PROPAGATE_CATCH(header.printDocumentation(flag.documentation()));
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))
  XI_ERROR_PROPAGATE_CATCH(header.print("enum struct "))
  XI_ERROR_PROPAGATE_CATCH(header.print(flag.name()))
  XI_ERROR_PROPAGATE_CATCH(header.print(" : uint16_t {"))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
  XI_ERROR_PROPAGATE_CATCH(header.print("None = 0,"))
  for (const auto &entry : flag.entries()) {
    XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))
    XI_ERROR_PROPAGATE_CATCH(header.print(entry.name()))
    XI_ERROR_PROPAGATE_CATCH(header.print(" = 1 << "))
    XI_ERROR_PROPAGATE_CATCH(header.print(toString(entry.tag().binary() - 1)))
    XI_ERROR_PROPAGATE_CATCH(header.print(","))
  }
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
  XI_ERROR_PROPAGATE_CATCH(header.print("}; // enum "))
  XI_ERROR_PROPAGATE_CATCH(header.print(flag.name()))

  for (const auto &entry : flag.entries()) {
    XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
    XI_ERROR_PROPAGATE_CATCH(header.printDocumentation(entry.documentation(), flag.name(), entry.name()))
  }

  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
  XI_ERROR_PROPAGATE_CATCH(header.print("XI_TYPESAFE_FLAG_MAKE_OPERATIONS("))
  XI_ERROR_PROPAGATE_CATCH(header.print(flag.name()))
  XI_ERROR_PROPAGATE_CATCH(header.print(")"))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))
  XI_ERROR_PROPAGATE_CATCH(header.print("XI_SERIALIZATION_FLAG("))
  XI_ERROR_PROPAGATE_CATCH(header.print(flag.name()))
  XI_ERROR_PROPAGATE_CATCH(header.print(")"))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))

  XI_ERROR_PROPAGATE_CATCH(header.printCloseNamespaces(flag.namespace_()->path()))

  if (flag.entries().size() > 0) {
    XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))

    XI_ERROR_PROPAGATE_CATCH(header.print("XI_SERIALIZATION_FLAG_RANGE("))
    XI_ERROR_PROPAGATE_CATCH(header.printTypeSignature(makeTypeSignature(*enumType)))
    XI_ERROR_PROPAGATE_CATCH(header.print(", "))
    XI_ERROR_PROPAGATE_CATCH(header.print(flag.entries().front().name()))
    XI_ERROR_PROPAGATE_CATCH(header.print(", "))
    XI_ERROR_PROPAGATE_CATCH(header.print(flag.entries().back().name()))
    XI_ERROR_PROPAGATE_CATCH(header.print(")"))

    for (const auto &entry : flag.entries()) {
      XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))
      XI_ERROR_PROPAGATE_CATCH(header.print("XI_SERIALIZATION_FLAG_TAG("))
      XI_ERROR_PROPAGATE_CATCH(header.printTypeSignature(makeTypeSignature(*enumType)))
      XI_ERROR_PROPAGATE_CATCH(header.print(", "))
      XI_ERROR_PROPAGATE_CATCH(header.print(entry.name()))
      XI_ERROR_PROPAGATE_CATCH(header.print(", "))
      XI_ERROR_PROPAGATE_CATCH(header.print("\""))
      XI_ERROR_PROPAGATE_CATCH(header.print(entry.tag().text()))
      XI_ERROR_PROPAGATE_CATCH(header.print("\""))
      XI_ERROR_PROPAGATE_CATCH(header.print(")"))
    }
  }

  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))
  XI_ERROR_PROPAGATE_CATCH(header.stream().flush())
  XI_SUCCEED();
}

Result<void> Generator::doOnContract(const Contract &contract) {
  XI_TRACE("Building contract: {}", contract.name());
  const auto currentNamespace = currentScope().currentNamespace();
  auto headerFile = fileMapper().headerFile(contract.name(), currentNamespace);
  XI_ERROR_PROPAGATE(headerFile)
  XI_TRACE(" -- Header: {}", headerFile);
  auto includeFile = fileMapper().includeFilePath(contract.name(), currentNamespace);
  XI_ERROR_PROPAGATE(headerFile)
  XI_TRACE(" -- Include: {}", includeFile);

  auto headerStream = headerFile->openTextForWrite(FileSystem::overwrite);
  XI_ERROR_PROPAGATE(headerStream)
  CppGenerator header{headerStream.take()};

  IncludeCollection headerIncludes{};
  headerIncludes.add(Include::Xi::Serialization::Serializer);
  if (!(contract.argument() || contract.returnType())) {
    headerIncludes.add(Include::Xi::Global);
  }
  if (contract.argument()) {
    XI_ERROR_PROPAGATE_CATCH(fileMapper().includes(*contract.argument(), headerIncludes))
  }
  if (contract.returnType()) {
    XI_ERROR_PROPAGATE_CATCH(fileMapper().includes(*contract.returnType(), headerIncludes))
  }

  XI_ERROR_PROPAGATE_CATCH(header.printCopyright())
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
  XI_ERROR_PROPAGATE_CATCH(header.printIncludeGuard())
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
  XI_ERROR_PROPAGATE_CATCH(header.printInclude(headerIncludes))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
  XI_ERROR_PROPAGATE_CATCH(header.printOpenNamespaces(contract.namespace_()->path()))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
  XI_ERROR_PROPAGATE_CATCH(header.printBeginStruct(contract.name()))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))

  XI_ERROR_PROPAGATE_CATCH(header.print(contract.name()))
  XI_ERROR_PROPAGATE_CATCH(header.print("() = delete;"))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))

  if (contract.argument()) {
    XI_ERROR_PROPAGATE_CATCH(header.print("using Request = "))
    XI_ERROR_PROPAGATE_CATCH(header.printTypeSignature(makeTypeSignature(*contract.argument()), currentNamespace))
    XI_ERROR_PROPAGATE_CATCH(header.print(";"))
  } else {
    XI_ERROR_PROPAGATE_CATCH(header.print("using Request = Null;"))
  }
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))

  if (contract.returnType()) {
    XI_ERROR_PROPAGATE_CATCH(header.print("using Response = "))
    XI_ERROR_PROPAGATE_CATCH(header.printTypeSignature(makeTypeSignature(*contract.returnType()), currentNamespace))
    XI_ERROR_PROPAGATE_CATCH(header.print(";"))
  } else {
    XI_ERROR_PROPAGATE_CATCH(header.print("using Response = Null;"))
  }

  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
  XI_ERROR_PROPAGATE_CATCH(header.printEndStruct(contract.name()))

  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(2))
  XI_ERROR_PROPAGATE_CATCH(header.printCloseNamespaces(contract.namespace_()->path()))
  XI_ERROR_PROPAGATE_CATCH(header.printNewLine(1))
  XI_ERROR_PROPAGATE_CATCH(header.stream().flush())
  XI_SUCCEED();
}

Result<void> Generator::doOnService(const Service &service) {
  XI_TRACE("Building service: {}", service.name());
  XI_SUCCEED();
}

Result<UniqueGenerator> makeGenerator(const Config &config) {
  XI_ERROR_TRY
  UniqueGenerator reval{new Generator};

  auto fileMapper = makeFileMapper(config.outputPath);
  XI_ERROR_PROPAGATE(fileMapper)

  reval->m_impl.reset(new Generator::_Impl{fileMapper.take()});
  return success(move(reval));

  XI_ERROR_CATCH
}

}  // namespace Cpp
}  // namespace Generator
}  // namespace Idl
}  // namespace Xi
