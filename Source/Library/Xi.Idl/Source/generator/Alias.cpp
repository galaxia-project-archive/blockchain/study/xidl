/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/Alias.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

const std::string &Alias::name() const {
  return m_name;
}

SharedConstNamespace Alias::namespace_() const {
  return m_namespace;
}

const Documentation &Alias::documentation() const {
  return m_doc;
}

const Type &Alias::type() const {
  return m_type;
}

void Alias::setName(const std::string &name_) {
  m_name = name_;
}

void Alias::setNamespace(SharedConstNamespace ns) {
  m_namespace = ns;
}

void Alias::setDocumentation(const Documentation &doc_) {
  m_doc = doc_;
}

void Alias::setType(const Type &type_) {
  m_type = type_;
}

AliasBuilder::AliasBuilder() : m_alias{result_success, Alias{}} {
  /* */
}

AliasBuilder &AliasBuilder::withName(const std::string &name) {
  if (!m_alias.isError()) {
    m_alias->setName(name);
  }
  return *this;
}

AliasBuilder &AliasBuilder::withNamespace(SharedConstNamespace ns) {
  if (!m_alias.isError()) {
    m_alias->setNamespace(ns);
  }
  return *this;
}

AliasBuilder &AliasBuilder::withDocumentation(const Documentation &doc) {
  if (!m_alias.isError()) {
    m_alias->setDocumentation(doc);
  }
  return *this;
}

AliasBuilder &AliasBuilder::withType(const Type &type) {
  if (!m_alias.isError()) {
    m_alias->setType(type);
  }
  return *this;
}

Result<Alias> AliasBuilder::build() {
  return m_alias;
}

Result<Type> makeType(const Alias &alias) {
  return TypeBuilder{}
      .withKind(Type::Kind::Alias)
      .withName(alias.name())
      .withNamespace(alias.namespace_())
      .withIsOptional(false)
      .withData(alias)
      .build();
}

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi
