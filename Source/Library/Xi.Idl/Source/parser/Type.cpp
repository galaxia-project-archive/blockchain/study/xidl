/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Parser/Type.hpp"

#include <utility>
#include <string>

#include <Xi/String/String.hpp>

#include "Xi/Idl/Parser/Keyword.hpp"
#include "Xi/Idl/Parser/Constants.hpp"
#include "Xi/Idl/Parser/ParserError.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

namespace {
class ContainerTypeAutomaton final : public ResultAutomaton<ContainerType> {
 private:
  Atomic m_cached;

 private:
  ContainerTypeAutomaton(Atomic cached, result_type& result) : ResultAutomaton(result), m_cached{std::move(cached)} {
    /* */
  }
  friend std::shared_ptr<ContainerTypeAutomaton> makeContainerTypeAutomaton(Atomic, ContainerType&);

 protected:
  Result<void> doProcessToken(const Token& token) override {
    if (token.value == "+" || token.value == "*") {
      Vector vec{};
      vec.baseType = std::move(m_cached);
      if (token.value == "+") {
        vec.capacityRange = Cardinality::AtLeastOne;
      } else {
        vec.capacityRange = Cardinality::Any;
      }
      m_result = std::move(vec);
    } else {
      Array array{};
      array.baseType = std::move(m_cached);
      auto size = fromString<ArraySize>(token.value);
      XI_ERROR_PROPAGATE(size)
      array.size = *size;
      m_result = std::move(array);
    }
    chain(makeKeywordAutomaton("]"));
    markFinished();
    XI_SUCCEED()
  }
};

std::shared_ptr<ContainerTypeAutomaton> makeContainerTypeAutomaton(Atomic cached, ContainerType& result) {
  std::shared_ptr<ContainerTypeAutomaton> reval{new ContainerTypeAutomaton{std::move(cached), result}};
  reval->chain(makeKeywordAutomaton("["));
  return reval;
}

}  // namespace

Result<void> BasicTypeAutomaton::doProcessToken(const Token& token) {
  if (token.value == "[") {
    m_result = ContainerType{};
    auto child = makeContainerTypeAutomaton(std::move(m_cached), std::get<ContainerType>(m_result));
    markFinished();
    chain(child);
    return child->processToken(token);
  } else {
    m_result = std::move(m_cached);
    return propagateBack(token);
  }
}

std::shared_ptr<BasicTypeAutomaton> makeBasicTypeAutomaton(BasicType& result) {
  std::shared_ptr<BasicTypeAutomaton> reval{new BasicTypeAutomaton{result}};
  reval->chain(makeAtomicAutomaton(reval->m_cached));
  return reval;
}

std::shared_ptr<TypeAutomaton> makeTypeAutomaton(Type& result) {
  std::shared_ptr<TypeAutomaton> reval{new TypeAutomaton{result}};
  reval->chain(makeBasicTypeAutomaton(result.base));
  reval->chain(makeOptionalKeywordAutomaton("?", result.isOptional));
  return reval;
}

Result<void> OptionalTypeAutomaton::doProcessToken(const Token& token) {
  if (token.value == "nothing") {
    m_result = std::nullopt;
    markFinished();
    XI_SUCCEED()
  } else {
    m_result = Type{};
    chain(makeTypeAutomaton(*m_result));
    markFinished();
    return processToken(token);
  }
}

std::shared_ptr<OptionalTypeAutomaton> makeOptionalTypeAutomaton(std::optional<Type>& result) {
  return std::shared_ptr<OptionalTypeAutomaton>{new OptionalTypeAutomaton{result}};
}

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi
