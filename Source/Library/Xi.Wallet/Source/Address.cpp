/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Wallet/Address.hpp"

#include <cstring>

#include <Xi/Encoding/Base58.hh>
#include <Xi/Crypto/Hash/Crc.hpp>
#include <Xi/Serialization/Binary/Binary.hpp>

using Checksum = Xi::Crypto::Hash::Crc::Hash32;

namespace Xi {
namespace Wallet {

Result<Address> Address::parse(const std::string &str) {
  const auto prefixTermination = str.find_first_of('1');
  XI_FAIL_IF(prefixTermination == std::string::npos, AddressError::InvalidPrefix)
  XI_FAIL_IF_NOT(prefixTermination < str.size() - 1, AddressError::InvalidPrefix)
  const auto prefix = AddressPrefix::parse(str.substr(0, prefixTermination));
  XI_ERROR_PROPAGATE(prefix)

  auto blob = Encoding::Base58::decode(str.substr(prefixTermination + 1));
  XI_ERROR_PROPAGATE(blob)
  XI_FAIL_IF(blob->size() < Checksum::bytes(), AddressError::InvalidChecksum);

  ByteVector dataToChecksum{prefix->binary()};
  dataToChecksum.resize(dataToChecksum.size() + blob->size() - Checksum::bytes());
  std::memcpy(dataToChecksum.data() + prefix->binary().size(), blob->data(), blob->size() - Checksum::bytes());

  ConstByteSpan dataSpan{blob->data(), blob->size() - Checksum::bytes()};
  ConstByteSpan checksumSpan{blob->data() + blob->size() - Checksum::bytes(), Checksum::bytes()};
  auto dataChecksum = Checksum::compute(dataToChecksum);
  XI_ERROR_PROPAGATE(dataChecksum)
  XI_FAIL_IF(std::memcmp(checksumSpan.data(), dataChecksum->data(), Checksum::bytes()) != 0,
             AddressError::InvalidChecksum);
  return Serialization::Binary::fromBinary<Address>(dataSpan);
}

std::string Address::stringify() const {
  auto blob = Serialization::Binary::toBinary(*this).takeOrThrow();
  auto checksum = Checksum::compute(blob).takeOrThrow();
  blob.resize(blob.size() + Checksum::bytes());
  std::memcpy(blob.data() + blob.size() - Checksum::bytes(), checksum.data(), Checksum::bytes());
  return Encoding::Base58::encode(blob);
}

const AddressPrefix &Address::prefix() const {
  return m_prefix;
}

void Address::setPrefix(const AddressPrefix &prefix_) {
  m_prefix = prefix_;
}

void Address::setExtension(AddressExtension &extension_) {
  m_extension = extension_;
}

AddressExtension &Address::extension() {
  return m_extension;
}

const AddressExtension &Address::extension() const {
  return m_extension;
}

void Address::setPublicKey(const Crypto::PublicKey &publicKey_) {
  m_publicKey = publicKey_;
}

const Crypto::PublicKey &Address::publicKey() const {
  return m_publicKey;
}

void Address::setNetwork(Config::Network::Type network_) {
  m_network = network_;
}

Config::Network::Type Address::network() const {
  return m_network;
}

XI_SERIALIZATION_COMPLEX_EXTERN_BEGIN(Address)
XI_SERIALIZATION_MEMBER(m_prefix, 0x0001, "prefix")
XI_FAIL_IF_NOT(m_prefix.isValid(), AddressError::InvalidPrefix)
XI_SERIALIZATION_MEMBER(m_network, 0x0002, "network")
XI_SERIALIZATION_MEMBER(m_publicKey, 0x0003, "public_key")
XI_FAIL_IF_NOT(m_publicKey.isValid(), AddressError::InvalidPublicKey)
XI_SERIALIZATION_MEMBER(m_extension, 0x0004, "extension")
XI_SERIALIZATION_COMPLEX_EXTERN_END

}  // namespace Wallet
}  // namespace Xi
