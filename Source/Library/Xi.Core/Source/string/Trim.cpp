/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/String/Trim.hpp"

#include <algorithm>
#include <iterator>

namespace Xi {

std::string trimLeft(std::string_view str) {
  std::string reval{};
  auto begin = std::find_if_not(str.begin(), str.end(), std::isspace);
  reval.reserve(static_cast<std::string::size_type>(std::distance(begin, str.end())));
  std::copy(begin, str.end(), std::back_inserter(reval));
  return reval;
}

void trimLeft(std::string &str, in_place_t) {
  auto end = std::find_if_not(str.begin(), str.end(), std::isspace);
  str.erase(str.begin(), end);
}

std::string trimRight(std::string_view str) {
  std::string reval{};
  size_t i = str.size();
  while (i > 0 && std::isspace(str[i - 1])) {
    i -= 1;
  }
  return std::string{str.data(), i};
}

void trimRight(std::string &str, in_place_t) {
  size_t i = str.size();
  while (i > 0 && std::isspace(str[i - 1])) {
    i -= 1;
  }
  str.resize(i);
}

std::string trim(std::string_view str) {
  auto reval = trimLeft(str);
  trimRight(reval, in_place_v);
  return reval;
}

void trim(std::string &str, in_place_t) {
  trimLeft(str, in_place_v);
  trimRight(str, in_place_v);
}

}  // namespace Xi
