/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Stream/CompoundInputStream.hpp"

#include <utility>

#include "Xi/Stream/StreamError.hpp"

namespace Xi {
namespace Stream {

CompoundInputStream::CompoundInputStream(std::vector<UniqueInputStream> sources) : m_pos{0} {
  for (auto& source : sources) {
    if (source) {
      m_sources.emplace_back(std::move(source));
    }
  }
}

Result<size_t> CompoundInputStream::read(Xi::ByteSpan buffer) {
  XI_ERROR_PROPAGATE(popEndOfStreamSources());
  {
    const auto ec = isEndOfStream();
    XI_ERROR_PROPAGATE(ec)
    XI_FAIL_IF(*ec, StreamError::EndOfStream)
  }
  return m_sources.front()->read(buffer);
}

Result<bool> CompoundInputStream::isEndOfStream() const {
  for (const auto& iSource : m_sources) {
    const auto ec = iSource->isEndOfStream();
    XI_ERROR_PROPAGATE(ec)
    XI_SUCCEED_IF(*ec, false)
  }
  XI_SUCCEED(true)
}

Result<size_t> CompoundInputStream::tell() const {XI_SUCCEED(m_pos)}

Result<Byte> CompoundInputStream::peek() const {
  const auto ec = isEndOfStream();
  XI_ERROR_PROPAGATE(ec)
  XI_FAIL_IF(*ec, StreamError::EndOfStream)
  for (const auto& iSource : m_sources) {
    const auto iec = iSource->isEndOfStream();
    XI_ERROR_PROPAGATE(iec)
    if (!*iec) {
      return iSource->peek();
    }
  }
  XI_FAIL(StreamError::EndOfStream)
}

Result<Byte> CompoundInputStream::take() {
  XI_ERROR_PROPAGATE(popEndOfStreamSources())
  const auto ec = isEndOfStream();
  XI_ERROR_PROPAGATE(ec);
  XI_FAIL_IF(*ec, StreamError::EndOfStream)
  const auto reval = m_sources.front()->take();
  m_pos += 1;
  return reval;
}

Result<void> CompoundInputStream::popEndOfStreamSources() {
  while (!m_sources.empty()) {
    const auto ec = m_sources.front()->isEndOfStream();
    XI_ERROR_PROPAGATE(ec)
    if (*ec) {
      m_sources.pop_front();
    }
    if (const auto iec = m_sources.front()->isEndOfStream(); *iec == true) {
      m_sources.pop_front();
    } else {
      break;
    }
  }
  return success();
}

}  // namespace Stream
}  // namespace Xi
