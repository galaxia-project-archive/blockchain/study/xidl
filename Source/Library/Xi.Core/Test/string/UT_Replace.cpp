/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/String/Replace.hpp>

#define XI_TEST_SUITE Xi_Algorithm_String_Replace

TEST(XI_TEST_SUITE, EmptyCases) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_THAT(replace("", "", "X"), Eq(""));
  EXPECT_THAT(replace("", "_", "X"), Eq(""));
  EXPECT_THAT(replace("X__X", "", "X"), Eq("X__X"));
}

TEST(XI_TEST_SUITE, NeverMatch) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_THAT(replace("^-.-^", "", "X"), Eq("^-.-^"));
  EXPECT_THAT(replace("^-.-^", "^-.-^_", "X"), Eq("^-.-^"));
  EXPECT_THAT(replace("^-.-^", "^_._^", "X"), Eq("^-.-^"));
  EXPECT_THAT(replace("^-.-^", "^_", "X"), Eq("^-.-^"));
}

TEST(XI_TEST_SUITE, FullMatch) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_THAT(replace("^-.-^", "^-.-^", ""), Eq(""));
  EXPECT_THAT(replace("^-.-^", "^-.-^", "X"), Eq("X"));
  EXPECT_THAT(replace("^-.-^", "^-.-^", "^-.-^"), Eq("^-.-^"));
  EXPECT_THAT(replace("^-.-^", "-", "_"), Eq("^_._^"));
  EXPECT_THAT(replace("^-.-^", "^", "X"), Eq("X-.-X"));
}

TEST(XI_TEST_SUITE, PartialMatch) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_THAT(replace("^-.-^", "-", "_"), Eq("^_._^"));
  EXPECT_THAT(replace("^-.-^", "^", "X"), Eq("X-.-X"));
  EXPECT_THAT(replace("-", "-", "^"), Eq("^"));
  EXPECT_THAT(replace("----", "-", "^."), Eq("^.^.^.^."));
  EXPECT_THAT(replace("----", "--", "^."), Eq("^.^."));
}

TEST(XI_TEST_SUITE, SelfReplacement) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_THAT(replace("--------", "-", "-"), Eq("--------"));
  EXPECT_THAT(replace("--------", "-", "--"), Eq("----------------"));
}
