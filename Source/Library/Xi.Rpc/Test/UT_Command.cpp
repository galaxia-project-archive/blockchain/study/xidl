/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/Testing/Result.hpp>
#include <Xi/String/ToString.hpp>
#include <Xi/Rpc/Command.hpp>

#define XI_TEST_SUITE Xi_Rpc_Command

TEST(XI_TEST_SUITE, FromString) {
  using namespace ::testing;
  using namespace ::Xi;
  using namespace ::Xi::Testing;
  using namespace ::Xi::Rpc;

  {
    auto command = fromString<SharedCommand>("");
    EXPECT_TRUE(isFailure(command));
  }

  {
    std::string name{};
    name.resize(Command::MaximumTextTag + 1, 'A');
    auto command = Command::parse(name);
    EXPECT_TRUE(isFailure(command));
  }

  {
    auto command = fromString<SharedCommand>("xi.my.command");
    EXPECT_TRUE(isSuccess(command));
    EXPECT_EQ(toString(**command), "xi.my.command");
  }
}
