/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rpc/RequestPrefix.hpp"

namespace Xi {
namespace Rpc {

Version RequestPrefix::version() const {
  return m_version;
}

void RequestPrefix::setVersion(Version version_) {
  m_version = version_;
}

std::optional<Identifier> RequestPrefix::identifier() const {
  return m_identifier;
}

void RequestPrefix::setIdentifier(std::optional<Identifier> identifier_) {
  m_identifier = identifier_;
}

SharedConstCommand RequestPrefix::command() const {
  return m_command;
}

void RequestPrefix::setCommand(SharedCommand command_) {
  m_command = command_;
}

bool RequestPrefix::isNotification() const {
  return !m_identifier.has_value();
}

XI_SERIALIZATION_COMPLEX_EXTERN_BEGIN(RequestPrefix)
XI_SERIALIZATION_MEMBER(m_version, 0x0001, "version")
XI_SERIALIZATION_MEMBER(m_identifier, 0x0002, "identifier")
XI_SERIALIZATION_MEMBER(m_command, 0x0003, "command")
XI_SERIALIZATION_COMPLEX_EXTERN_END

}  // namespace Rpc
}  // namespace Xi
