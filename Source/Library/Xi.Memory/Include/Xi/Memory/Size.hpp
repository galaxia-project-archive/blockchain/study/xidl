/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <type_traits>

#include <Xi/Algorithm/Math.h>
#include <Xi/TypeSafe/Integral.hpp>

namespace Xi {
namespace Memory {

template <std::uintmax_t _UnitPrefix>
struct Size : TypeSafe::EnableIntegralFromThis<std::uintmax_t, Size<_UnitPrefix>>,
              TypeSafe::EnableIntegralAdditionFromThis<Size<_UnitPrefix>>,
              TypeSafe::EnableIntegralSubtractionFromThis<Size<_UnitPrefix>> {
  static inline constexpr auto unitPrefix() {
    return _UnitPrefix;
  }

  static_assert(isPowerOf2(unitPrefix()), "Unit prefix of memory size must be a multiple of 2 (or 1)");

  using TypeSafe::EnableIntegralFromThis<std::uintmax_t, Size<_UnitPrefix>>::EnableIntegralFromThis;

  template <std::uintmax_t _OtherUnitPrefix>
  operator Size<_OtherUnitPrefix>() const {
    static_assert(_OtherUnitPrefix <= unitPrefix(), "Unit prefix conversion reduces accuracy, use size_cast.");
    return Size<_OtherUnitPrefix>{this->native() * (_OtherUnitPrefix / unitPrefix())};
  }
};

template <typename _OtherSizeT, typename _ThisSizeT>
_OtherSizeT size_cast(_ThisSizeT toCast) {
  return _OtherSizeT{(toCast.native() * _ThisSizeT::unitPrefix()) / _OtherSizeT::unitPrefix()};
}

using Bytes = Size<1>;
using KiloBytes = Size<1024>;
using MegaBytes = Size<1048576>;
using GigaBytes = Size<1073741824>;
using TeraBytes = Size<1099511627776>;
using PetaBytes = Size<1125899906842624>;

}  // namespace Memory
}  // namespace Xi
