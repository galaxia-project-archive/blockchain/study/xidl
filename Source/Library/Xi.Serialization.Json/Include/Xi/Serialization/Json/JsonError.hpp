/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/ErrorCode.hpp>

namespace Xi {
namespace Serialization {
namespace Json {

XI_ERROR_CODE_BEGIN(Json)
XI_ERROR_CODE_VALUE(NoValue, 0x0001)
XI_ERROR_CODE_VALUE(Internal, 0x0002)

XI_ERROR_CODE_VALUE(TypeMissmatchInteger, 0x0010)
XI_ERROR_CODE_VALUE(TypeMissmatchLongInteger, 0x0011)
XI_ERROR_CODE_VALUE(TypeMissmatchUnsignedInteger, 0x0012)
XI_ERROR_CODE_VALUE(TypeMissmatchUnsignedLongInteger, 0x0013)
XI_ERROR_CODE_VALUE(TypeMissmatchNumber, 0x0014)
XI_ERROR_CODE_VALUE(TypeMissmatchLongNumber, 0x0015)
XI_ERROR_CODE_VALUE(TypeMissmatchString, 0x0016)
XI_ERROR_CODE_VALUE(TypeMissmatchBoolean, 0x0017)
XI_ERROR_CODE_VALUE(TypeMissmatchArray, 0x0018)
XI_ERROR_CODE_VALUE(TypeMissmatchObject, 0x0019)

XI_ERROR_CODE_VALUE(IndexOutOfRange, 0x0020)
XI_ERROR_CODE_VALUE(IntegerOutOfBounds, 0x0021)

XI_ERROR_CODE_VALUE(NullTag, 0x0030)
XI_ERROR_CODE_VALUE(FlagOverflow, 0x0031)
XI_ERROR_CODE_VALUE(DuplicateTag, 0x0032)

XI_ERROR_CODE_VALUE(SizeMissmatch, 0x0040)

XI_ERROR_CODE_END(Json, "Serialization::JsonError")

}  // namespace Json
}  // namespace Serialization
}  // namespace Xi
