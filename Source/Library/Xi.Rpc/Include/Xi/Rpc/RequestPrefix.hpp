/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>
#include <Xi/Serialization/Serialization.hpp>

#include "Xi/Rpc/Version.hpp"
#include "Xi/Rpc/Identifier.hpp"
#include "Xi/Rpc/Command.hpp"

namespace Xi {
namespace Rpc {

class RequestPrefix {
 public:
  RequestPrefix();
  XI_DELETE_COPY(RequestPrefix);
  XI_DEFAULT_MOVE(RequestPrefix);
  virtual ~RequestPrefix() = default;

  Version version() const;
  void setVersion(Version version_);

  std::optional<Identifier> identifier() const;
  void setIdentifier(std::optional<Identifier> identifier_);

  SharedConstCommand command() const;
  void setCommand(SharedCommand command_);

  bool isNotification() const;

  XI_SERIALIZATION_COMPLEX_EXTERN()
 private:
  Version m_version;
  std::optional<Identifier> m_identifier;
  SharedCommand m_command;
};

}  // namespace Rpc
}  // namespace Xi
