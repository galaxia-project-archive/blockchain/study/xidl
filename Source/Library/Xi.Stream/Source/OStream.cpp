/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Stream/OStream.hpp"

#include <utility>

#include <Xi/Exceptions.hpp>

#include "Xi/Stream/StreamError.hpp"

namespace Xi {
namespace Stream {

OStream::OStream(Ownership<std::ostream> dest) : m_dest{std::move(dest)} {
  /* */
}

OStream::~OStream() {
  /* */
}

Result<size_t> OStream::write(ConstByteSpan buffer) {
  try {
    m_dest.get().write(reinterpret_cast<const char*>(buffer.data()), static_cast<std::streamsize>(buffer.size()));
    return success(buffer.size());
  } catch (...) {
    XI_FAIL(std::current_exception())
  }
}

Result<void> OStream::flush() {
  try {
    m_dest.get().flush();
    return success();
  } catch (...) {
    XI_FAIL(std::current_exception())
  }
}

}  // namespace Stream
}  // namespace Xi
