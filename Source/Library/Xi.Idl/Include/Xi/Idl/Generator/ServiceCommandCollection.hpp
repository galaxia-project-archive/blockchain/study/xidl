/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <vector>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>

#include "Xi/Idl/Generator/ServiceCommand.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

class ServiceCommandCollection {
 private:
  using container_type = std::vector<ServiceCommand>;

 public:
  using const_iterator = container_type::const_iterator;

 public:
  ServiceCommandCollection() = default;
  XI_DEFAULT_COPY(ServiceCommandCollection);
  XI_DEFAULT_MOVE(ServiceCommandCollection);
  ~ServiceCommandCollection() = default;

 public:
  Result<void> add(ServiceCommand command);

  const_iterator begin() const;
  const_iterator end() const;
  const_iterator cbegin() const;
  const_iterator cend() const;

 private:
  container_type m_commands;
};

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi
