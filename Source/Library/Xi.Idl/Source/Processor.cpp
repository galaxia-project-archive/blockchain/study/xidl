/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Processor.hpp"

#include <algorithm>
#include <fstream>

#include <Xi/FileSystem.h>
#include <Xi/ScopeExit.hpp>
#include <Xi/Log/Log.hpp>
#include <Xi/String/ToString.hpp>
#include <Xi/Stream/IStream.hpp>

#include "Xi/Idl/Parser/Parser.hpp"
#include "Xi/Idl/Generator/Primitive.hpp"
#include "Xi/Idl/Generator/Array.hpp"
#include "Xi/Idl/Generator/Vector.hpp"

XI_LOGGER("Idl/Processor")

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Idl, Processor)
XI_ERROR_CODE_DESC(CircularDependency, "imports contain a circular dependency")
XI_ERROR_CODE_DESC(ReferenceNotFound, "type reference could not be resolved")
XI_ERROR_CODE_DESC(NamespaceNotFound, "referenced namespace not found")
XI_ERROR_CODE_DESC(ServeReferenceAmbigious, "service serve reference is ambigious, may be a contract or service")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Idl {

Result<void> Processor::process(const std::string &file, Generator::Generator &gen) {
  Processor processor{};
  return processor.doProcess(file, gen);
}

Result<void> Processor::doProcess(const std::string &file, Generator::Generator &gen) {
  auto rootContent = importFile(file, "");
  XI_ERROR_PROPAGATE(rootContent)
  XI_ERROR_PROPAGATE_CATCH(buildScope((*rootContent)->content, gen));
  return success();
}

Result<SharedCacheEntry> Processor::importFile(const std::string &filepath, const std::string &currentFilepath) {
  XI_ERROR_TRY
  // Path Resolution
  std::string relativeDir = "";
  if (!currentFilepath.empty()) {
    auto ec = FileSystem::directory(currentFilepath);
    XI_ERROR_PROPAGATE(ec)
    relativeDir = ec.take();
  }
  auto resolvedFilepath = FileSystem::searchFile(filepath, ".xidl", relativeDir);
  XI_ERROR_PROPAGATE(resolvedFilepath)

  // Do we have a circular dependency?
  {
    const auto circleSearch = std::find_if(begin(m_importStack), end(m_importStack), [&](const auto &i) {
      return FileSystem::equivalent(i, *resolvedFilepath).takeOrThrow();
    });
    XI_FAIL_IF(circleSearch != end(m_importStack), ProcessorError::CircularDependency)
  }

  m_importStack.push_back(*resolvedFilepath);
  XI_SCOPE_EXIT([this]() { this->m_importStack.pop_back(); })

  // Caching
  {
    const auto ec = m_cache.search(*resolvedFilepath);
    XI_ERROR_PROPAGATE(ec);
    XI_SUCCEED_IF(*ec, *ec)
  }

  const auto cacheEntry = std::make_shared<CacheEntry>();
  cacheEntry->filepath = *resolvedFilepath;
  // Parse
  {
    Parser::Parser rootParser{};
    std::ifstream rootFile{*resolvedFilepath};
    Stream::IStream stream{makeDelegatedOwnership<std::istream>(rootFile)};
    auto content = rootParser.parse(stream);
    XI_ERROR_PROPAGATE(content)
    cacheEntry->content = content.take();
  }

  // Recurse
  const auto filePathDirectory = FileSystem::directory(*resolvedFilepath);
  XI_ERROR_PROPAGATE(filePathDirectory);
  for (const auto &import : cacheEntry->content.imports) {
    const auto ec = importFile(import.filepath, *filePathDirectory);
    XI_ERROR_PROPAGATE(ec)
  }

  // Import Scope
  {
    auto ec = buildScope(cacheEntry->content);
    XI_ERROR_PROPAGATE(ec)
    cacheEntry->scope = ec.take();
  }

  // Persist Cache
  XI_ERROR_PROPAGATE_CATCH(m_cache.insert(cacheEntry));

  return success(cacheEntry);
  XI_ERROR_CATCH
}

Result<Generator::Scope> Processor::buildScope(const Parser::FileContent &content) {
  XI_ERROR_TRY
  Generator::Generator gen{};
  return buildScope(content, gen);
  XI_ERROR_CATCH
}

Result<Generator::Scope> Processor::buildScope(const Parser::FileContent &content, Generator::Generator &gen) {
  XI_ERROR_TRY
  Generator::Scope reval{};

  XI_ERROR_PROPAGATE_CATCH(gen.begin(reval))

  // Namespace
  auto ns = reval.namespaces().addNamespace(content.namespace_.nested);
  XI_ERROR_PROPAGATE_CATCH(ns);
  XI_ERROR_PROPAGATE_CATCH(gen.openNamespace(*ns));

  // Imports
  for (const auto &fimport : content.imports) {
    const auto importEntry = m_cache.search(fimport.filepath);
    XI_ERROR_PROPAGATE(importEntry)
    XI_EXCEPTIONAL_IF_NOT(NotFoundError, *importEntry)
    // clang-format off
    const auto import = Generator::ImportBuilder{}
      .withAlias(fimport.alias)
      .withFilepath((*importEntry)->filepath)
      .withScope((*importEntry)->scope)
      .build();
    // clang-format on
    XI_ERROR_PROPAGATE(import);
    XI_ERROR_PROPAGATE_CATCH(gen.onImport(*import))
  }

  // Type Declarations
  for (const auto &typeDeclaration : content.typeDeclarations) {
    const auto decl_doc = buildDocumentation(typeDeclaration.doc);
    // Package
    if (auto parser_package = std::get_if<Parser::Package>(std::addressof(typeDeclaration.value))) {
      Generator::PackageBuilder pkgBuilder{};
      for (const auto &parser_field : parser_package->fields) {
        const auto type = buildType(parser_field.type, reval);
        XI_ERROR_PROPAGATE(type)
        const auto tag = buildTag(parser_field.tag);
        XI_ERROR_PROPAGATE(tag)
        const auto fieldDoc = buildDocumentation(parser_field.doc);
        XI_ERROR_PROPAGATE(fieldDoc)
        // clang-format off
        auto field = Generator::FieldBuilder{}
          .withName(parser_field.name)
          .withTag(*tag)
          .withType(*type)
          .withDocumentation(*fieldDoc)
          .build();
        // clang-format on
        XI_ERROR_PROPAGATE(field)
        pkgBuilder.withField(field.take());
      }

      pkgBuilder.withName(parser_package->name);
      pkgBuilder.withNamespace(reval.currentNamespace());
      pkgBuilder.withDocumentation(*decl_doc);
      if (parser_package->inheritance) {
        auto inheritedType = resolveTypeReference(*parser_package->inheritance, reval);
        XI_ERROR_PROPAGATE(inheritedType)
        pkgBuilder.withInheritance(inheritedType.take());
      } else {
        pkgBuilder.withInheritance(std::nullopt);
      }

      auto package = pkgBuilder.build();
      XI_ERROR_PROPAGATE(package);
      XI_ERROR_PROPAGATE_CATCH(gen.onPackage(*package));
    }  // Variant
    else if (auto parser_variant = std::get_if<Parser::Variant>(std::addressof(typeDeclaration.value))) {
      Generator::VariantBuilder varBuilder{};

      for (const auto &variant_field : parser_variant->fields) {
        const auto type = buildType(variant_field.type, reval);
        XI_ERROR_PROPAGATE(type)
        const auto tag = buildTag(variant_field.tag);
        XI_ERROR_PROPAGATE(tag)
        const auto fieldDoc = buildDocumentation(variant_field.doc);
        XI_ERROR_PROPAGATE(fieldDoc)
        // clang-format off
        auto field = Generator::FieldBuilder{}
          .withName(variant_field.name)
          .withTag(*tag)
          .withType(*type)
          .withDocumentation(*fieldDoc)
          .build();
        // clang-format on
        XI_ERROR_PROPAGATE(field)
        varBuilder.withField(field.take());
      }

      varBuilder.withName(parser_variant->name);
      varBuilder.withNamespace(reval.currentNamespace());
      varBuilder.withDocumentation(*decl_doc);

      auto variant = varBuilder.build();
      XI_ERROR_PROPAGATE(variant);
      XI_ERROR_PROPAGATE_CATCH(gen.onVariant(*variant));
    }  // Enum
    else if (auto parser_enum = std::get_if<Parser::Enum>(std::addressof(typeDeclaration.value))) {
      Generator::EnumBuilder enumBuilder{};
      for (const auto &parser_enum_entry : parser_enum->values) {
        const auto idoc = buildDocumentation(parser_enum_entry.doc);
        XI_ERROR_PROPAGATE(idoc)
        const auto tag = buildTag(parser_enum_entry.tag);
        XI_ERROR_PROPAGATE(tag)
        // clang-format off
        auto enumEntry = Generator::EnumEntryBuilder{}
          .withName(parser_enum_entry.name)
          .withTag(*tag)
          .withDocumentation(*idoc)
          .build();
        // clang-format on
        XI_ERROR_PROPAGATE(enumEntry)
        enumBuilder.withEntry(enumEntry.take());
      }

      enumBuilder.withName(parser_enum->name);
      enumBuilder.withNamespace(reval.currentNamespace());
      enumBuilder.withDocumentation(*decl_doc);
      auto enum_ = enumBuilder.build();
      XI_ERROR_PROPAGATE(enum_)
      XI_ERROR_PROPAGATE_CATCH(gen.onEnum(*enum_))
    }  // Flag
    else if (auto parser_flag = std::get_if<Parser::Flag>(std::addressof(typeDeclaration.value))) {
      Generator::FlagBuilder flagBuilder{};

      for (const auto &parser_flag_entry : parser_flag->values) {
        const auto idoc = buildDocumentation(parser_flag_entry.doc);
        XI_ERROR_PROPAGATE(idoc)
        const auto tag = buildTag(parser_flag_entry.tag);
        XI_ERROR_PROPAGATE(tag)
        // clang-format off
        auto flagEntry = Generator::FlagEntryBuilder{}
          .withName(parser_flag_entry.name)
          .withTag(*tag)
          .withDocumentation(*idoc)
          .build();
        // clang-format on
        XI_ERROR_PROPAGATE(flagEntry)
        flagBuilder.withEntry(flagEntry.take());
      }

      flagBuilder.withName(parser_flag->name);
      flagBuilder.withNamespace(reval.currentNamespace());
      flagBuilder.withDocumentation(*decl_doc);
      auto flag = flagBuilder.build();
      XI_ERROR_PROPAGATE(flag)
      XI_ERROR_PROPAGATE_CATCH(gen.onFlag(*flag))
    }  // Alias
    else if (auto parser_alias = std::get_if<Parser::Alias>(std::addressof(typeDeclaration.value))) {
      auto type = buildType(parser_alias->type, reval);
      XI_ERROR_PROPAGATE(type)
      // clang-format off
      auto alias = Generator::AliasBuilder{}
        .withName(parser_alias->name)
        .withNamespace(reval.currentNamespace())
        .withDocumentation(*decl_doc)
        .withType(type.take())
        .build();
      // clang-format on
      XI_ERROR_PROPAGATE(alias)
      XI_ERROR_PROPAGATE_CATCH(gen.onAlias(*alias))
    }  // Typedef
    else if (auto parser_typedef = std::get_if<Parser::Typedef>(std::addressof(typeDeclaration.value))) {
      auto type = buildType(parser_typedef->type, reval);
      XI_ERROR_PROPAGATE(type)
      // clang-format off
      auto typedef_ = Generator::TypedefBuilder{}
       .withName(parser_typedef->name)
       .withNamespace(reval.currentNamespace())
       .withDocumentation(*decl_doc)
       .withType(type.take())
       .build();
      // clang-format on
      XI_ERROR_PROPAGATE(typedef_)
      XI_ERROR_PROPAGATE_CATCH(gen.onTypedef(*typedef_))
    } else {
      XI_EXCEPTIONAL(InvalidVariantTypeError)
    }
  }

  // Contracts
  for (const auto &parser_contract : content.contracts) {
    Generator::ContractBuilder builder{};
    builder.withName(parser_contract.name);
    builder.withNamespace(reval.currentNamespace());

    const auto contractDoc = buildDocumentation(parser_contract.doc);
    XI_ERROR_PROPAGATE(contractDoc)
    builder.withDocumentation(*contractDoc);

    if (parser_contract.argument) {
      auto type = buildType(*parser_contract.argument, reval);
      XI_ERROR_PROPAGATE(type)
      builder.withArgument(type.take());
    } else {
      builder.withArgument(std::nullopt);
    }

    if (parser_contract.returnType) {
      auto type = buildType(*parser_contract.returnType, reval);
      XI_ERROR_PROPAGATE(type)
      builder.withReturnType(type.take());
    } else {
      builder.withReturnType(std::nullopt);
    }

    auto contract = builder.build();
    XI_ERROR_PROPAGATE(contract)
    XI_ERROR_PROPAGATE_CATCH(gen.onContract(contract.take()));
  }

  // Services
  for (const auto &parser_service : content.services) {
    Generator::ServiceBuilder builder{};
    builder.withName(parser_service.name);
    builder.withNamespace(reval.currentNamespace());

    auto serviceDoc = buildDocumentation(parser_service.doc);
    XI_ERROR_PROPAGATE(serviceDoc);
    builder.withDocumentation(*serviceDoc);

    for (const auto &parser_command : parser_service.commands) {
      Generator::ServiceCommandBuilder cmdBuilder{};

      const auto commandTag = buildTag(parser_command.tag);
      XI_ERROR_PROPAGATE(commandTag)
      cmdBuilder.withTag(*commandTag);

      auto commandDoc = buildDocumentation(parser_command.doc);
      XI_ERROR_PROPAGATE(commandDoc);
      cmdBuilder.withDocumentation(*commandDoc);

      auto cmdService = resolveServiceReference(parser_command.contract, reval);
      auto cmdContract = resolveContractReference(parser_command.contract, reval);
      XI_FAIL_IF(cmdService.isValue() && cmdContract.isValue(), ProcessorError::ServeReferenceAmbigious)
      if (cmdService.isValue()) {
        cmdBuilder.withKind(Generator::ServiceCommand::Kind::Service).withData(cmdService.take());
      } else if (cmdContract.isValue()) {
        cmdBuilder.withKind(Generator::ServiceCommand::Kind::Contract).withData(cmdContract.take());
      } else {
        XI_FAIL(ProcessorError::ReferenceNotFound)
      }
      auto command = cmdBuilder.build();
      XI_ERROR_PROPAGATE(command)
      builder.withCommand(command.take());
    }
    auto service = builder.build();
    XI_ERROR_PROPAGATE(service)
    XI_ERROR_PROPAGATE_CATCH(gen.onService(service.take()))
  }

  auto rootNs = reval.currentNamespace();
  XI_ERROR_PROPAGATE_CATCH(gen.closeNamespace(*ns))
  XI_ERROR_PROPAGATE_CATCH(gen.end(reval))

  reval.namespaces().setCurrentNamespace(rootNs);
  return success(std::move(reval));
  XI_ERROR_CATCH
}

Result<Generator::Type> Xi::Idl::Processor::buildType(const Parser::Type &type, Generator::Scope &scope) {
  XI_ERROR_TRY
  Generator::TypeBuilder builder{/* */};
  builder.withIsOptional(type.isOptional);
  if (auto parser_atomic = std::get_if<Parser::Atomic>(std::addressof(type.base))) {
    if (auto parser_reference = std::get_if<Parser::Reference>(parser_atomic)) {
      auto resolvedType = resolveTypeReference(*parser_reference, scope);
      if (resolvedType.isError()) {
        XI_ERROR("Error resolving reference '{}': {}", *parser_reference, resolvedType.error())
        XI_RETURN_EC(resolvedType.error());
      }
      XI_SUCCEED(resolvedType->toOptional(type.isOptional))
    } else {
      builder.withKind(Generator::Type::Kind::Primitive);
      Generator::PrimitiveBuilder primitiveBuilder{};
      if (std::get_if<Parser::Byte>(parser_atomic)) {
        primitiveBuilder.withKind(Generator::Primitive::Kind::Byte);
      } else if (std::get_if<Parser::Int8>(parser_atomic)) {
        primitiveBuilder.withKind(Generator::Primitive::Kind::Int8);
      } else if (std::get_if<Parser::Int16>(parser_atomic)) {
        primitiveBuilder.withKind(Generator::Primitive::Kind::Int16);
      } else if (std::get_if<Parser::Int32>(parser_atomic)) {
        primitiveBuilder.withKind(Generator::Primitive::Kind::Int32);
      } else if (std::get_if<Parser::Int64>(parser_atomic)) {
        primitiveBuilder.withKind(Generator::Primitive::Kind::Int64);
      } else if (std::get_if<Parser::UInt8>(parser_atomic)) {
        primitiveBuilder.withKind(Generator::Primitive::Kind::UInt8);
      } else if (std::get_if<Parser::UInt16>(parser_atomic)) {
        primitiveBuilder.withKind(Generator::Primitive::Kind::UInt16);
      } else if (std::get_if<Parser::UInt32>(parser_atomic)) {
        primitiveBuilder.withKind(Generator::Primitive::Kind::UInt32);
      } else if (std::get_if<Parser::UInt64>(parser_atomic)) {
        primitiveBuilder.withKind(Generator::Primitive::Kind::UInt64);
      } else if (std::get_if<Parser::Float16>(parser_atomic)) {
        primitiveBuilder.withKind(Generator::Primitive::Kind::Float16);
      } else if (std::get_if<Parser::Float32>(parser_atomic)) {
        primitiveBuilder.withKind(Generator::Primitive::Kind::Float32);
      } else if (std::get_if<Parser::Float64>(parser_atomic)) {
        primitiveBuilder.withKind(Generator::Primitive::Kind::Float64);
      } else if (std::get_if<Parser::Boolean>(parser_atomic)) {
        primitiveBuilder.withKind(Generator::Primitive::Kind::Boolean);
      } else if (std::get_if<Parser::String>(parser_atomic)) {
        primitiveBuilder.withKind(Generator::Primitive::Kind::String);
      } else {
        XI_EXCEPTIONAL(InvalidVariantTypeError)
      }
      auto primitive = primitiveBuilder.build();
      XI_ERROR_PROPAGATE(primitive)
      builder.withData(primitive.take());
    }
  } else if (const auto parser_container = std::get_if<Parser::ContainerType>(std::addressof(type.base))) {
    if (auto parser_array = std::get_if<Parser::Array>(parser_container)) {
      builder.withKind(Generator::Type::Kind::Array);
      Generator::ArrayBuilder arrayBuilder{};
      auto arrayType = buildType(Parser::Type{parser_array->baseType, false}, scope);
      XI_ERROR_PROPAGATE(arrayType)
      arrayBuilder.withType(arrayType.take());
      arrayBuilder.withSize(parser_array->size);
      auto array = arrayBuilder.build();
      XI_ERROR_PROPAGATE(array);
      builder.withData(array.take());
    } else if (auto parser_vector = std::get_if<Parser::Vector>(parser_container)) {
      builder.withKind(Generator::Type::Kind::Vector);
      Generator::VectorBuilder vectorBuilder{};
      auto arrayType = buildType(Parser::Type{parser_vector->baseType, false}, scope);
      XI_ERROR_PROPAGATE(arrayType)
      vectorBuilder.withType(arrayType.take());
      vectorBuilder.withMinimumSize(parser_vector->capacityRange.minimum);
      vectorBuilder.withMaximumSize(parser_vector->capacityRange.maximum);
      auto vector = vectorBuilder.build();
      XI_ERROR_PROPAGATE(vector);
      builder.withData(vector.take());
    } else {
      XI_EXCEPTIONAL(InvalidVariantTypeError)
    }
  } else {
    XI_EXCEPTIONAL(InvalidVariantTypeError)
  }
  return builder.build();
  XI_ERROR_CATCH
}

Result<Generator::Documentation> Processor::buildDocumentation(const Parser::Documentation &doc) {
  Generator::DocumentationBuilder builder{};
  Parser::DocumentationTag previousTag{Parser::NoDocumentationTag{}};
  for (const auto &iDocEntry : doc) {
    auto tag = iDocEntry.tag;
    if (std::holds_alternative<Parser::NoDocumentationTag>(tag)) {
      tag = previousTag;
    }
    if (std::holds_alternative<Parser::NoDocumentationTag>(tag)) {
      tag = Parser::BriefDocumentationTag{};
    }

    if (std::holds_alternative<Parser::BriefDocumentationTag>(tag)) {
      builder.withBrief(iDocEntry.content);
    } else if (std::holds_alternative<Parser::DetailedDocumentationTag>(tag)) {
      builder.withDetailed(iDocEntry.content);
    } else if (std::holds_alternative<Parser::ExampleDocumentationTag>(tag)) {
      builder.withExample(iDocEntry.content);
    } else {
      XI_EXCEPTIONAL(InvalidVariantTypeError)
    }

    previousTag = tag;
  }
  return builder.build();
}

Result<Generator::SharedConstNamespace> Processor::resolveNamespace(const Parser::Reference &ref,
                                                                    const Generator::Scope &scope) {
  XI_FAIL_IF(ref.nested.empty(), ProcessorError::ReferenceNotFound)
  XI_SUCCEED_IF(ref.nested.size() == 1, scope.currentNamespace())
  const auto ns = scope.namespaces().resolveNamespace(makeConstSpan(ref.nested.data(), ref.nested.size() - 1));
  XI_FAIL_IF_NOT(ns, ProcessorError::NamespaceNotFound)
  XI_SUCCEED(ns)
}

Result<Generator::Type> Processor::resolveTypeReference(const Parser::Reference &ref, const Generator::Scope &scope) {
  const auto resolvedNs = resolveNamespace(ref, scope);
  XI_ERROR_PROPAGATE(resolvedNs)
  auto ns = *resolvedNs;
  while (ns) {
    const auto search = scope.types(ns).search(ref.nested.back());
    XI_SUCCEED_IF(search.has_value(), *search);
    ns = ns->parent().lock();
  }
  XI_FAIL(ProcessorError::ReferenceNotFound)
}

Result<Generator::Contract> Processor::resolveContractReference(const Parser::Reference &ref,
                                                                const Generator::Scope &scope) {
  const auto resolvedNs = resolveNamespace(ref, scope);
  XI_ERROR_PROPAGATE(resolvedNs)
  auto ns = *resolvedNs;
  while (ns) {
    const auto search = scope.contracts(ns).search(ref.nested.back());
    XI_SUCCEED_IF(search.has_value(), *search);
    ns = ns->parent().lock();
  }
  XI_FAIL(ProcessorError::ReferenceNotFound)
}

Result<Generator::Service> Processor::resolveServiceReference(const Parser::Reference &ref,
                                                              const Generator::Scope &scope) {
  const auto resolvedNs = resolveNamespace(ref, scope);
  XI_ERROR_PROPAGATE(resolvedNs)
  auto ns = *resolvedNs;
  while (ns) {
    const auto search = scope.services(ns).search(ref.nested.back());
    XI_SUCCEED_IF(search.has_value(), *search);
    ns = ns->parent().lock();
  }
  XI_FAIL(ProcessorError::ReferenceNotFound)
}

Result<Generator::SerializationTag> Processor::buildTag(const Parser::Tag &tag) {
  return Generator::SerializationTagBuilder{}.withText(tag.text).withBinary(tag.binary).build();
}

}  // namespace Idl
}  // namespace Xi
