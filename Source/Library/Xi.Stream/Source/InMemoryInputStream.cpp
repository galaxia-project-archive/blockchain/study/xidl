/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Stream/InMemoryInputStream.hpp"

#include <cstring>
#include <algorithm>

#include "Xi/Stream/StreamError.hpp"

namespace Xi {
namespace Stream {

InMemoryInputStream::InMemoryInputStream(ConstByteSpan source) : m_source{source}, m_pos{0} {
}

Result<size_t> InMemoryInputStream::read(ByteSpan buffer) {
  const auto isEos = isEndOfStream();
  XI_ERROR_PROPAGATE(isEos)
  if (*isEos) {
    return success<size_t>(0);
  }

  size_t size = std::min(buffer.size(), m_source.size() - m_pos);
  std::memcpy(buffer.data(), m_source.data() + m_pos, static_cast<size_t>(size));
  m_pos += size;
  return success(static_cast<size_t>(size));
}

Result<bool> InMemoryInputStream::isEndOfStream() const {
  const auto pos = tell();
  XI_ERROR_PROPAGATE(pos)
  return success(*pos == m_source.size());
}

Result<size_t> InMemoryInputStream::tell() const {
  return success(m_pos);
}

Result<Byte> InMemoryInputStream::peek() const {
  const auto isEos = isEndOfStream();
  XI_ERROR_PROPAGATE(isEos)
  XI_FAIL_IF(*isEos, StreamError::EndOfStream)
  return success(m_source[m_pos]);
}

Result<Byte> InMemoryInputStream::take() {
  const auto isEos = isEndOfStream();
  XI_ERROR_PROPAGATE(isEos)
  XI_FAIL_IF(*isEos, StreamError::EndOfStream)
  return success(m_source[m_pos++]);
}

}  // namespace Stream
}  // namespace Xi
