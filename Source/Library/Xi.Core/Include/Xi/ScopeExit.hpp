/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <utility>

#include <Xi/Global.hh>

namespace Xi {

template <typename _FunctorT>
class ScopeExit {
 private:
  _FunctorT m_functor;

 public:
  explicit ScopeExit(_FunctorT&& functor) : m_functor(std::forward<_FunctorT>(functor)) {
    /* */
  }

  ~ScopeExit() {
    try {
      m_functor();
    } catch (...) {
      XI_PRINT_EC("scope desctructor threw.");
    }
  }
};

template <typename _FunctorT>
ScopeExit<_FunctorT> makeScopeExit(_FunctorT&& functor) {
  return ScopeExit<_FunctorT>(std::forward<_FunctorT>(functor));
}

}  // namespace Xi

#define XI_SCOPE_EXIT(STMT) \
  [[maybe_unused]] const auto XI_CONCATENATE(__ScopeExit, __LINE__) = ::Xi::makeScopeExit(STMT);
