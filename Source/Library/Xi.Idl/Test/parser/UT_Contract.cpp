/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/Testing/Result.hpp>
#include <Xi/Idl/Parser/Contract.hpp>

#define XI_TEST_SUITE Xi_Idl_Parser_Contract

class XI_TEST_SUITE : public ::testing::Test {
 public:
  Xi::Idl::Parser::ContractVector contracts;
  std::shared_ptr<Xi::Idl::Parser::ContractsAutomaton> automaton;

  XI_TEST_SUITE() : contracts{}, automaton{nullptr} {
    /* */
  }

  void process(const std::string& content) {
    contracts = Xi::Idl::Parser::ContractVector{/* */};
    automaton = Xi::Idl::Parser::makeContractsAutomaton(contracts);
    auto ec = automaton->process(content);
    ASSERT_THAT(ec, Xi::Testing::IsSuccess());
  }
};

TEST_F(XI_TEST_SUITE, WellFormed) {
  using namespace Xi::Idl::Parser;
  using namespace ::testing;

  process(R"__(contract get_info(nothing) : boolean?;
contract push_tx(
  #Transaction?
) : nothing;)__");

  ASSERT_THAT(contracts, SizeIs(Eq(2)));
  {
    const auto& contract = contracts.at(0);
    EXPECT_EQ(contract.name, "get_info");
    EXPECT_FALSE(contract.argument);

    ASSERT_TRUE(contract.returnType.has_value());
    EXPECT_TRUE(contract.returnType->isOptional);
    ASSERT_TRUE(std::holds_alternative<Atomic>(contract.returnType->base));
    const auto& atomic = std::get<Atomic>(contract.returnType->base);
    EXPECT_TRUE(std::holds_alternative<Boolean>(atomic));
  }
  {
    const auto& contract = contracts.at(1);
    EXPECT_EQ(contract.name, "push_tx");
    EXPECT_FALSE(contract.returnType.has_value());
    ASSERT_TRUE(contract.argument);
    {
      const auto& arg = *contract.argument;
      ASSERT_TRUE(std::holds_alternative<Atomic>(arg.base));
      const auto& atomic = std::get<Atomic>(arg.base);
      ASSERT_TRUE(std::holds_alternative<Reference>(atomic));
      const auto& ref = std::get<Reference>(atomic);
      ASSERT_THAT(ref.nested, SizeIs(Eq(1)));
      EXPECT_THAT(ref.nested[0], Eq("Transaction"));
    }
  }
}
