/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/Field.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

const std::string &Field::name() const {
  return m_name;
}

void Field::setName(const std::string &name) {
  m_name = name;
}

const SerializationTag &Field::tag() const {
  return m_tag;
}

void Field::setTag(const SerializationTag &tag) {
  m_tag = tag;
}

const Type &Field::type() const {
  return m_type;
}

const Documentation &Field::documentation() const {
  return m_doc;
}

void Field::setType(const Type &t) {
  m_type = t;
}

void Field::setDocumentation(const Documentation &doc_) {
  m_doc = doc_;
}

FieldBuilder::FieldBuilder() {
  m_field = Result<Field>{result_success, Field{}};
}

FieldBuilder &FieldBuilder::withName(const std::string &name) {
  if (!m_field.isError()) {
    m_field->setName(name);
  }
  return *this;
}

FieldBuilder &FieldBuilder::withTag(const SerializationTag &tag) {
  if (!m_field.isError()) {
    m_field->setTag(tag);
  }
  return *this;
}

FieldBuilder &FieldBuilder::withType(const Type &type) {
  if (!m_field.isError()) {
    m_field->setType(type);
  }
  return *this;
}

FieldBuilder &FieldBuilder::withDocumentation(const Documentation &doc) {
  if (!m_field.isError()) {
    m_field->setDocumentation(doc);
  }
  return *this;
}

Result<Field> FieldBuilder::build() {
  return m_field;
}

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi
