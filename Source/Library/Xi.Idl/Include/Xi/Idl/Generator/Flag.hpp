/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string>
#include <vector>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>

#include "Xi/Idl/Generator/Namespace.hpp"
#include "Xi/Idl/Generator/Documentation.hpp"
#include "Xi/Idl/Generator/Type.hpp"
#include "Xi/Idl/Generator/FlagEntry.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

class Flag {
 public:
  XI_DEFAULT_COPY(Flag);
  XI_DEFAULT_MOVE(Flag);

  const std::string& name() const;
  SharedConstNamespace namespace_() const;
  const Documentation& documentation() const;
  const std::vector<FlagEntry> entries() const;

 private:
  Flag() = default;

  void setName(const std::string& name_);
  void setNamespace(SharedConstNamespace ns);
  void setDocumentation(const Documentation& doc_);
  std::vector<FlagEntry>& entries();

  friend class FlagBuilder;

 private:
  std::string m_name;
  SharedConstNamespace m_namespace;
  Documentation m_doc;
  std::vector<FlagEntry> m_entries;
};

class FlagBuilder {
 public:
  FlagBuilder();
  XI_DELETE_COPY(FlagBuilder);
  XI_DELETE_MOVE(FlagBuilder);

  FlagBuilder& withName(const std::string& name);
  FlagBuilder& withNamespace(SharedConstNamespace ns);
  FlagBuilder& withDocumentation(const Documentation& doc);
  FlagBuilder& withEntry(const FlagEntry& entry);

  Result<Flag> build();

 private:
  Result<Flag> m_flag;
};

Result<Type> makeType(const Flag& flag);

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi
