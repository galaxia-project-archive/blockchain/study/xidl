/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string>
#include <utility>
#include <cinttypes>
#include <type_traits>

#include "Xi/Result.hpp"
#include "Xi/TypeTrait/IsDetected.hpp"

#include "Xi/String/Parse/Parse.hpp"

namespace Xi {

template <typename _ValueT, typename... _Ts, std::enable_if_t<has_parse_member_expression_v<_ValueT, _Ts...>, int> = 0>
Result<_ValueT> fromString(const std::string& str, _Ts&&... args) {
  return _ValueT::parse(str, std::forward<_Ts>(args)...);
}

template <typename _ValueT, typename... _Ts, std::enable_if_t<has_parse_expression_v<_ValueT, _Ts...>, int> = 0>
Result<_ValueT> fromString(const std::string& str, _Ts&&... args) {
  _ValueT value;
  XI_ERROR_PROPAGATE_CATCH(parse(str, value, std::forward<_Ts>(args)...))
  if constexpr (std::is_compound_v<_ValueT>) {
    XI_SUCCEED(std::move(value))
  } else {
    XI_SUCCEED(value)
  }
}

}  // namespace Xi
