/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <filesystem>
#include <string>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>

namespace Xi {
namespace FileSystem {

class File;
class Directory;

/*!
 * \brief The Entry class wraps a subentry of a directory, which is a file or a directory.
 *
 * On querying subentries or searching for a specific entry it is undetermined whether this entry is a file or a
 * directory. That is why this wrapper is used a a return value of common queries to enforce the caller to check
 * the expected integrity.
 */
class Entry {
 public:
  /*!
   * \brief The Kind enum describes the kind of filesystem entry.
   *
   * Thile library only considers files and directories.
   */
  enum struct Kind {
    /// This entry points to a file.
    File,
    /// This entry points to a directory.
    Directory,
  };

 public:
  /*!
   * \brief kind Queries the kind of resource for this entry.
   * \return An error if the query failed otherwise the actual type.
   */
  Result<Kind> kind() const;

  /*!
   * \brief isFile checks if the given entity is a file.
   * \return True if the entry is a file othwerise false.
   *
   * \note On symbolic links the path is resolved first in order to determmine the underlying type.
   */
  Result<bool> isFile() const;

  /*!
   * \brief isDirectory checks if the given entity is a directory.
   * \return True if the entry is a directory othwerise false.
   *
   * \note On symbolic links the path is resolved first in order to determmine the underlying type.
   */
  Result<bool> isDirectory() const;

  /*!
   * \brief asFile Interprets this entry as a file.
   * \return A File object corresponding to this entry.
   */
  Result<File> asFile() const;

  /*!
   * \brief asDirectory Interprets this entry as a directory.
   * \return A Directory object corresponding to this entry.
   */
  Result<Directory> asDirectory() const;

  /*!
   * \brief toString Construct a full path representation of this entry.
   * \return Full filepath of this entry.
   */
  std::string stringify() const;

 private:
  /*!
   * \brief Entry construct a new entry without any checks.
   * \param path The path to this entry.
   */
  explicit Entry(const std::filesystem::path& path);

  friend class Directory;
  friend class EntryIterator;
  friend Result<Entry> makeEntry(const std::filesystem::path&);

 private:
  /// Stores the entry filesystem path.
  std::filesystem::path m_path;
};

/*!
 * \brief makeEntry Construct a new filesystem entry for a given path.
 * \param path The path to the filesystem entry.
 * \return The entry if exists and no error occurend on evaluation.
 */
Result<Entry> makeEntry(const std::filesystem::path& path);

}  // namespace FileSystem
}  // namespace Xi
