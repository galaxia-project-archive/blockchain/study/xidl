# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_SOURCE_LIBRARY_XI_APP)
  return()
endif()
set(CMAKE_XI_SOURCE_LIBRARY_XI_APP TRUE)

xi_include(Xi/Make/Library)
xi_include(Extern/RocksDb)
xi_include(Package/Boost)
xi_include(Package/CxxOpts)
xi_include(Source/Library/Legacy/Serialization)
xi_include(Source/Library/Legacy/Common)
xi_include(Source/Library/Legacy/CommonCli)
xi_include(Source/Library/Legacy/P2p)
xi_include(Source/Library/Legacy/CryptoNoteCore)
xi_include(Source/Library/Legacy/CryptoNoteProtocol)
xi_include(Source/Library/Legacy/NodeRpcProxy)
xi_include(Source/Library/Legacy/NodeInProcess)
xi_include(Source/Library/Legacy/BlockchainExplorer)
xi_include(Source/Library/Xi/Core)
xi_include(Source/Library/Xi/Resource)
xi_include(Source/Library/Xi/VersionInfo)
xi_include(Source/Library/Xi/Log/Log)
xi_include(Source/Library/Xi/Log/Discord)
xi_include(Source/Library/Xi/CrashHandler)
xi_include(Source/Library/Xi/Serialization)
xi_include(Source/Library/Xi/Serialization/Console)
xi_include(Source/Library/Xi/VersionInfo)
xi_include(Resource/License)
xi_include(Resource/Checkpoint)

xi_make_library(
  Xi.App

  PUBLIC_LIBRARIES
    Xi::Core
    Xi::VersionInfo
    Xi::Resource
    Xi::Log::Discord
    Xi::CrashHandler
    Xi::Serialization

    Legacy::Serialization
    Legacy::Common
    Legacy::CommonCLI
    Legacy::P2p
    Legacy::CryptoNoteCore
    Legacy::CryptoNoteProtocol
    Legacy::NodeRpcProxy
    Legacy::NodeInProcess
    Legacy::BlockchainExplorer

    Boost::boost
    cxxopts::cxxopts

  PRIVATE_LIBRARIES
    Xi::Log
    Xi::Resource
    Xi::Serialization::Console

    Resource::License

    rocksdb::rocksdb
    rang::rang
)
