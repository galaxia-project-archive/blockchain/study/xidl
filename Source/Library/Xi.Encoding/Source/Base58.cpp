/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Encoding/Base58.hh"

#include <utility>
#include <cstring>
#include <iterator>

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Encoding::Base58, Decode)
XI_ERROR_CODE_DESC(NullArgument, "a null argument was provided to the inner implementation")
XI_ERROR_CODE_DESC(InvalidCharacter, "base58 encoding contains an illegal character")
XI_ERROR_CODE_DESC(InvalidSize, "base58 encoding size is not dividable by 4")
XI_ERROR_CODE_DESC(NoneCanonical, "base58 is formed none canonical")
XI_ERROR_CODE_DESC(SizeMissmatch, "base58 encoding size is not expected size")
XI_ERROR_CODE_DESC(Internal, "base58 encoding failed internally")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Encoding {
namespace Base58 {

std::string encode(Xi::ConstByteSpan raw) {
  std::string reval{};
  size_t size = xi_encoding_base58_encode_max_length(raw.size());
  reval.resize(size);
  xi_encoding_base58_encode(reval.data(), std::addressof(size), raw.data(), raw.size());
  reval.resize(size);
  return reval;
}

Result<ByteVector> decode(const std::string &raw) {
  ByteVector reval{};
  size_t size = xi_encoding_base58_decode_max_length(raw.size());
  reval.resize(size, 0);
  const auto ec = xi_encoding_base58_decode(reval.data(), std::addressof(size), raw.data(), raw.size());
  if (size < reval.size()) {
    reval.erase(reval.begin(), std::next(reval.begin(), static_cast<ByteVector::difference_type>(reval.size() - size)));
  }
  if (ec != XI_RETURN_CODE_SUCCESS) {
    switch (ec) {
      case XI_BASE58_DECODE_INVALID_CHAR:
        XI_FAIL(DecodeError::InvalidCharacter)
      case XI_BASE58_DECODE_INVALID_SIZE:
        XI_FAIL(DecodeError::InvalidSize)
      case XI_BASE58_DECODE_NONE_CANONICAL:
        XI_FAIL(DecodeError::NoneCanonical)
      default:
        XI_FAIL(DecodeError::Internal);
    }
  }
  XI_SUCCEED(std::move(reval))
}

Result<void> decodeStrict(const std::string &raw, Xi::ByteSpan out) {
  auto data = decode(raw);
  XI_ERROR_PROPAGATE(data);
  XI_FAIL_IF_NOT(data->size() == out.size(), DecodeError::SizeMissmatch);
  std::memcpy(out.data(), data->data(), out.size());
  XI_SUCCEED()
}

}  // namespace Base58
}  // namespace Encoding
}  // namespace Xi
