/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string>

#include <Xi/Global.hh>

namespace Xi {
namespace Idl {
namespace Generator {
namespace Cpp {

class Include {
 public:
  /// Describes the kind of include to determine ordering and enclosing path style
  enum struct Kind {
    /// Part of the std library, included first in <>-brackets.
    StandardLibrary = 1,
    /// Is an external dependency, will be embraced by ExternalIncludePush/Pop and usese <>-brackets.
    ExternalLibrary = 2,
    /// An internal depdency, but not the current library, will be embraced by <>-brackets.
    InternalLibrary = 3,
    /// Header is part of the current library, will be embraced by ""
    ThisLibrary = 4,
    /// Is included from a source file that corresponds to this header file.
    SourceHeader = 0,
  };

 public:
  struct Std {
    Std() = delete;

    static const Include cinttypes;
    static const Include array;
    static const Include vector;
    static const Include string;
    static const Include variant;
    static const Include optional;
    static const Include utility;
  };

  struct Xi {
    Xi() = delete;

    static const Include Global;
    static const Include Byte;
    static const Include Result;
    static const Include ErrorCode;

    struct Serialization {
      Serialization() = delete;

      static const Include Serializer;
      static const Include Complex;
      static const Include ArraySerialization;
      static const Include VectorSerialization;
      static const Include OptionalSerialization;
      static const Include VariantSerialization;
      static const Include EnumSerialization;
      static const Include FlagSerialization;
    };

    struct TypeSafe {
      TypeSafe() = delete;

      static const Include Flag;
    };

    struct Rpc {
      Rpc() = delete;

      static const Include IServiceProvider;
    };
  };

 public:
  explicit Include(const std::string& path, const Kind kind);
  XI_DEFAULT_COPY(Include);
  XI_DEFAULT_MOVE(Include);
  ~Include() = default;

  /// returns a valid #include macro usage line for c++
  std::string toIncludeStatement() const;

  const std::string& path() const;
  Kind kind() const;

  bool operator==(const Include& rhs) const;
  bool operator!=(const Include& rhs) const;
  bool operator<(const Include& rhs) const;

 private:
  std::string m_path;
  Kind m_kind;
};

}  // namespace Cpp
}  // namespace Generator
}  // namespace Idl
}  // namespace Xi
