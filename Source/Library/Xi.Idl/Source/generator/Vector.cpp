/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/Vector.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

const Type &Vector::type() const {
  return m_type;
}

VectorSize Vector::minimumSize() const {
  return m_minimumSize;
}

VectorSize Vector::maximumSize() const {
  return m_maximumSize;
}

void Vector::setType(const Type &type_) {
  m_type = type_;
}

void Vector::setMinimumSize(VectorSize size_) {
  m_minimumSize = size_;
}

void Vector::setMaximumSize(VectorSize size_) {
  m_maximumSize = size_;
}

VectorBuilder::VectorBuilder() : m_vector{result_success, Vector{/* */}} {
  /* */
}

VectorBuilder &VectorBuilder::withType(const Type &type) {
  if (!m_vector.isError()) {
    m_vector->setType(type);
  }
  return *this;
}

VectorBuilder &VectorBuilder::withMinimumSize(VectorSize size) {
  if (!m_vector.isError()) {
    m_vector->setMinimumSize(size);
  }
  return *this;
}

VectorBuilder &VectorBuilder::withMaximumSize(VectorSize size) {
  if (!m_vector.isError()) {
    m_vector->setMaximumSize(size);
  }
  return *this;
}

Result<Vector> VectorBuilder::build() {
  return m_vector;
}

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi
