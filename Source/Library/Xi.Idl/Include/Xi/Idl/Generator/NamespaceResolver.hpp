/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <map>

#include <Xi/Global.hh>
#include <Xi/Span.hpp>
#include <Xi/Result.hpp>
#include <Xi/ErrorCode.hpp>

#include "Xi/Idl/Generator/Namespace.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

XI_ERROR_CODE_BEGIN(NamespaceResolver)
XI_ERROR_CODE_VALUE(AliasAmbigious, 0x0001)
XI_ERROR_CODE_VALUE(AliasOverload, 0x0002)
XI_ERROR_CODE_VALUE(NamespaceAmbigious, 0x0003)
XI_ERROR_CODE_END(NamespaceResolver, "error managing the namespace resolver")

class NamespaceResolver {
 public:
  Result<SharedConstNamespace> addNamespace(ConstSpan<std::string> path);
  Result<SharedConstNamespace> addAlias(const std::string& alias, SharedConstNamespace ns);
  Result<SharedConstNamespace> addAlias(const std::string& alias, ConstSpan<std::string> path);

  SharedConstNamespace searchRoot(const std::string& name) const;
  SharedConstNamespace searchAlias(const std::string& name) const;
  SharedConstNamespace searchNamespace(const std::string& name) const;
  SharedConstNamespace searchNamespace(ConstSpan<std::string> path) const;
  SharedConstNamespace resolveNamespace(const std::string& name) const;
  SharedConstNamespace resolveNamespace(ConstSpan<std::string> path) const;

  SharedConstNamespace currentNamespace() const;
  void setCurrentNamespace(SharedConstNamespace ns);

  Result<void> import(const NamespaceResolver& other);

 private:
  Result<SharedNamespace> addMutableNamespace(ConstSpan<std::string> path);
  Result<void> import(SharedConstNamespace ns);

 private:
  std::map<std::string, SharedNamespace> m_rootNamespaces;
  std::map<std::string, SharedConstNamespace> m_alias;
  SharedConstNamespace m_currentNamespace;
};

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Idl::Generator, NamespaceResolver)
