/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/String/Replace.hpp"

#include <algorithm>
#include <iterator>

namespace Xi {

std::string replace(std::string_view str, std::string_view toReplace, std::string_view replacement) {
  if (str.empty()) {
    return "";
  }
  if (toReplace.empty() || toReplace.size() > str.size()) {
    return std::string{str.data(), str.size()};
  }

  std::string reval{};
  reval.reserve(str.size());

  size_t pos = 0;
  size_t lastCopy = 0;
  size_t matchPos = 0;

  while (pos < str.size()) {
    if (str[pos] == toReplace[matchPos]) {
      matchPos += 1;
      if (!(matchPos < toReplace.size())) {
        std::copy(str.data() + lastCopy, str.data() + pos + 1 - toReplace.size(), std::back_inserter(reval));
        std::copy(replacement.begin(), replacement.end(), std::back_inserter(reval));
        lastCopy = pos + 1;
        matchPos = 0;
      }
    } else {
      matchPos = 0;
    }

    pos += 1;
  }

  if (lastCopy < pos) {
    std::copy(str.data() + lastCopy, str.data() + pos, std::back_inserter(reval));
  }

  return reval;
}

}  // namespace Xi
