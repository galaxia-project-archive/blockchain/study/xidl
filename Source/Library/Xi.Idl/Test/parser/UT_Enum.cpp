/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/Testing/Result.hpp>
#include <Xi/Idl/Parser/Enum.hpp>

#define XI_TEST_SUITE Xi_Idl_Parser_Enum

class XI_TEST_SUITE : public ::testing::Test {
 public:
  Xi::Idl::Parser::Enum enum_;
  std::shared_ptr<Xi::Idl::Parser::EnumAutomaton> automaton;

  XI_TEST_SUITE() : enum_{}, automaton{nullptr} {
    /* */
  }

  void process(const std::string& content) {
    enum_ = Xi::Idl::Parser::Enum{/* */};
    automaton = Xi::Idl::Parser::makeEnumAutomaton(enum_);
    auto ec = automaton->process(content);
    ASSERT_THAT(ec, Xi::Testing::IsSuccess());
  }
};

TEST_F(XI_TEST_SUITE, WellFormed) {
  using namespace Xi::Idl::Parser;
  using namespace ::testing;

  process(R"__(
enum TransactionContainer {
  Main (1;main);
  Alternative (2;alternative);
  Pool (3;pool);
}
)__");

  EXPECT_THAT(enum_.name, Eq("TransactionContainer"));
  ASSERT_THAT(enum_.values, SizeIs(Eq(3)));
  {
    const auto& value = enum_.values.at(0);
    EXPECT_THAT(value.name, Eq("Main"));
    EXPECT_THAT(value.tag.binary, Eq(1));
    EXPECT_THAT(value.tag.text, Eq("main"));
  }
  {
    const auto& value = enum_.values.at(1);
    EXPECT_THAT(value.name, Eq("Alternative"));
    EXPECT_THAT(value.tag.binary, Eq(2));
    EXPECT_THAT(value.tag.text, Eq("alternative"));
  }
  {
    const auto& value = enum_.values.at(2);
    EXPECT_THAT(value.name, Eq("Pool"));
    EXPECT_THAT(value.tag.binary, Eq(3));
    EXPECT_THAT(value.tag.text, Eq("pool"));
  }
}
