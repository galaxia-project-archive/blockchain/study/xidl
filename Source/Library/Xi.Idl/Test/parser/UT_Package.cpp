/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/Testing/Result.hpp>
#include <Xi/Idl/Parser/Package.hpp>

#define XI_TEST_SUITE Xi_Idl_Parser_Package

class XI_TEST_SUITE : public ::testing::Test {
 public:
  Xi::Idl::Parser::Package package;
  std::shared_ptr<Xi::Idl::Parser::PackageAutomaton> automaton;

  XI_TEST_SUITE() : package{}, automaton{nullptr} {
    /* */
  }

  void process(const std::string& content) {
    package = Xi::Idl::Parser::Package{/* */};
    automaton = Xi::Idl::Parser::makePackageAutomaton(package);
    auto ec = automaton->process(content);
    ASSERT_THAT(ec, Xi::Testing::IsSuccess());
  }
};

TEST_F(XI_TEST_SUITE, WellFormed) {
  using namespace Xi::Idl::Parser;
  using namespace ::testing;

  process(R"__(
package Block extends #BlockHeader {
  Transactions (1;transactions) : #Transaction[*];
  CumulativeFees (2;cumulative_fees) : uint64;
}
)__");

  EXPECT_THAT(package.name, Eq("Block"));
  ASSERT_TRUE(package.inheritance.has_value());
  ASSERT_THAT(package.inheritance->nested, SizeIs(Eq(1)));
  EXPECT_THAT(package.inheritance->nested[0], Eq("BlockHeader"));

  ASSERT_THAT(package.fields, SizeIs(Eq(2)));
  {
    const auto& field = package.fields.at(0);
    EXPECT_THAT(field.name, Eq("Transactions"));
    EXPECT_THAT(field.tag.binary, Eq(1));
    EXPECT_THAT(field.tag.text, Eq("transactions"));
  }
  {
    const auto& field = package.fields.at(1);
    EXPECT_THAT(field.name, Eq("CumulativeFees"));
    EXPECT_THAT(field.tag.binary, Eq(2));
    EXPECT_THAT(field.tag.text, Eq("cumulative_fees"));
  }

  process(R"__(
package Block {
  Transactions (1;transactions) : #Transaction[*];
  CumulativeFees (2;cumulative_fees) : uint64;
}
)__");

  EXPECT_THAT(package.name, Eq("Block"));
  EXPECT_FALSE(package.inheritance.has_value());

  ASSERT_THAT(package.fields, SizeIs(Eq(2)));
  {
    const auto& field = package.fields.at(0);
    EXPECT_THAT(field.name, Eq("Transactions"));
    EXPECT_THAT(field.tag.binary, Eq(1));
    EXPECT_THAT(field.tag.text, Eq("transactions"));
  }
  {
    const auto& field = package.fields.at(1);
    EXPECT_THAT(field.name, Eq("CumulativeFees"));
    EXPECT_THAT(field.tag.binary, Eq(2));
    EXPECT_THAT(field.tag.text, Eq("cumulative_fees"));
  }
}
