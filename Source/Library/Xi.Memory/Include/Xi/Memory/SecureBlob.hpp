/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include "Xi/Blob.hpp"
#include "Xi/Memory/Clear.hh"

namespace Xi {
namespace Memory {

template <typename _StructT, size_t _SizeV>
struct EnableSecureBlobFromThis : EnableBlobFromThis<_StructT, _SizeV> {
  using EnableBlobFromThis<_StructT, _SizeV>::EnableBlobFromThis;

  void clear() {
    ::Xi::Memory::secureClear(this->mutableSpan());
  }

  ~EnableSecureBlobFromThis() {
    this->clear();
  }
};

}  // namespace Memory
}  // namespace Xi
