/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>

#include "Xi/Idl/Generator/SerializationTag.hpp"
#include "Xi/Idl/Generator/Documentation.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

class EnumEntry {
 public:
  const std::string& name() const;
  const SerializationTag& tag() const;
  const Documentation& documentation() const;

 private:
  EnumEntry() = default;

  void setName(const std::string& name_);
  void setTag(const SerializationTag& tag_);
  void setDocumentation(const Documentation& doc_);

  friend class EnumEntryBuilder;
  friend class Enum;

 private:
  std::string m_name;
  SerializationTag m_tag;
  Documentation m_doc;
};

class EnumEntryBuilder {
 public:
  EnumEntryBuilder();
  XI_DELETE_COPY(EnumEntryBuilder);
  XI_DELETE_MOVE(EnumEntryBuilder);

  EnumEntryBuilder& withName(const std::string& name);
  EnumEntryBuilder& withTag(const SerializationTag& tag);
  EnumEntryBuilder& withDocumentation(const Documentation& doc);

  Result<EnumEntry> build();

 private:
  Result<EnumEntry> m_entry;
};

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi
