/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Cache.hpp"

#include <algorithm>

#include <Xi/Exceptions.hpp>
#include <Xi/FileSystem.h>

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Idl, Cache)
XI_ERROR_CODE_DESC(AlreadyExists, "entry already exists in cache")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Idl {

Result<bool> Cache::contains(const std::string& filepath) const {
  XI_ERROR_TRY
  const auto ec = search(filepath);
  XI_ERROR_PROPAGATE(ec);
  return success(*ec != nullptr);
  XI_ERROR_CATCH
}

Result<SharedCacheEntry> Cache::search(const std::string& filepath) const {
  XI_ERROR_TRY
  auto entrySearch = std::find_if(begin(m_entries), end(m_entries), [=](const auto entry) {
    XI_EXCEPTIONAL_IF_NOT(NullArgumentError, entry)
    return FileSystem::equivalent(filepath, entry->filepath).takeOrThrow();
  });
  if (entrySearch == end(m_entries)) {
    return emplaceSuccess<SharedCacheEntry>(nullptr);
  } else {
    return emplaceSuccess<SharedCacheEntry>(*entrySearch);
  }
  XI_ERROR_CATCH
}

Result<void> Cache::insert(SharedCacheEntry entry) {
  XI_ERROR_TRY
  XI_EXCEPTIONAL_IF_NOT(NullArgumentError, entry)

  {
    const auto ec = contains(entry->filepath);
    XI_ERROR_PROPAGATE(ec)
    XI_FAIL_IF(*ec, CacheError::AlreadyExists)
  }
  m_entries.emplace_back(entry);

  return success();
  XI_ERROR_CATCH
}

}  // namespace Idl
}  // namespace Xi
