/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <array>
#include <utility>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>
#include <Xi/ErrorCode.hpp>

#include "Xi/Idl/CacheEntry.hpp"

namespace Xi {
namespace Idl {

XI_ERROR_CODE_BEGIN(Cache)
XI_ERROR_CODE_VALUE(AlreadyExists, 0x0001)
XI_ERROR_CODE_END(Cache, "idl processor cache error")

/// filepaths may be already resolved
class Cache {
 public:
  Result<bool> contains(const std::string& filepath) const;
  Result<SharedCacheEntry> search(const std::string& filepath) const;
  Result<void> insert(SharedCacheEntry entry);

 private:
  std::vector<SharedCacheEntry> m_entries;
};

}  // namespace Idl
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Idl, Cache)
