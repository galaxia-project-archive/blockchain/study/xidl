﻿#include "Xi/Config/Network.h"

#include <Xi/Exceptions.hpp>
#include <Xi/Resource/Resource.hpp>
#include <Xi/Network/Protocol.hpp>
#include <Xi/Network/Port.hpp>
#include <Xi/VersionInfo/Version.hpp>

Xi::Config::Network::Type Xi::Config::Network::defaultNetworkType() {
  const auto channel = VersionInfo::Version::project().channel();
  if (!channel) {
    return Type::LocalTestNet;
  }
  switch (*channel) {
    case VersionInfo::Channel::Stable:
      return Type::MainNet;
    case VersionInfo::Channel::Beta:
      return Type::StageNet;
    case VersionInfo::Channel::Edge:
      return Type::TestNet;
    case VersionInfo::Channel::Clutter:
      return Type::LocalTestNet;
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

std::string Xi::Config::Network::defaultNetwork() {
  using namespace Xi::Resource;

  switch (defaultNetworkType()) {
    case Type::LocalTestNet:
      return resourcePath("chains/galaxia/local");
    case Type::TestNet:
      return resourcePath("chains/galaxia/test");
    case Type::StageNet:
      exceptional<NotFoundError>("staging network is currently not available");
    case Type::MainNet:
      return resourcePath("chains/galaxia");
  }

  exceptional<InvalidEnumValueError>("Unknown network type.");
}

std::string Xi::Config::Network::checkpoints(const Xi::Config::Network::Type network) {
  using namespace Xi::Resource;

  switch (network) {
    case Type::LocalTestNet:
      return resourcePath("checkpoints/galaxia/local");
    case Type::TestNet:
      return resourcePath("checkpoints/galaxia/test");
    case Type::StageNet:
      return resourcePath("checkpoints/galaxia/staging");
    case Type::MainNet:
      return resourcePath("checkpoints/galaxia");
  }

  exceptional<InvalidEnumValueError>("Unknown network type.");
}

uint16_t Xi::Config::Network::Configuration::p2pDefaultPort() {
  return Xi::Network::Port::fromProtocol(Xi::Network::Protocol::Xip).valueOrThrow().native();
}

uint16_t Xi::Config::Network::Configuration::rpcDefaultPort() {
  return Xi::Network::Port::fromProtocol(Xi::Network::Protocol::Xi).valueOrThrow().native();
}
