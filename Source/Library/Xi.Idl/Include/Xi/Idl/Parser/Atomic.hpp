/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <vector>
#include <variant>

#include "Xi/Idl/Parser/Identifier.hpp"
#include "Xi/Idl/Parser/Reference.hpp"
#include "Xi/Idl/Parser/Automaton.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

struct Byte {};
struct Int8 {};
struct Int16 {};
struct Int32 {};
struct Int64 {};
struct UInt8 {};
struct UInt16 {};
struct UInt32 {};
struct UInt64 {};
struct Float16 {};
struct Float32 {};
struct Float64 {};
struct Boolean {};
struct String {};

// clang-format off
using Atomic = std::variant<
  Byte,
  Int8,
  Int16,
  Int32,
  Int64,
  UInt8,
  UInt16,
  UInt32,
  UInt64,
  Float16,
  Float32,
  Float64,
  Boolean,
  String,
  Reference
>;
// clang-format on

class AtomicAutomaton final : public ResultAutomaton<Atomic> {
 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<AtomicAutomaton> makeAtomicAutomaton(Atomic&);

 protected:
  Result<void> doProcessToken(const Token& token) override;
};

std::shared_ptr<AtomicAutomaton> makeAtomicAutomaton(Atomic& result);

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi
