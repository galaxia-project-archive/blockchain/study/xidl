/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <iostream>
#include <algorithm>
#include <vector>
#include <sstream>
#include <fstream>

#include <Xi/Extern/Push.hh>
#include <cxxopts.hpp>
#include <Xi/Extern/Pop.hh>

#include <Xi/Exceptions.hpp>
#include <Xi/ScopeExit.hpp>
#include <Xi/Log/Log.hpp>
#include <Xi/String/String.hpp>
#include <Xi/Encoding/Base16.hh>
#include <Xi/Resource/Resource.hpp>
#include <Xi/Crypto/FastHash.hpp>
#include <Xi/Crypto/Random/Random.hh>
#include <Xi/FileSystem/FileSystem.hpp>
#include <Xi/Serialization/Serialization.hpp>
#include <Xi/Serialization/Yaml/Yaml.hpp>
#include <Xi/Serialization/Binary/Binary.hpp>

XI_LOGGER("ResourceBuilder")

namespace XiResourceBuilder {

const std::string Copyright = R"__(
/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */
)__";

const std::string Header = R"__(
namespace {
)__";

const std::string Footer = R"__(
}
)__";

struct Options {
  std::vector<std::string> inputs;
  std::string output;
};

struct ResourceEntity {
  std::string name;
  std::string path{};
  Xi::Resource::Type type{};

  XI_SERIALIZATION_COMPLEX_BEGIN()
  XI_SERIALIZATION_MEMBER(name, 0x0001, "name")
  XI_SERIALIZATION_MEMBER(path, 0x0002, "path")
  XI_SERIALIZATION_MEMBER(type, 0x0003, "type")
  XI_SERIALIZATION_COMPLEX_END
};

struct ResourceDefinition {
  std::string _namespace;
  std::string identifier;
  std::string prefix{};
  std::string root{};
  std::vector<ResourceEntity> entities{};

  XI_SERIALIZATION_COMPLEX_BEGIN()
  XI_SERIALIZATION_MEMBER(_namespace, 0x0001, "namespace")
  XI_SERIALIZATION_MEMBER(identifier, 0x0002, "identifier")
  XI_SERIALIZATION_MEMBER(prefix, 0x0003, "prefix")
  XI_SERIALIZATION_MEMBER(root, 0x0004, "root")
  XI_SERIALIZATION_MEMBER(entities, 0x0005, "entities")
  XI_SERIALIZATION_COMPLEX_END
};

std::string entityIdentifier(const std::string& path) {
  return std::string{"__resource_"} +
         Xi::toString(Xi::Crypto::FastHash::compute(Xi::asConstByteSpan(path)).takeOrThrow());
}

std::string wrapText(const std::string& text, const std::string& id) {
  std::stringstream builder{};
  builder << "static const std::string " << id << " = [](){\n";
  builder << R"_(
  std::stringstream builder{};
)_";
  const size_t StringLengthLimit = 16380 - 64;
  for (size_t i = 0; i * StringLengthLimit < text.size(); ++i) {
    const size_t offset = i * StringLengthLimit;
    const size_t count = std::min(StringLengthLimit, text.size() - offset);
    std::string itext = text.substr(offset, count);

    std::string seperator{};
    do {
      Xi::ByteArray<8> seperatorBytes{};
      (void)Xi::Crypto::Random::generate(seperatorBytes, Xi::asConstByteSpan(seperator));
      seperator = Xi::Encoding::Base16::encode(Xi::asConstByteSpan(seperatorBytes.data(), seperatorBytes.size()));
    } while (itext.find(seperator) != std::string::npos);
    builder << "\nbuilder << R\"" << seperator << "(";
    builder << itext;
    builder << ")" << seperator << "\";\n";
  }
  builder << "\nreturn builder.str();\n}();";
  return builder.str();
}

}  // namespace XiResourceBuilder

int main(int argc, char** argv) {
  using namespace Xi;
  using namespace Xi::Resource;
  using namespace XiResourceBuilder;
  using Xi::Crypto::FastHash;

  try {
    Options options{};
    cxxopts::Options cli{"Xi.Resource.Builder", "Generates source files to embed resources into the application."};
    // clang-format off
    cli.add_options("")
      ("i,inputs", "the file to embed", cxxopts::value(options.inputs))
      ("o,output", "the file to embed", cxxopts::value(options.output))
    ;
    // clang-format on
    cli.parse(argc, argv);

    auto outputDir = FileSystem::makeDirectory(options.output).takeOrThrow();
    outputDir.createRegularIfNotExists().throwOnError();

    for (const auto& input : options.inputs) {
      XI_INFO("Processing '{}'...", input)
      auto inputFile = FileSystem::makeFile(input).takeOrThrow();
      auto yaml = inputFile.readAllText().takeOrThrow();
      auto resources = Serialization::Yaml::fromYaml<ResourceDefinition>(yaml).takeOrThrow();
      auto rootdir = FileSystem::makeDirectory(resources.root).takeOrThrow();

      const auto ns = split(resources._namespace, ":", make_copy_v);

      auto tempOutHeader = FileSystem::makeTemporaryFile().takeOrThrow();
      XI_SCOPE_EXIT([&tempOutHeader]() { tempOutHeader.remove().throwOnError(); });
      auto tempOutSource = FileSystem::makeTemporaryFile().takeOrThrow();
      XI_SCOPE_EXIT([&tempOutSource]() { tempOutSource.remove().throwOnError(); });

      auto includeDir = outputDir.relativeDirectory("Include").takeOrThrow();
      auto headersDir = includeDir.relativeDirectory(join(ns, FileSystem::Directory::Seperator)).takeOrThrow();
      headersDir.createRegularIfNotExists().throwOnError();

      auto sourceDir = outputDir.relativeDirectory("Source").takeOrThrow();
      sourceDir.createRegularIfNotExists().throwOnError();

      {
        std::ofstream headerBuilder{tempOutHeader.absolute().takeOrThrow(), std::ios::trunc};
        std::ofstream sourceBuilder{tempOutSource.absolute().takeOrThrow(), std::ios::trunc};

        headerBuilder << Copyright << "\n#pragma once\n";
        sourceBuilder << Copyright << "\n";
        sourceBuilder << "#include \"" << join(ns, "/") << "/" << resources.identifier + ".hpp\"\n\n";
        sourceBuilder << "#include <mutex>\n"
                      << "#include <atomic>\n"
                      << "#include <thread>\n\n"
                      << "#include <sstream>\n\n";
        sourceBuilder << "#include <Xi/Resource/Resource.hpp>\n\n";
        for (const auto& ins : ns) {
          headerBuilder << "namespace " << ins << " {\n";
          sourceBuilder << "namespace " << ins << " {\n";
        }

        headerBuilder << "\nvoid load" << resources.identifier << "();\n";
        sourceBuilder << "\n";

        std::stringstream initBuilder{};
        initBuilder << "void load" << resources.identifier << "() {\n"
                    << R"__(
  static std::mutex __guard{};
  static std::atomic_bool __isInitialized{false};

  if(__isInitialized) {
    return;
  }

  std::lock_guard<std::mutex> lock{__guard};
  (void)lock;

  if(__isInitialized) {
    return;
  }

  auto& container = const_cast<::Xi::Resource::Container&>(::Xi::Resource::embedded());

)__";

        const auto inputFileDir = inputFile.directory();
        auto collectionDir = rootdir.relativeDirectory(inputFileDir).takeOrThrow();
        if (!resources.root.empty()) {
          collectionDir = collectionDir.relativeDirectory(resources.root).takeOrThrow();
        }
        for (const auto& entity : resources.entities) {
          auto entityFile = FileSystem::makeFile(entity.path).takeOrThrow();
          auto entityDirectory = entityFile.directory();
          entityFile = entityFile.rooted(collectionDir).takeOrThrow();
          const auto enitityId = resourcePrefix() + resources.prefix + entity.name;
          const auto entityString = entityIdentifier(enitityId);

          if (entity.type == Type::Text) {
            const auto text = entityFile.readAllText().takeOrThrow();
            sourceBuilder << wrapText(text, entityString) << std::endl;
          }
          initBuilder << "  container.set(\"" << enitityId << "\", " << entityString << ").throwOnError();\n";
        }

        initBuilder << R"__(
  __isInitialized.store(true);
}
)__";
        sourceBuilder << "\n" << initBuilder.str();

        headerBuilder << "\n";
        sourceBuilder << "\n";

        for (auto i = ns.rbegin(); i != ns.rend(); ++i) {
          headerBuilder << "} // " << *i << "\n";
          sourceBuilder << "} // " << *i << "\n";
        }
      }

      auto headerPath = headersDir.relativeFile(resources.identifier + ".hpp").takeOrThrow();
      if (!headerPath.exists().valueOrThrow()) {
        tempOutHeader.copyTo(headerPath).throwOnError();
      } else {
        FastHash current, updated;
        {
          auto stream = tempOutHeader.openBinaryForRead().takeOrThrow();
          FastHash::compute(*stream, updated).throwOnError();
        }
        {
          auto stream = headerPath.openBinaryForRead().takeOrThrow();
          FastHash::compute(*stream, current).throwOnError();
        }
        if (current != updated) {
          XI_INFO("Updating '{}'.", headerPath)
          tempOutHeader.copyTo(headerPath, FileSystem::overwrite).throwOnError();
        } else {
          XI_INFO("'{}' already up to date.", headerPath)
        }
      }

      auto sourcePath = sourceDir.relativeFile(resources.identifier + ".cpp").takeOrThrow();
      if (!sourcePath.exists().valueOrThrow()) {
        tempOutSource.copyTo(sourcePath).throwOnError();
      } else {
        FastHash current, updated;
        {
          auto stream = tempOutSource.openBinaryForRead().takeOrThrow();
          FastHash::compute(*stream, updated).throwOnError();
        }
        {
          auto stream = sourcePath.openBinaryForRead().takeOrThrow();
          FastHash::compute(*stream, current).throwOnError();
        }
        if (current != updated) {
          XI_INFO("Updating '{}'.", sourcePath)
          tempOutSource.copyTo(sourcePath, FileSystem::overwrite).throwOnError();
        } else {
          XI_INFO("'{}' already up to date.", sourcePath)
        }
      }
    }

    return EXIT_SUCCESS;
  } catch (std::exception& e) {
    XI_FATAL("Code generation threw exception '{}'", e.what());
    return EXIT_FAILURE;
  }
}
