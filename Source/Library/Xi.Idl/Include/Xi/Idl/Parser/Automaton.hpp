/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <memory>
#include <deque>
#include <string>
#include <vector>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>
#include <Xi/Exceptions.hpp>

#include "Xi/Idl/Parser/Token.hpp"
#include "Xi/Idl/Parser/Cursor.hpp"
#include "Xi/Idl/Parser/ParserError.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

XI_DECLARE_SMART_POINTER_CLASS(Automaton)

/*!
 * \brief The Automaton class encapsulates token processing chains of repsonsibility.
 */
class Automaton : public std::enable_shared_from_this<Automaton> {
 public:
  virtual ~Automaton();

  Result<void> process(const std::string& content);
  Result<void> process(Cursor& cursor);
  Result<void> processToken(const Token& token);
  bool isFinished();

 protected:
  void chain(SharedAutomaton child);
  void chainFront(SharedAutomaton child);
  void chainBack(SharedAutomaton child);

  void skipSpaces();
  void skipNewLines();
  void skipVoid();
  Result<void> propagateBack(const Token& token);
  void markFinished();

  void listenToSpaces();
  void listenToNewLine();
  void listenToVoid();

  /*
   * This is kinda hacky, we can use chain documentation to allow documentation being parsed,
   * however the callee may not be sure where this documentation belongs to as the automaton may
   * propagate back and decides typedeclarations are finished and services are now being parsed.
   *
   * Thus you may use chainDocumentation() to chain back a new automaton to parse documentation into
   * a cache in this object. Once any automaton knows where the documentation shall take place it calls
   * emplaceDocumentation to grab and reset the internal documentation cache state.
   */
  void chainDocumentation();
  void emplaceDocumentation(std::vector<struct DocumentationEntry>& doc);

 protected:
  Automaton();
  virtual Result<void> doProcessToken(const Token& token);

 private:
  SharedAutomaton currentChild() const;

 private:
  WeakAutomaton m_parent;
  std::deque<SharedAutomaton> m_chain;
  bool m_finished = false;
  bool m_processSpaces = false;
  bool m_processNewLine = false;
  std::vector<struct DocumentationEntry> m_docCache;
};

template <typename _ResultT>
class ResultAutomaton : public Automaton {
 public:
  using result_type = _ResultT;

 protected:
  result_type& m_result;

 public:
  explicit ResultAutomaton(result_type& out) : m_result{out} {
    /* */
  }
};

class ReadLineAutomaton : public ResultAutomaton<std::string> {
 protected:
  Result<void> doProcessToken(const Token& token);

 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<ReadLineAutomaton> makeReadLineAutomaton(std::string&);
};

std::shared_ptr<ReadLineAutomaton> makeReadLineAutomaton(std::string& result);

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi
