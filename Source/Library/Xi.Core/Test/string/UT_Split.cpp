/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/String/Split.hpp>

#define XI_TEST_SUITE Xi_Algorithm_String_Split

TEST(XI_TEST_SUITE, Empty) {
  using namespace ::testing;
  using namespace ::Xi;

  {
    auto result = split("", "");
    EXPECT_THAT(result, IsEmpty());
  }

  {
    auto result = split("", "\0");
    EXPECT_THAT(result, IsEmpty());
  }

  {
    auto result = split("\0\n\0", "\0\n");
    EXPECT_THAT(result, IsEmpty());
  }
}

TEST(XI_TEST_SUITE, Single) {
  using namespace ::testing;
  using namespace ::Xi;

  {
    auto result = split("a", "");
    ASSERT_THAT(result, SizeIs(1));
    EXPECT_THAT(result[0], Eq("a"));
  }

  {
    auto result = split("  a\0 ", "bcde");
    ASSERT_THAT(result, SizeIs(1));
    EXPECT_THAT(result[0], Eq("  a\0 "));
  }
}

TEST(XI_TEST_SUITE, Multi) {
  using namespace ::testing;
  using namespace ::Xi;

  {
    auto result = split("abba", "b");
    ASSERT_THAT(result, SizeIs(2));
    EXPECT_THAT(result[0], Eq("a"));
    EXPECT_THAT(result[1], Eq("a"));
  }
}
