/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/FileSystem/FileType.hpp"

#include <Xi/Exceptions.hpp>

namespace Xi {
namespace FileSystem {

std::string stringify(const FileType ftype) {
  switch (ftype) {
    case FileType::None:
      return "none";
    case FileType::Regular:
      return "regular";
    case FileType::SymbolicLink:
      return "symbolicLink";
    case FileType::Block:
      return "block";
    case FileType::Character:
      return "character";
    case FileType::Pipe:
      return "pipe";
    case FileType::Socket:
      return "socket";
    case FileType::Unknown:
      return "unknown";
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

}  // namespace FileSystem
}  // namespace Xi
