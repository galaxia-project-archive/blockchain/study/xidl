﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/App/LoggingOptions.h"

#include <Xi/Global.hh>

void Xi::App::LoggingOptions::loadEnvironment(Xi::App::Environment &env) {
  std::string defaultLogLevel{};
  std::string consoleLogLevel{};
  std::string discordLogLevel{};
  std::string fileLogLevel{};
  // clang-format off
  env
    (defaultLogLevel, "LOG_LEVEL")
    (consoleLogLevel, "LOG_CONSOLE_LEVEL")
    (discordLogLevel, "LOG_DISCORD_LEVEL")
    (fileLogLevel, "LOG_FILE_LEVEL")
    (LogFilePath, "LOG_FILE_PATH")
    (DiscordWebhook, "LOG_DISCORD_WEBHOOK")
    (DiscordAuthor, "LOG_DISCORD_AUTHOR")
  ;
  // clang-format on
  if (!defaultLogLevel.empty()) {
    DefaultLogLevel = fromString<Logging::Level>(defaultLogLevel).valueOrThrow();
  }
  if (!consoleLogLevel.empty()) {
    ConsoleLogLevel = fromString<Logging::Level>(consoleLogLevel).valueOrThrow();
  }
  if (!discordLogLevel.empty()) {
    DiscordLogLevel = fromString<Logging::Level>(discordLogLevel).valueOrThrow();
  }
  if (!fileLogLevel.empty()) {
    FileLogLevel = fromString<Logging::Level>(fileLogLevel).valueOrThrow();
  }
}

void Xi::App::LoggingOptions::emplaceOptions(cxxopts::Options &options) {
  const std::string acceptedLevels{"none|fatal|error|warning|info|debbuging|trace"};
  // clang-format off
  options.add_options("logging")
    ("log-level", "minimum log level for all loggers applied by default",
        cxxopts::value<std::string>()->default_value(toString(DefaultLogLevel)), acceptedLevels)
    ("log-console-level", "minimum log level for console logging", cxxopts::value<std::string>(), acceptedLevels)
    ("log-file-level", "minimum log level for file logging", cxxopts::value<std::string>(), acceptedLevels)
    ("log-file", "file path writing logs to", cxxopts::value<std::string>(LogFilePath)->default_value(LogFilePath), "<filepath>")
    ("log-discord-level", "minimum log level for discord logging", cxxopts::value<std::string>(), acceptedLevels)
    ("log-discord-webhook", "discord webhook url", cxxopts::value<std::string>(DiscordWebhook)->default_value(DiscordWebhook), "<url>")
    ("log-discord-author", "author name for the discord posts", cxxopts::value<std::string>(DiscordAuthor)->default_value(DiscordAuthor), "<name>")
  ;
  // clang-format on
}

bool Xi::App::LoggingOptions::evaluateParsedOptions(const cxxopts::Options &options,
                                                    const cxxopts::ParseResult &result) {
  XI_UNUSED(options);
  if (result.count("log-level")) {
    DefaultLogLevel = Xi::fromString<Logging::Level>(result["log-level"].as<std::string>()).valueOrThrow();
  }
  if (result.count("log-console-level")) {
    ConsoleLogLevel = Xi::fromString<Logging::Level>(result["log-console-level"].as<std::string>()).valueOrThrow();
  }
  if (result.count("log-file-level")) {
    FileLogLevel = Xi::fromString<Logging::Level>(result["log-file-level"].as<std::string>()).valueOrThrow();
  }
  if (result.count("log-discord-level")) {
    DefaultLogLevel = Xi::fromString<Logging::Level>(result["log-discord-level"].as<std::string>()).valueOrThrow();
  }
  return false;
}
