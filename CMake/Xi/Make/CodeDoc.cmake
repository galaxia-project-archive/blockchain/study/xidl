# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_MAKE_CODEDOC)
    return()
endif()
set(CMAKE_XI_MAKE_CODEDOC TRUE)

option(XI_BUILD_CODEDOC "Enables doxygen code documentation targets" OFF)

xi_include(Xi/Log)

# xi_make_code_doc(
#   <lib_dir>               << The target library directory (MUST be relative to the current source directory)
#   <lib_name>              << The target library name
# )
# If code documentation is enabled (XI_BUILD_CODEDOC=ON) creates an addition target (<lib_name>.CodeDoc)
# which builds code documentation using doxygen.

if(XI_BUILD_CODEDOC)
    xi_debug(
        "Code documentation using doxygen enabled. Please ensure all dependencies are installed:"
            "Doxygen: http://www.doxygen.nl/"
            "Graphviz: https://www.graphviz.org/"
            "DIA: http://dia-installer.de/"
            "MSC-Generator: https://sites.google.com/site/mscgen2393/"
    )

    find_package(
        Doxygen 1.8.7

        REQUIRED

        COMPONENTS
            dot
            mscgen
            dia
    )

    macro(xi_make_code_doc lib_dir lib_name)
        set(include_dir ${lib_dir}/Include)
        set(source_dir ${lib_dir}/Source)
        set(doc_dir ${lib_dir}/Doc)

        set(existing_doc_dirs)
        foreach(may_doc_dir ${include_dir} ${source_dir} ${doc_dir})
            if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/${may_doc_dir})
                list(APPEND existing_doc_dirs ${CMAKE_CURRENT_SOURCE_DIR}/${may_doc_dir})
                xi_debug("CodeDoc directory included: ${CMAKE_CURRENT_SOURCE_DIR}/${may_doc_dir}")
            else()
                xi_debug("CodeDoc directory excluded: ${CMAKE_CURRENT_SOURCE_DIR}/${may_doc_dir}")
            endif()
        endforeach()

        set(
            asset_search_dirs
                DOXYGEN_MSCFILE_DIRS
                DOXYGEN_DOTFILE_DIRS
                DOXYGEN_DIAFILE_DIRS
        )
        if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/${doc_dir})
            foreach(asset_search_dir ${asset_search_dirs})
                set(${asset_search_dir} "${CMAKE_CURRENT_SOURCE_DIR}/${doc_dir}")
            endforeach()
        else()
            foreach(asset_search_dir ${asset_search_dirs})
                set(${asset_search_dir} "")
            endforeach()
        endif()

        set(DOXYGEN_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/${lib_name}.CodeDoc")
        set(DOXYGEN_UML_LOOK YES)
        doxygen_add_docs(
            "${lib_name}.CodeDoc"

            ${existing_doc_dirs}

            WORKING_DIRECTORY
                ${CMAKE_CURRENT_SOURCE_DIR}
        )
    endmacro()
else()
    xi_status("Code documentation using doxygen disabled (XI_BUILD_CODEDOC=OFF) , skipping...")

    macro(xi_make_code_doc)
        # Dummy
    endmacro()
endif()
