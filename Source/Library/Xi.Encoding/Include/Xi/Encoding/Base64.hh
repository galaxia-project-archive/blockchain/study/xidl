﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>
#include <Xi/Byte.hh>

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdlib.h>

#define XI_BASE64_DECODE_INVALID_CHAR -1
#define XI_BASE64_DECODE_INVALID_SIZE -2
#define XI_BASE64_DECODE_NULL_ARGUMENT -3
#define XI_BASE64_ENCODE_NULL_ARGUMENT -1

/*!
 * \brief xi_encoding_bas64_encode_length Returns the resulting string length of a base64 byte encoding.
 * \param sourceLength The number of bytes to encode.
 * \return Number of chars printed if the byte stream is encoded.
 */
size_t xi_encoding_base64_encode_length(size_t sourceLength);

int xi_encoding_base64_encode(char* encoded, const xi_byte_t* data, size_t dataLength);

int xi_encoding_base64_decode_length(const char* encoded, size_t encodedLength, size_t* out);

int xi_encoding_base64_decode(xi_byte_t* decoded, const char* data, size_t dataLength);

#if defined(__cplusplus)
}
#endif

#if defined(__cplusplus)
#include <string>
#include <string_view>

#include <Xi/Result.hpp>
#include <Xi/ErrorCode.hpp>

namespace Xi {
namespace Encoding {
namespace Base64 {
/*!
 * \brief encode encodes a raw data block into a Bas64 encoded string
 * \param raw the binary data to encode
 * \return the Base64 encoding of the binary data
 */
std::string encode(ConstByteSpan raw);

size_t encodeSize(const size_t sourceSize);

/*!
 * \brief decodes a Base64 encoded data string into its binary format
 * \param raw the Base64 encoded data block
 * \return the raw data blob thats was encoded by the given Base64 string
 */
Result<ByteVector> decode(const std::string_view raw);

Result<size_t> decodeSize(const std::string_view raw);

Result<size_t> decode(const std::string_view raw, ByteSpan out);
Result<void> decodeStrict(const std::string_view raw, ByteSpan out);

XI_ERROR_CODE_BEGIN(Decode)
XI_ERROR_CODE_VALUE(NullArgument, 0x0001)
XI_ERROR_CODE_VALUE(InvalidSize, 0x0002)
XI_ERROR_CODE_VALUE(InvalidCharacter, 0x0003)
XI_ERROR_CODE_VALUE(OutOfMemory, 0x0004)
XI_ERROR_CODE_VALUE(SizeMissmatch, 0x0005)
XI_ERROR_CODE_END(Decode, "base64 decoding failure")

}  // namespace Base64
}  // namespace Encoding
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Encoding::Base64, Decode)

#endif
