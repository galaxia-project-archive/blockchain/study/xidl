/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/FileSystem/Permission.hpp"

namespace fs = std::filesystem;

namespace Xi {
namespace FileSystem {

std::string stringify(const Permission perm) {
  std::string reval{9u, '-'};
  if (hasFlag(perm, Permission::OwnerRead)) {
    reval[0] = 'r';
  }
  if (hasFlag(perm, Permission::OwnerWrite)) {
    reval[1] = 'w';
  }
  if (hasFlag(perm, Permission::OwnerExecute)) {
    reval[2] = 'x';
  }
  if (hasFlag(perm, Permission::GroupRead)) {
    reval[3] = 'r';
  }
  if (hasFlag(perm, Permission::GroupWrite)) {
    reval[4] = 'w';
  }
  if (hasFlag(perm, Permission::GroupExecute)) {
    reval[5] = 'x';
  }
  if (hasFlag(perm, Permission::WorldRead)) {
    reval[6] = 'r';
  }
  if (hasFlag(perm, Permission::WorldWrite)) {
    reval[7] = 'w';
  }
  if (hasFlag(perm, Permission::WorldExecute)) {
    reval[8] = 'x';
  }
  return reval;
}

Permission fromStd(std::filesystem::perms stdperms) {
  Permission reval = Permission::None;
  if ((stdperms & fs::perms::owner_read) != fs::perms::none) {
    reval |= Permission::OwnerRead;
  }
  if ((stdperms & fs::perms::owner_write) != fs::perms::none) {
    reval |= Permission::OwnerWrite;
  }
  if ((stdperms & fs::perms::owner_exec) != fs::perms::none) {
    reval |= Permission::OwnerExecute;
  }
  if ((stdperms & fs::perms::group_read) != fs::perms::none) {
    reval |= Permission::GroupRead;
  }
  if ((stdperms & fs::perms::group_write) != fs::perms::none) {
    reval |= Permission::GroupWrite;
  }
  if ((stdperms & fs::perms::group_exec) != fs::perms::none) {
    reval |= Permission::GroupExecute;
  }
  if ((stdperms & fs::perms::others_read) != fs::perms::none) {
    reval |= Permission::WorldRead;
  }
  if ((stdperms & fs::perms::others_write) != fs::perms::none) {
    reval |= Permission::WorldWrite;
  }
  if ((stdperms & fs::perms::others_exec) != fs::perms::none) {
    reval |= Permission::WorldExecute;
  }
  return reval;
}

std::filesystem::perms toStd(Permission perms) {
  fs::perms reval = fs::perms::none;
  if (hasFlag(perms, Permission::OwnerRead)) {
    reval |= fs::perms::owner_read;
  }
  if (hasFlag(perms, Permission::OwnerWrite)) {
    reval |= fs::perms::owner_write;
  }
  if (hasFlag(perms, Permission::OwnerExecute)) {
    reval |= fs::perms::owner_exec;
  }
  if (hasFlag(perms, Permission::OwnerRead)) {
    reval |= fs::perms::group_read;
  }
  if (hasFlag(perms, Permission::GroupWrite)) {
    reval |= fs::perms::group_write;
  }
  if (hasFlag(perms, Permission::GroupExecute)) {
    reval |= fs::perms::group_exec;
  }
  if (hasFlag(perms, Permission::WorldRead)) {
    reval |= fs::perms::others_read;
  }
  if (hasFlag(perms, Permission::WorldWrite)) {
    reval |= fs::perms::others_write;
  }
  if (hasFlag(perms, Permission::WorldExecute)) {
    reval |= fs::perms::others_exec;
  }
  return reval;
}

}  // namespace FileSystem
}  // namespace Xi
