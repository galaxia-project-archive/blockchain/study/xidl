/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rpc/Response.hpp"

#include <utility>

namespace Xi {
namespace Rpc {

Response::Response() : m_data{std::in_place_index_t<1>(), makeContractPayload()} {
  /* */
}

Version Response::version() const {
  return m_version;
}

void Response::setVersion(const Version version_) {
  m_version = version_;
}

Identifier Response::identifier() const {
  return m_identifier;
}

const ResponseData &Response::data() const {
  return m_data;
}

void Response::setData(Error error) {
  m_data.emplace<0>(std::move(error));
}

void Response::setData(ContractPayload object) {
  m_data.emplace<1>(std::move(object));
}

}  // namespace Rpc
}  // namespace Xi

XI_SERIALIZATION_COMPLEX_EXTERN_BEGIN(Xi::Rpc::Response)
XI_SERIALIZATION_MEMBER(m_version, 0x0001, "version")
XI_SERIALIZATION_MEMBER(m_identifier, 0x0002, "identifier")
XI_SERIALIZATION_MEMBER(m_data, 0x0003, "data")
XI_SERIALIZATION_COMPLEX_EXTERN_END
