# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_LIBRARY_FILESYSTEM)
  return()
endif()
set(CMAKE_XI_LIBRARY_FILESYSTEM TRUE)

xi_include(Xi/Make/Library)
xi_include(Source/Library/Xi/Core)
xi_include(Source/Library/Xi/TypeSafe)
xi_include(Source/Library/Xi/Stream)
xi_include(Source/Library/Xi/Memory)

xi_make_library(
  Xi.FileSystem

  PUBLIC_LIBRARIES
    Xi::Core
    Xi::Stream
    Xi::TypeSafe
    Xi::Memory
)

if(XI_COMPILER_GCC)
  if(CMAKE_CXX_COMPILER_VERSION VERSION_LESS 9)
    target_link_libraries(
      Library.Xi.FileSystem

      PUBLIC
        stdc++fs
    )
  endif()
endif()
