/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/FileSystem/EntryGenerator.hpp"

namespace Xi {
namespace FileSystem {

EntryGenerator::iterator EntryGenerator::begin() const {
  return m_begin;
}

EntryGenerator::iterator EntryGenerator::end() const {
  return m_end;
}

EntryGenerator::iterator EntryGenerator::cbegin() const {
  return m_begin;
}

EntryGenerator::iterator EntryGenerator::cend() const {
  return m_end;
}

EntryGenerator::EntryGenerator(std::filesystem::directory_iterator begin_, std::filesystem::directory_iterator end_)
    : m_begin{begin_}, m_end{end_} {
  /* */
}

}  // namespace FileSystem
}  // namespace Xi
