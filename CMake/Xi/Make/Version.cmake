# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_MAKE_VERSION)
    return()
endif()
set(CMAKE_XI_MAKE_VERSION TRUE)

xi_include(Xi/Escape)
xi_include(Xi/Compiler)
xi_include(Xi/Compiler/Classify)

option(XI_SKIP_VERSION_UPDATE "Skips version update for unnecessary recompilation on dev environments" OFF)

set(XI_MAKE_VERSION_TEMPLATE "${CMAKE_CURRENT_LIST_DIR}/Version.hh.in" CACHE INTERNAL "")
set(XI_MAKE_VERSION_SCRIPT "${CMAKE_CURRENT_LIST_DIR}/VersionScript.cmake" CACHE INTERNAL "")

# xi_make_version(
#   <target_source>         << Target source directory (MUST be relative to current source directory)
#   <target_name>           << Target name (MUST be canoncial)
#   <out_var>               << Variable name to store version number
# )
# Creates an interface version library by parsing the library version file (<target_source>/VERSION).
# The version library is linked to the target and contains c macros and c++ constexpr functions to retrieve
# version information.
#
# C-Macros:
#   - <PREFIX>_VERSION_MAJOR <major>
#   - <PREFIX>_VERSION_MINOR <major>
#   - <PREFIX>_VERSION_PATCH <major>
#   - <PREFIX>_VERSION_BUILD <major>
#
# C++-Methods:
#   - bool <NAMESPACE>::hasMajor(void)
#   - std::uint16_t <NAMESPACE>::major(void)
#   - bool <NAMESPACE>::hasMinor(void)
#   - std::uint16_t <NAMESPACE>::minor(void)
#   - bool <NAMESPACE>::hasPatch(void)
#   - std::uint16_t <NAMESPACE>::patch(void)
#   - bool <NAMESPACE>::hasBuild(void)
#   - std::uint16_t <NAMESPACE>::build(void)
#
# Macros are only defined if the corresponding version number is availabe, this holds also true for c++
# methods returning those version numbers.
# If you like to query iff a version number is available you may consider one of two ways.
#
# C:
#   #if defined(<PREFIX>_VERSION_<TYPE>)
#       // Version is available
#   #endif
#
# C++:
#   if constexpr(<NAMESPACE>::has<TYPE>()) {
#       // Version is available
#   }
macro(xi_make_version _version_file target_name out_var)
    set(target_version_name "${target_name}")
    set(target_version_file "${_version_file}")
    set(traget_version_bin_dir "${CMAKE_CURRENT_BINARY_DIR}/${target_version_name}")
    string(REPLACE "." "/" target_path ${target_name})
    string(REPLACE "." "::" target_namespace ${target_version_name})

    set(target_version_include_dir "${traget_version_bin_dir}/Include")
    set(target_version_header_dir "${target_version_include_dir}/${target_path}")
    set(target_version_header_file "${target_version_header_dir}/Version.hh")
    set(target_version_source_dir "${traget_version_bin_dir}/Source")

    file(STRINGS ${target_version_file} input_content)
    list(LENGTH input_content intput_content_length)
    if(input_content_length LESS 1)
        message(FATAL_ERROR "Empty version content for '${INPUT_FILE}'.")
    endif()
    list(GET input_content 0 version_line)

    if(NOT EXISTS ${target_version_file})
        xi_fatal("Target '${target_name}' version file '${target_version_file}' missing.")
    endif()

    file(MAKE_DIRECTORY ${target_version_include_dir})
    file(MAKE_DIRECTORY ${target_version_header_dir})
    file(MAKE_DIRECTORY ${target_version_source_dir})

    string(TOUPPER "${CMAKE_BUILD_TYPE}" upper_build_type)
    set(c_compiler_flags "${XI_C_FLAGS} ${CMAKE_C_FLAGS} ${CMAKE_C_FLAGS_${upper_build_type}}")
    set(cxx_compiler_flags "${XI_CXX_FLAGS} ${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_${upper_build_type}}")

    set(
      SCRIPT_PARAMETER
        "-DXI_SKIP_VERSION_UPDATE=${XI_SKIP_VERSION_UPDATE}"

        "-DPROJECT_DIR=${PROJECT_SOURCE_DIR}"
        "-DINPUT_FILE=${target_version_file}"
        "-DHEADER_DIR=${target_version_header_dir}"
        "-DTEMPLATE_FILE=${XI_MAKE_VERSION_TEMPLATE}"
        "-DNAMESPACE=${target_namespace}"

        "-DCOMPILER_PLATFORM=${XI_COMPILER_PLATFORM_${XI_COMPILER_PLATFORM_ID}_NAME}"
        "-DCOMPILER_ENDIANESS=${XI_COMPILER_ENDIANESS}"
        "-DCOMPILER_ID=${XI_COMPILER_ID}"
        "-DCOMPILER_C_FLAGS=${c_compiler_flags}"
        "-DCOMPILER_C_ID=${CMAKE_C_COMPILER_ID}"
        "-DCOMPILER_C_VERSION=${CMAKE_C_COMPILER_VERSION}"
        "-DCOMPILER_CXX_FLAGS=${cxx_compiler_flags}"
        "-DCOMPILER_CXX_ID=${CMAKE_CXX_COMPILER_ID}"
        "-DCOMPILER_CXX_VERSION=${CMAKE_CXX_COMPILER_VERSION}"
    )

    set(SCRIPT_PARAMETER_ESCAPED)
    foreach(parameter ${SCRIPT_PARAMETER})
      xi_escape(parameter "${parameter}")
      list(APPEND SCRIPT_PARAMETER_ESCAPED ${parameter})
    endforeach()

    add_custom_target(
        ${target_version_name}.Generate

        COMMAND
            ${CMAKE_COMMAND}
                ${SCRIPT_PARAMETER_ESCAPED}
                -P ${XI_MAKE_VERSION_SCRIPT}

        SOURCES
            ${target_version_file}
            ${XI_MAKE_VERSION_SCRIPT}
            ${XI_MAKE_VERSION_TEMPLATE}

        BYPRODUCTS
            ${target_version_header_file}

        WORKING_DIRECTORY
            ${target_version_bin_dir}
    )

    add_library(${target_version_name} INTERFACE)

    add_dependencies(
      ${target_version_name}
        ${target_version_name}.Generate
    )

    target_include_directories(
        ${target_version_name}

        INTERFACE
            ${target_version_include_dir}
    )

    target_sources(
        ${target_version_name}

        INTERFACE
            ${target_version_header_file}
    )

    add_library(${target_namespace} ALIAS ${target_version_name})
endmacro()
