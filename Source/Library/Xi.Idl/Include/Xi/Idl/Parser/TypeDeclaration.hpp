/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <variant>
#include <vector>

#include "Xi/Idl/Parser/Package.hpp"
#include "Xi/Idl/Parser/Variant.hpp"
#include "Xi/Idl/Parser/Enum.hpp"
#include "Xi/Idl/Parser/Flag.hpp"
#include "Xi/Idl/Parser/Alias.hpp"
#include "Xi/Idl/Parser/Typedef.hpp"
#include "Xi/Idl/Parser/Documentation.hpp"
#include "Xi/Idl/Parser/Automaton.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

// clang-format off
using TypeDeclarationValue = std::variant<
  Package,
  Variant,
  Enum,
  Flag,
  Alias,
  Typedef
>;
// clang-format on

struct TypeDeclaration {
  Documentation doc;
  TypeDeclarationValue value;
};

using TypeDeclarationVector = std::vector<TypeDeclaration>;

class TypeDeclarationValueAutomaton final : public ResultAutomaton<TypeDeclarationValue> {
 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<TypeDeclarationValueAutomaton> makeTypeDeclarationValueAutomaton(TypeDeclarationValue&);

 protected:
  Result<void> doProcessToken(const Token& token) override;
};

std::shared_ptr<TypeDeclarationValueAutomaton> makeTypeDeclarationValueAutomaton(TypeDeclarationValue& result);

class TypeDeclarationsAutomaton final : public ResultAutomaton<TypeDeclarationVector> {
 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<TypeDeclarationsAutomaton> makeTypeDeclarationsAutomaton(TypeDeclarationVector&);

 protected:
  Result<void> doProcessToken(const Token& token) override;
};

std::shared_ptr<TypeDeclarationsAutomaton> makeTypeDeclarationsAutomaton(TypeDeclarationVector& result);

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi
