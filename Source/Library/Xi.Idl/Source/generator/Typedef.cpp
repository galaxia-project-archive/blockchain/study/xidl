/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/Typedef.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

const std::string &Typedef::name() const {
  return m_name;
}

SharedConstNamespace Typedef::namespace_() const {
  return m_namespace;
}

const Documentation &Typedef::documentation() const {
  return m_doc;
}

const Type &Typedef::type() const {
  return m_type;
}

void Typedef::setName(const std::string &name_) {
  m_name = name_;
}

void Typedef::setNamespace(SharedConstNamespace ns) {
  m_namespace = ns;
}

void Typedef::setDocumentation(const Documentation &doc_) {
  m_doc = doc_;
}

void Typedef::setType(const Type &type_) {
  m_type = type_;
}

TypedefBuilder::TypedefBuilder() : m_Typedef{result_success, Typedef{}} {
  /* */
}

TypedefBuilder &TypedefBuilder::withName(const std::string &name) {
  if (!m_Typedef.isError()) {
    m_Typedef->setName(name);
  }
  return *this;
}

TypedefBuilder &TypedefBuilder::withNamespace(SharedConstNamespace ns) {
  if (!m_Typedef.isError()) {
    m_Typedef->setNamespace(ns);
  }
  return *this;
}

TypedefBuilder &TypedefBuilder::withDocumentation(const Documentation &doc) {
  if (!m_Typedef.isError()) {
    m_Typedef->setDocumentation(doc);
  }
  return *this;
}

TypedefBuilder &TypedefBuilder::withType(const Type &type) {
  if (!m_Typedef.isError()) {
    m_Typedef->setType(type);
  }
  return *this;
}

Result<Typedef> TypedefBuilder::build() {
  return m_Typedef;
}

Result<Type> makeType(const Typedef &typedef_) {
  return TypeBuilder{}
      .withKind(Type::Kind::Typedef)
      .withName(typedef_.name())
      .withNamespace(typedef_.namespace_())
      .withIsOptional(false)
      .withData(typedef_)
      .build();
}

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi
