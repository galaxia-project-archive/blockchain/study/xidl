/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <string>

namespace Xi {
namespace Stringify {

struct hexadecimal_t {
  /* */
};

static inline constexpr hexadecimal_t hexadecimal{/* */};

}  // namespace Stringify

std::string stringify(const uint8_t value, Stringify::hexadecimal_t);
std::string stringify(const uint16_t value, Stringify::hexadecimal_t);
std::string stringify(const uint32_t value, Stringify::hexadecimal_t);
std::string stringify(const uint64_t value, Stringify::hexadecimal_t);
std::string stringify(const int8_t value, Stringify::hexadecimal_t);
std::string stringify(const int16_t value, Stringify::hexadecimal_t);
std::string stringify(const int32_t value, Stringify::hexadecimal_t);
std::string stringify(const int64_t value, Stringify::hexadecimal_t);

}  // namespace Xi
