/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/VersionInfo/VersionInfo.hpp"

namespace Xi {
namespace VersionInfo {

const VersionInfo &VersionInfo::project() {
  static const VersionInfo __singleton = VersionInfoBuilder{/* */}
                                             .withVersion(Version::project())
                                             .withCompiler(Compiler::project())
                                             .withGit(Git::project())
                                             .build();
  return __singleton;
}

const Version &VersionInfo::version() const {
  return m_version;
}

const Compiler &VersionInfo::compiler() const {
  return m_compiler;
}

const std::optional<Git> &VersionInfo::git() const {
  return m_git;
}

XI_SERIALIZATION_COMPLEX_EXTERN_BEGIN(VersionInfo)
XI_SERIALIZATION_MEMBER(m_version, 0x0001, "version")
XI_SERIALIZATION_MEMBER(m_compiler, 0x0001, "compiler")
XI_SERIALIZATION_MEMBER(m_git, 0x0001, "git")
XI_SERIALIZATION_COMPLEX_EXTERN_END

VersionInfoBuilder &VersionInfoBuilder::withVersion(const Version &version) {
  m_info.m_version = version;
  return *this;
}

VersionInfoBuilder &VersionInfoBuilder::withCompiler(const Compiler &compiler) {
  m_info.m_compiler = compiler;
  return *this;
}

VersionInfoBuilder &VersionInfoBuilder::withGit(const std::optional<Git> git) {
  m_info.m_git = git;
  return *this;
}

VersionInfo VersionInfoBuilder::build() const {
  return m_info;
}

}  // namespace VersionInfo
}  // namespace Xi
