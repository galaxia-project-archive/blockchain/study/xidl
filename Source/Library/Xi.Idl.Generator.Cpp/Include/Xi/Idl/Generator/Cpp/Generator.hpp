/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <ostream>
#include <memory>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>
#include <Xi/Idl/Generator/Generator.hpp>

#include "Xi/Idl/Generator/Cpp/Config.hpp"

namespace Xi {
namespace Idl {
namespace Generator {
namespace Cpp {

XI_DECLARE_SMART_POINTER_CLASS(Generator)

class Generator final : public Idl::Generator::Generator {
 public:
  XI_DELETE_COPY(Generator);
  XI_DELETE_MOVE(Generator);
  ~Generator() override;

 private:
  explicit Generator();

  const class FileMapper& fileMapper() const;

  friend Result<UniqueGenerator> makeGenerator(const Config&);

 protected:
  Result<void> doOpenNamespace(SharedConstNamespace ns) override;
  Result<void> doOnPackage(const Package& package) override;
  Result<void> doOnVariant(const Variant& variant) override;
  Result<void> doOnAlias(const Alias& alias) override;
  Result<void> doOnTypedef(const Typedef& typedef_) override;
  Result<void> doOnEnum(const Enum& enum_) override;
  Result<void> doOnFlag(const Flag& flag) override;
  Result<void> doOnContract(const Contract& contract) override;
  Result<void> doOnService(const Service& service) override;

 private:
  struct _Impl;
  std::unique_ptr<_Impl> m_impl;
};

Result<UniqueGenerator> makeGenerator(const Config& config);

}  // namespace Cpp
}  // namespace Generator
}  // namespace Idl
}  // namespace Xi
