/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string>
#include <optional>
#include <memory>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>
#include <Xi/FileSystem/FileSystem.hpp>
#include <Xi/Idl/Generator/Namespace.hpp>
#include <Xi/Idl/Generator/Type.hpp>

#include "Xi/Idl/Generator/Cpp/IncludeCollection.hpp"

namespace Xi {
namespace Idl {
namespace Generator {
namespace Cpp {

/*!
 * \brief The FileMapper class maps include and source paths for generated/included files.
 *
 * A type declaration may encode array/vector/optional of a type. Those will not induce
 * a seperate type declaration, thus it is required to include the corresponding standard
 * headers. For any other kind of type the defintion is considered to be in a seperate header
 * file. Ie. SearchQuery[+]? with SearchQuery being a variant of various types will include
 * <optional>, <vector> and "../SearchQuery.hpp". In this case SearchQuery.hpp is considered
 * to include all its dependencies based on the variant definition.
 */
class FileMapper {
 public:
  XI_DEFAULT_MOVE(FileMapper);
  XI_DEFAULT_COPY(FileMapper);

  Result<FileSystem::Directory> headerDirectory(SharedConstNamespace ns) const;
  Result<FileSystem::File> headerFile(const std::string& name, SharedConstNamespace ns) const;
  Result<FileSystem::File> headerFile(const Type& type) const;
  Result<FileSystem::Directory> sourceDirectory(const Type& type) const;
  Result<FileSystem::File> sourceFile(const Type& type) const;

  /*!
   * \brief Returns all includes required to reference this type.
   *
   * \attention This may not include all required headers to declare the type.
   */
  Result<IncludeCollection> includes(const Type& type) const;

  /*!
   * \brief Adds all includes required to reference this type.
   *
   * \attention This may not include all required headers to declare the type.
   */
  Result<void> includes(const Type& type, IncludeCollection& out) const;

  std::optional<Include> include(const Primitive& primitve) const;
  std::string includeDirectoryPath(SharedConstNamespace ns) const;
  std::string includeFilePath(const std::string& name, SharedConstNamespace ns) const;
  std::string includeFilePath(const Type& type) const;

 private:
  explicit FileMapper() = default;
  friend Result<FileMapper> makeFileMapper(const std::string&);

  std::vector<std::string> namespacePath(SharedConstNamespace ns) const;
  std::string namespaceHeaderPath(SharedConstNamespace ns) const;
  std::string namespaceSourcePath(SharedConstNamespace ns) const;
  std::string namespaceIncludePath(SharedConstNamespace ns) const;

 private:
  FileSystem::Directory m_root;
  FileSystem::Directory m_include;
  FileSystem::Directory m_source;
};

Result<FileMapper> makeFileMapper(const std::string& outputpath);

}  // namespace Cpp
}  // namespace Generator
}  // namespace Idl
}  // namespace Xi
