﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/Testing/Result.hpp>
#include <Xi/Http/ContentType.h>

#include <string>

#define XI_TESTSUITE T_Xi_Http_ContentType

TEST(XI_TESTSUITE, ToString) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_ANY_THROW(toString(static_cast<Http::ContentType>(-1)));
  EXPECT_THAT(toString(Http::ContentType::Json), Eq("application/json"));
}

TEST(XI_TESTSUITE, LexicalCast) {
  using namespace ::testing;
  using namespace ::Xi;
  using namespace ::Xi::Testing;

  EXPECT_THAT(fromString<Http::ContentType>(""), IsFailure());
  EXPECT_THAT(fromString<Http::ContentType>("Application/json"), IsFailure());
  EXPECT_THAT(fromString<Http::ContentType>(" application/json"), IsFailure());
  EXPECT_THAT(fromString<Http::ContentType>("application/json "), IsFailure());
  EXPECT_THAT(fromString<Http::ContentType>("application/json").valueOrThrow(), Eq(Http::ContentType::Json));
}
