/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <optional>
#include <map>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>
#include <Xi/ErrorCode.hpp>
#include <Xi/TypeSafe/Flag.hpp>
#include <Xi/Serialization/Serialization.hpp>
#include <Xi/Serialization/Flag.hpp>
#include <Xi/Crypto/PublicKey.hpp>
#include <Xi/Crypto/SecretKey.hpp>

namespace Xi {
namespace Wallet {

XI_ERROR_CODE_BEGIN(AddressExtension)
XI_ERROR_CODE_VALUE(InvalidViewKey, 0x0001)
XI_ERROR_CODE_VALUE(InvalidPaymentId, 0x0002)
XI_ERROR_CODE_VALUE(EmptyCustom, 0x0003)
XI_ERROR_CODE_END(AddressExtension, "Wallet::AddressExtensionError")

class AddressExtension {
 public:
  enum struct Feature {
    None = 0,
    ViewKey = 1 << 0,
    PaymentId = 1 << 1,
    Custom = 1 << 2,
  };

 public:
  explicit AddressExtension() = default;
  XI_DEFAULT_COPY(AddressExtension);
  XI_DEFAULT_MOVE(AddressExtension);
  ~AddressExtension() = default;

  Feature features() const;

  const std::optional<Crypto::PublicKey>& viewKey() const;
  void setViewKey(const std::optional<Crypto::PublicKey>& viewKey);

  const std::optional<Crypto::PublicKey>& paymentId() const;
  void setPaymentId(const std::optional<Crypto::PublicKey>& paymentId);

  const std::map<std::string, std::string>& custom() const;
  std::map<std::string, std::string>& custom();

  Result<void> serialize(Serialization::Serializer& serializer);

 private:
  std::optional<Crypto::PublicKey> m_viewKey{std::nullopt};
  std::optional<Crypto::PublicKey> m_paymentId{std::nullopt};
  std::map<std::string, std::string> m_custom{/* */};
};

XI_TYPESAFE_FLAG_MAKE_OPERATIONS(AddressExtension::Feature)
XI_SERIALIZATION_FLAG(AddressExtension::Feature)

}  // namespace Wallet
}  // namespace Xi

XI_SERIALIZATION_FLAG_RANGE(Xi::Wallet::AddressExtension::Feature, ViewKey, Custom)
XI_SERIALIZATION_FLAG_TAG(Xi::Wallet::AddressExtension::Feature, ViewKey, "view_key")
XI_SERIALIZATION_FLAG_TAG(Xi::Wallet::AddressExtension::Feature, PaymentId, "payment_id")
XI_SERIALIZATION_FLAG_TAG(Xi::Wallet::AddressExtension::Feature, Custom, "custom")

XI_ERROR_CODE_OVERLOADS(Xi::Wallet, AddressExtension)
