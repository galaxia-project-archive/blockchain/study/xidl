/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/Cpp/FileMapper.hpp"

#include <string>
#include <utility>

#include <Xi/Exceptions.hpp>
#include <Xi/String/String.hpp>
#include <Xi/FileSystem.h>
#include <Xi/Idl/Generator/Primitive.hpp>
#include <Xi/Idl/Generator/Typedef.hpp>
#include <Xi/Idl/Generator/Alias.hpp>
#include <Xi/Idl/Generator/Vector.hpp>
#include <Xi/Idl/Generator/Array.hpp>
#include <Xi/Idl/Generator/Variant.hpp>

namespace Xi {
namespace Idl {
namespace Generator {
namespace Cpp {

namespace {
const std::string HeaderFileEnding{".hpp"};
const std::string SourceFileEnding{".cpp"};
}  // namespace

Result<FileSystem::Directory> FileMapper::headerDirectory(SharedConstNamespace ns) const {
  XI_EXCEPTIONAL_IF_NOT(NullArgumentError, ns)
  XI_EXCEPTIONAL_IF(InvalidArgumentError, equalTo(ns, Namespace::Global))
  return m_include.relativeDirectory(namespaceHeaderPath(ns));
}

Result<FileSystem::File> FileMapper::headerFile(const std::string& name, SharedConstNamespace ns) const {
  return m_include.relativeFile(includeFilePath(name, ns));
}

Result<FileSystem::File> FileMapper::headerFile(const Type& type) const {
  return m_include.relativeFile(includeFilePath(type));
}

Result<FileSystem::Directory> FileMapper::sourceDirectory(const Type& type) const {
  return m_source.relativeDirectory(namespaceSourcePath(type.namespace_()));
}

Result<FileSystem::File> FileMapper::sourceFile(const Type& type) const {
  auto dir = sourceDirectory(type);
  XI_ERROR_PROPAGATE(dir)
  return dir->relativeFile(type.name() + SourceFileEnding);
}

Result<IncludeCollection> FileMapper::includes(const Type& type) const {
  XI_ERROR_TRY
  IncludeCollection reval{};
  if (type.isOptional()) {
    reval.add(Include::Std::optional);
  }
  if (type.isPrimitive()) {
    if (const auto primitiveInclude = include(type.asPrimitive())) {
      reval.add(*primitiveInclude);
    }
  } else if (type.isArray()) {
    reval.add(Include::Std::array);
    XI_ERROR_PROPAGATE_CATCH(includes(type.asArray().type(), reval))
  } else if (type.isVector()) {
    reval.add(Include::Std::vector);
    XI_ERROR_PROPAGATE_CATCH(includes(type.asVector().type(), reval))
  } else {
    reval.add(Include{includeFilePath(type), Include::Kind::ThisLibrary});
  }

  XI_SUCCEED(std::move(reval));
  XI_ERROR_CATCH
}

Result<void> FileMapper::includes(const Type& type, IncludeCollection& out) const {
  auto includes_ = includes(type);
  XI_ERROR_PROPAGATE(includes_)
  for (const auto& i : includes_->includes()) {
    out.add(i);
  }
  XI_SUCCEED()
}

std::string FileMapper::includeDirectoryPath(SharedConstNamespace ns) const {
  XI_EXCEPTIONAL_IF_NOT(NullArgumentError, ns)
  return namespaceIncludePath(ns);
}

std::string FileMapper::includeFilePath(const std::string& name, SharedConstNamespace ns) const {
  return includeDirectoryPath(ns) + "/" + name + HeaderFileEnding;
}

std::string FileMapper::includeFilePath(const Type& type) const {
  return includeFilePath(type.name(), type.namespace_());
}

std::vector<std::string> FileMapper::namespacePath(SharedConstNamespace ns) const {
  XI_EXCEPTIONAL_IF_NOT(NullArgumentError, ns)
  const auto nsPath = ns->path();
  std::vector<std::string> segments{};
  segments.reserve(nsPath.size());
  std::transform(begin(nsPath), end(nsPath), std::back_inserter(segments),
                 [](const auto& segment) { return segment->name(); });
  return segments;
}

std::string FileMapper::namespaceHeaderPath(SharedConstNamespace ns) const {
  return join(namespacePath(ns), FileSystem::PathSeperator);
}

std::string FileMapper::namespaceSourcePath(SharedConstNamespace ns) const {
  auto segments = namespacePath(ns);
  for (auto& segment : segments) {
    if (segment.empty()) {
      continue;
    }
    segment[0] = toLower(segment[0]);
  }
  return join(segments, FileSystem::PathSeperator);
}

std::string FileMapper::namespaceIncludePath(SharedConstNamespace ns) const {
  return join(namespacePath(ns), "/");
}

std::optional<Include> FileMapper::include(const Primitive& primitve) const {
  switch (primitve.kind()) {
    case Primitive::Kind::Int8:
    case Primitive::Kind::Int16:
    case Primitive::Kind::Int32:
    case Primitive::Kind::Int64:
    case Primitive::Kind::UInt8:
    case Primitive::Kind::UInt16:
    case Primitive::Kind::UInt32:
    case Primitive::Kind::UInt64:
      return Include::Std::cinttypes;
    case Primitive::Kind::String:
      return Include::Std::string;
    case Primitive::Kind::Float16:
    case Primitive::Kind::Float32:
    case Primitive::Kind::Float64:
    case Primitive::Kind::Boolean:
      return std::nullopt;
    case Primitive::Kind::Byte:
      return Include::Xi::Byte;
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

Result<FileMapper> makeFileMapper(const std::string& outputpath) {
  XI_ERROR_TRY
  FileMapper reval{/* */};
  {
    auto root = FileSystem::makeDirectory(outputpath);
    XI_ERROR_PROPAGATE(root)
    reval.m_root = root.take();
  }
  {
    auto include = reval.m_root.relativeDirectory("include");
    XI_ERROR_PROPAGATE(include)
    reval.m_include = include.take();
  }
  {
    auto source = reval.m_root.relativeDirectory("source");
    XI_ERROR_PROPAGATE(source)
    reval.m_source = source.take();
  }
  XI_SUCCEED(reval)
  XI_ERROR_CATCH
}

}  // namespace Cpp
}  // namespace Generator
}  // namespace Idl
}  // namespace Xi
