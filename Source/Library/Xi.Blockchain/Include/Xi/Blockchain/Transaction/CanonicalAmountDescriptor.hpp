/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <optional>

#include <Xi/Global.hh>

#include "Xi/Blockchain/Transaction/Amount.hpp"
#include "Xi/Blockchain/Transaction/CanonicalAmount.hpp"

namespace Xi {
namespace Blockchain {
namespace Transaction {

class CanonicalAmountDescriptor {
 public:
  static std::optional<CanonicalAmountDescriptor> find(const Amount amount);

 public:
  XI_DEFAULT_COPY(CanonicalAmountDescriptor);
  XI_DEFAULT_MOVE(CanonicalAmountDescriptor);
  ~CanonicalAmountDescriptor() = default;

  uint8_t decade() const;
  uint8_t digit() const;
  CanonicalAmount amount() const;

  operator CanonicalAmount() const;

 private:
  explicit CanonicalAmountDescriptor(uint8_t decade, uint8_t digit);
  friend class CanonicalAmountDescriptorCollection;

 private:
  uint8_t m_decade;
  uint8_t m_digit;
  CanonicalAmount m_amount;
};

}  // namespace Transaction
}  // namespace Blockchain
}  // namespace Xi
