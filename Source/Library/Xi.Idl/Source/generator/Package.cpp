/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/Package.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

const std::string &Package::name() const {
  return m_name;
}

SharedConstNamespace Package::namespace_() const {
  return m_namespace;
}

const std::vector<Field> &Package::fields() const {
  return m_fields;
}

const std::optional<Type> &Package::inheritance() const {
  return m_inheritance;
}

const Documentation &Package::documentation() const {
  return m_doc;
}

void Package::setName(const std::string &id) {
  m_name = id;
}

void Package::setNamespace(SharedConstNamespace ns) {
  m_namespace = ns;
}

std::vector<Field> &Package::fields() {
  return m_fields;
}

void Package::setInheritance(const std::optional<Type> &inheritance_) {
  m_inheritance = inheritance_;
}

void Package::setDocumentation(const Documentation &doc_) {
  m_doc = doc_;
}

PackageBuilder::PackageBuilder() {
  m_package = success(Package{});
}

PackageBuilder &PackageBuilder::withName(const std::string &name) {
  if (!m_package.isError()) {
    m_package->setName(name);
  }
  return *this;
}

PackageBuilder &PackageBuilder::withNamespace(SharedConstNamespace ns) {
  if (!m_package.isError()) {
    m_package->setNamespace(ns);
  }
  return *this;
}

PackageBuilder &PackageBuilder::withField(const Field &field) {
  if (!m_package.isError()) {
    m_package->fields().push_back(field);
  }
  return *this;
}

PackageBuilder &PackageBuilder::withInheritance(const std::optional<Type> &type) {
  if (!m_package.isError()) {
    m_package->setInheritance(type);
  }
  return *this;
}

PackageBuilder &PackageBuilder::withDocumentation(const Documentation &doc) {
  if (!m_package.isError()) {
    m_package->setDocumentation(doc);
  }
  return *this;
}

Result<Package> PackageBuilder::build() {
  return m_package;
}

Result<Type> makeType(const Package &package) {
  return TypeBuilder{}
      .withKind(Type::Kind::Package)
      .withName(package.name())
      .withNamespace(package.namespace_())
      .withIsOptional(false)
      .withData(package)
      .build();
}

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi
