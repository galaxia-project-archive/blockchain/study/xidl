/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>

#include "Xi/Idl/Generator/Type.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

using VectorSize = uint16_t;

class Vector {
 public:
  XI_DEFAULT_COPY(Vector);
  XI_DEFAULT_MOVE(Vector);

  const Type& type() const;
  VectorSize minimumSize() const;
  VectorSize maximumSize() const;

 private:
  Vector() = default;

  void setType(const Type& type_);
  void setMinimumSize(VectorSize size_);
  void setMaximumSize(VectorSize size_);

  friend class VectorBuilder;

 private:
  Type m_type;
  uint16_t m_minimumSize;
  uint16_t m_maximumSize;
};

class VectorBuilder {
 public:
  VectorBuilder();
  XI_DELETE_COPY(VectorBuilder);
  XI_DELETE_MOVE(VectorBuilder);

  VectorBuilder& withType(const Type& type);
  VectorBuilder& withMinimumSize(VectorSize size);
  VectorBuilder& withMaximumSize(VectorSize size);

  Result<Vector> build();

 private:
  Result<Vector> m_vector;
};

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi
