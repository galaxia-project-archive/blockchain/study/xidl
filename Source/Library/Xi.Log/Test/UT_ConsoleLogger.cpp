/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <Xi/Extern/Push.hh>
#include <gmock/gmock.h>
#include <Xi/Extern/Pop.hh>

#include <iostream>

#include <Xi/Log/ConsoleLogger.hpp>
#include <Xi/Log/Log.hpp>
#include <Xi/Log/Registry.hpp>

#define XI_TEST_SUITE Xi_Log_ConsoleLogger

TEST(XI_TEST_SUITE, StdOutSink) {
  using namespace ::testing;
  using namespace Xi::Log;

  Registry::put(ConsoleLoggerBuilder{}.withColoring().withStandardStream().build());
  auto handle = Registry::get("StdOutSink");
  handle.debug("The answer is {}. Just took {:.2f} seconds to find out.", 42, 1.23);
}

XI_LOGGER("Xi/Log/Test")

TEST(XI_TEST_SUITE, HandleLog) {
  XI_INFO("Some cats may meow! ^-.-^");
}
