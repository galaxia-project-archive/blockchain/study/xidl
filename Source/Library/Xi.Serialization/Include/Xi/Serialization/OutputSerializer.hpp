/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string_view>
#include <cinttypes>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>
#include <Xi/Byte.hh>

#include "Xi/Serialization/Format.hpp"
#include "Xi/Serialization/Tag.hpp"

namespace Xi {
namespace Serialization {
class OutputSerializer {
 public:
  virtual ~OutputSerializer() = default;

  [[nodiscard]] virtual Format format() const = 0;

  [[nodiscard]] virtual Result<void> writeInt8(std::int8_t value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> writeUInt8(std::uint8_t value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> writeInt16(std::int16_t value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> writeUInt16(std::uint16_t value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> writeInt32(std::int32_t value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> writeUInt32(std::uint32_t value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> writeInt64(std::int64_t value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> writeUInt64(std::uint64_t value, const Tag& nameTag) = 0;

  [[nodiscard]] virtual Result<void> writeBoolean(bool value, const Tag& nameTag) = 0;

  [[nodiscard]] virtual Result<void> writeFloat(float value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> writeDouble(double value, const Tag& nameTag) = 0;

  [[nodiscard]] virtual Result<void> writeString(const std::string_view value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> writeBinary(ConstByteSpan value, const Tag& nameTag) = 0;

  [[nodiscard]] virtual Result<void> writeBlob(ConstByteSpan value, const Tag& nameTag) = 0;

  [[nodiscard]] virtual Result<void> writeTag(const Tag& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> writeFlag(const TagVector& tag, const Tag& nameTag) = 0;

  [[nodiscard]] virtual Result<void> beginWriteComplex(const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> endWriteComplex() = 0;

  [[nodiscard]] virtual Result<void> beginWriteVector(size_t size, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> endWriteVector() = 0;

  [[nodiscard]] virtual Result<void> beginWriteArray(size_t size, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> endWriteArray() = 0;

  [[nodiscard]] virtual Result<void> writeNull(const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> writeNotNull(const Tag& nameTag) = 0;
};

XI_DECLARE_SMART_POINTER(OutputSerializer)

}  // namespace Serialization
}  // namespace Xi
