/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string>
#include <algorithm>

#include "Xi/Idl/Parser/SourcePosition.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

struct Token {
  std::string value;
  SourcePosition begin;
  SourcePosition end;

  static bool isSpace(const char ch);
  static bool isNewLine(const char ch);
  static bool isVoid(const char ch);

  bool isEmpty() const;
  bool isEndOfStream() const;
  bool isSpace() const;
  bool isNewLine() const;
  bool isVoid() const;

  std::string stringify() const;
};

Token makeEndOfStreamToken(const SourcePosition& currentPosition);

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi
