/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/Variant.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

const std::string &Variant::name() const {
  return m_name;
}

SharedConstNamespace Variant::namespace_() const {
  return m_namespace;
}

const Documentation &Variant::documentaion() const {
  return m_doc;
}

const std::vector<Field> &Variant::fields() const {
  return m_fields;
}

void Variant::setName(const std::string &id) {
  m_name = id;
}

void Variant::setNamespace(SharedConstNamespace ns) {
  m_namespace = ns;
}

void Variant::setDocumentation(const Documentation &doc_) {
  m_doc = doc_;
}

std::vector<Field> &Variant::fields() {
  return m_fields;
}

VariantBuilder::VariantBuilder() {
  m_Variant = success(Variant{});
}

VariantBuilder &VariantBuilder::withName(const std::string &name) {
  if (!m_Variant.isError()) {
    m_Variant->setName(name);
  }
  return *this;
}

VariantBuilder &VariantBuilder::withNamespace(SharedConstNamespace ns) {
  if (!m_Variant.isError()) {
    m_Variant->setNamespace(ns);
  }
  return *this;
}

VariantBuilder &VariantBuilder::withDocumentation(const Documentation &doc) {
  if (!m_Variant.isError()) {
    m_Variant->setDocumentation(doc);
  }
  return *this;
}

VariantBuilder &VariantBuilder::withField(const Field &field) {
  if (!m_Variant.isError()) {
    m_Variant->fields().push_back(field);
  }
  return *this;
}

Result<Variant> VariantBuilder::build() {
  return m_Variant;
}

Result<Type> makeType(const Variant &variant) {
  return TypeBuilder{}
      .withKind(Type::Kind::Variant)
      .withName(variant.name())
      .withNamespace(variant.namespace_())
      .withIsOptional(false)
      .withData(variant)
      .build();
}

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi
