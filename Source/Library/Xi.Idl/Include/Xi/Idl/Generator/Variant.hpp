/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string>
#include <vector>
#include <functional>

#include <Xi/Global.hh>

#include "Xi/Idl/Generator/Field.hpp"
#include "Xi/Idl/Generator/Namespace.hpp"
#include "Xi/Idl/Generator/Documentation.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

class Variant {
 public:
  XI_DEFAULT_COPY(Variant);
  XI_DEFAULT_MOVE(Variant);

  const std::string& name() const;
  SharedConstNamespace namespace_() const;
  const Documentation& documentaion() const;
  const std::vector<Field>& fields() const;

 private:
  Variant() = default;

  void setName(const std::string& id);
  void setNamespace(SharedConstNamespace ns);
  void setDocumentation(const Documentation& doc_);
  std::vector<Field>& fields();

  friend class VariantBuilder;

 private:
  std::string m_name;
  SharedConstNamespace m_namespace;
  Documentation m_doc;
  std::vector<Field> m_fields;
};

class VariantBuilder {
 public:
  VariantBuilder();
  XI_DELETE_COPY(VariantBuilder);
  XI_DELETE_MOVE(VariantBuilder);

  VariantBuilder& withName(const std::string& name);
  VariantBuilder& withNamespace(SharedConstNamespace ns);
  VariantBuilder& withDocumentation(const Documentation& doc);
  VariantBuilder& withField(const Field& field);

  Result<Variant> build();

 private:
  Result<Variant> m_Variant;
};

Result<Type> makeType(const Variant& variant);

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi
