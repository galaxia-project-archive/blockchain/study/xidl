/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Stream/IStream.hpp"

#include <utility>

#include "Xi/Stream/StreamError.hpp"

namespace Xi {
namespace Stream {

IStream::IStream(Ownership<std::istream> source) : m_source{std::move(source)} {
  /* */
}

Result<size_t> IStream::read(ByteSpan buffer) {
  const auto isEos = isEndOfStream();
  XI_ERROR_PROPAGATE(isEos)
  if (*isEos) {
    return success<size_t>(0);
  }
  m_source.get().read(reinterpret_cast<char *>(buffer.data()), static_cast<std::streamsize>(buffer.size()));
  auto read = m_source.get().gcount();
  if (read <= 0) {
    XI_FAIL_IF(m_source.get().fail(), StreamError::BadState)
    return success<size_t>(0);
  } else {
    return success(static_cast<size_t>(read));
  }
}

Result<bool> IStream::isEndOfStream() const {
  XI_SUCCEED_IF(m_source.get().eof(), true);
  XI_FAIL_IF(m_source.get().fail(), StreamError::BadState);
  XI_SUCCEED(false);
}

Result<size_t> IStream::tell() const {
  const auto pos = m_source.get().tellg();
  XI_FAIL_IF(pos < 0, StreamError::BadState)
  return success(static_cast<size_t>(pos));
}

Result<Byte> IStream::peek() const {
  const auto isEos = isEndOfStream();
  XI_ERROR_PROPAGATE(isEos)
  XI_FAIL_IF(*isEos, StreamError::EndOfStream)
  const auto byte = static_cast<char>(m_source.get().peek());
  return success(static_cast<Byte>(byte));
}

Result<Byte> IStream::take() {
  const auto isEos = isEndOfStream();
  XI_ERROR_PROPAGATE(isEos)
  XI_FAIL_IF(*isEos, StreamError::EndOfStream)
  const auto byte = static_cast<char>(m_source.get().get());
  return success(static_cast<Byte>(byte));
}

}  // namespace Stream
}  // namespace Xi
