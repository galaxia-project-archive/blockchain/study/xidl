/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>

#include "Xi/Idl/Generator/Namespace.hpp"
#include "Xi/Idl/Generator/Documentation.hpp"
#include "Xi/Idl/Generator/Type.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

class Typedef {
 public:
  XI_DEFAULT_COPY(Typedef);
  XI_DEFAULT_MOVE(Typedef);

  const std::string& name() const;
  SharedConstNamespace namespace_() const;
  const Documentation& documentation() const;
  const Type& type() const;

 private:
  Typedef() = default;

  void setName(const std::string& name_);
  void setNamespace(SharedConstNamespace ns);
  void setDocumentation(const Documentation& doc_);
  void setType(const Type& type_);

  friend class TypedefBuilder;

 private:
  std::string m_name;
  SharedConstNamespace m_namespace;
  Documentation m_doc;
  Type m_type;
};

class TypedefBuilder {
 public:
  TypedefBuilder();
  XI_DELETE_COPY(TypedefBuilder);
  XI_DELETE_MOVE(TypedefBuilder);

  TypedefBuilder& withName(const std::string& name);
  TypedefBuilder& withNamespace(SharedConstNamespace ns);
  TypedefBuilder& withDocumentation(const Documentation& doc);
  TypedefBuilder& withType(const Type& type);

  Result<Typedef> build();

 private:
  Result<Typedef> m_Typedef;
};

Result<Type> makeType(const Typedef& typedef_);

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi
