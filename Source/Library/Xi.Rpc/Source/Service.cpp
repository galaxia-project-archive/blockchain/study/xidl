/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rpc/Service.hpp"

#include <utility>

#include <Xi/Log/Log.hpp>
#include <Xi/Exceptions.hpp>
#include <Xi/Serialization/Serializer.hpp>

#include "Xi/Rpc/ServiceError.hpp"

XI_LOGGER("Rpc/Service")

namespace Xi {
namespace Rpc {

const std::unordered_set<SharedConstCommand> &Service::commands() const {
  return m_commands;
}

Result<void> Service::addService(SharedConstCommand command, SharedService service) {
  XI_EXCEPTIONAL_IF_NOT(NullArgumentError, command)
  XI_FAIL_IF(command->binary() == Command::Null.binary(), ServiceError::CommandNull)
  XI_FAIL_IF(command->text() == Command::Null.text(), ServiceError::CommandNull)
  XI_FAIL_IF(queryService(command->binary()), ServiceError::CommandAlreadyExists)
  XI_FAIL_IF(queryService(command->text()), ServiceError::CommandAlreadyExists)
  XI_FAIL_IF(queryContract(command->binary()), ServiceError::CommandAlreadyExists)
  XI_FAIL_IF(queryContract(command->text()), ServiceError::CommandAlreadyExists)

  m_serviceByBinary[command->binary()] = service;
  m_serviceByText[command->text()] = service;
  m_commands.insert(command);

  XI_SUCCEED()
}

Result<void> Service::addContract(SharedConstCommand command, SharedContract contract) {
  XI_EXCEPTIONAL_IF_NOT(NullArgumentError, command)
  XI_FAIL_IF(command->binary() == Command::Null.binary(), ServiceError::CommandNull)
  XI_FAIL_IF(command->text() == Command::Null.text(), ServiceError::CommandNull)
  XI_FAIL_IF(queryContract(command->binary()), ServiceError::CommandAlreadyExists)
  XI_FAIL_IF(queryContract(command->text()), ServiceError::CommandAlreadyExists)
  XI_FAIL_IF(queryService(command->binary()), ServiceError::CommandAlreadyExists)
  XI_FAIL_IF(queryService(command->text()), ServiceError::CommandAlreadyExists)

  m_contractByBinary[command->binary()] = contract;
  m_contractByText[command->text()] = contract;
  m_commands.insert(command);

  XI_SUCCEED()
}

SharedContract Service::queryContract(SharedConstCommand command) const {
  XI_EXCEPTIONAL_IF_NOT(NullArgumentError, command)
  auto reval = queryContract(command->binary());
  XI_RETURN_SC_IF(reval, reval);
  XI_RETURN_SC(queryContract(command->text()));
}

SharedContract Service::queryContract(Command::PathSpan path) const {
  XI_RETURN_SC_IF(path.empty(), nullptr);
  if (path.size() == 1) {
    return queryContract(path[0]);
  } else {
    auto service = queryService(path[0]);
    XI_RETURN_SC_IF_NOT(service, nullptr);
    return service->queryContract(path.slice(1));
  }
}

SharedContract Service::queryContract(Command::Binary binary) const {
  XI_RETURN_SC_IF(binary == Command::Null.binary(), nullptr);
  const auto search = m_contractByBinary.find(binary);
  if (search == m_contractByBinary.end()) {
    return nullptr;
  } else {
    return search->second;
  }
}

SharedContract Service::queryContract(Command::ConstBinarySpan binary) const {
  if (binary.empty()) {
    return nullptr;
  } else if (binary.size() == 1) {
    return queryContract(binary.front());
  } else {
    auto service = queryService(binary.front());
    XI_RETURN_SC_IF_NOT(service, nullptr);
    return service->queryContract(binary.slice(1));
  }
}

SharedContract Service::queryContract(const Command::Text &text) const {
  XI_RETURN_SC_IF(text == Command::Null.text(), nullptr);
  const auto search = m_contractByText.find(text);
  if (search == m_contractByText.end()) {
    return nullptr;
  } else {
    return search->second;
  }
}

SharedContract Service::queryContract(Command::ConstTextSpan text) const {
  if (text.empty()) {
    return nullptr;
  } else if (text.size() == 1) {
    return queryContract(text.front());
  } else {
    auto service = queryService(text.front());
    XI_RETURN_SC_IF_NOT(service, nullptr);
    return service->queryContract(text.slice(1));
  }
}

SharedService Service::queryService(SharedConstCommand command) const {
  XI_EXCEPTIONAL_IF_NOT(NullArgumentError, command)
  auto reval = queryService(command->binary());
  XI_RETURN_SC_IF(reval, reval);
  XI_RETURN_SC(queryService(command->text()));
}

SharedService Service::queryService(Command::Binary binary) const {
  XI_RETURN_SC_IF(binary == Command::Null.binary(), nullptr);
  const auto search = m_serviceByBinary.find(binary);
  if (search == m_serviceByBinary.end()) {
    return nullptr;
  } else {
    return search->second;
  }
}

SharedService Service::queryService(Command::ConstBinarySpan binary) const {
  if (binary.empty()) {
    return nullptr;
  } else if (binary.size() == 1) {
    return queryService(binary.front());
  } else {
    auto service = queryService(binary.front());
    XI_RETURN_SC_IF_NOT(service, nullptr);
    return service->queryService(binary.slice(1));
  }
}

SharedService Service::queryService(const Command::Text &text) const {
  XI_RETURN_SC_IF(text == Command::Null.text(), nullptr);
  const auto search = m_serviceByText.find(text);
  if (search == m_serviceByText.end()) {
    return nullptr;
  } else {
    return search->second;
  }
}

SharedService Service::queryService(Command::ConstTextSpan text) const {
  if (text.empty()) {
    return nullptr;
  } else if (text.size() == 1) {
    return queryService(text.front());
  } else {
    auto service = queryService(text.front());
    XI_RETURN_SC_IF_NOT(service, nullptr);
    return service->queryService(text.slice(1));
  }
}

SharedService makeService() {
  return SharedService{new Service{/* */}};
}

}  // namespace Rpc
}  // namespace Xi
