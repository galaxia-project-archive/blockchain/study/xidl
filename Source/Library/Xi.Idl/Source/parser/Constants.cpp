/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Parser/Constants.hpp"

#include "Xi/Idl/Parser/Keyword.hpp"

#include "Xi/Idl/Parser/ParserError.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

Result<void> StringConstantAutomaton::doProcessToken(const Token &token) {
  XI_FAIL_IF(token.isEndOfStream(), ParserError::UnexpectedEndOfStream);
  if (token.value == "'") {
    markFinished();
    XI_SUCCEED()
  } else {
    m_result += token.value;
    XI_SUCCEED()
  }
}

std::shared_ptr<StringConstantAutomaton> makeStringConstantAutomaton(std::string &result) {
  std::shared_ptr<StringConstantAutomaton> reval{new StringConstantAutomaton{result}};
  reval->listenToVoid();
  reval->chain(makeKeywordAutomaton("'"));
  return reval;
}

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi
