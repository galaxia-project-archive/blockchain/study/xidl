/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Parser/Keyword.hpp"

#include "Xi/Idl/Parser/ParserError.hpp"

#include <Xi/Log/Log.hpp>

XI_LOGGER("Idl/Parser/Keyword")

namespace Xi {
namespace Idl {
namespace Parser {

Result<void> KeywordAutomaton::doProcessToken(const Token &token) {
  if (!result.empty()) {
    return propagateBack(token);
  }
  XI_FAIL_IF_NOT(result.empty(), ParserError::UnexpectedToken)
  result = token.value;
  if (!expected.empty() && result != expected) {
    XI_ERROR("Expected keyword '{}' actually got '{}' at {}", expected, token, token.begin)
    XI_FAIL(ParserError::UnexpectedToken)
  }
  markFinished();
  XI_SUCCEED()
}

std::shared_ptr<KeywordAutomaton> makeKeywordAutomaton(const std::string &expected) {
  std::shared_ptr<KeywordAutomaton> reval{new KeywordAutomaton{}};
  reval->expected = expected;
  return reval;
}

std::shared_ptr<OptionalKeywordAutomaton> makeOptionalKeywordAutomaton(const std::string &id, bool &res) {
  res = false;
  return std::shared_ptr<OptionalKeywordAutomaton>{new OptionalKeywordAutomaton{id, res}};
}

OptionalKeywordAutomaton::OptionalKeywordAutomaton(const std::string &key, bool &res)
    : ResultAutomaton<bool>(res), m_identicator{key} {
}

Result<void> OptionalKeywordAutomaton::doProcessToken(const Token &token) {
  if (token.value == m_identicator) {
    m_result = true;
    markFinished();
    XI_SUCCEED()
  } else {
    m_result = false;
    return propagateBack(token);
  }
}

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi
