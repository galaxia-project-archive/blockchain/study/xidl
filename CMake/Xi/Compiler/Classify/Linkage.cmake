# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_COMPILER_CLASSIFY_LINKAGE)
    return()
endif()
set(CMAKE_XI_COMPILER_CLASSIFY_LINKAGE TRUE)

if(DEFINED BUILD_SHARED_LIBS AND BUILD_SHARED_LIBS)
    set(XI_COMPILER_SHARED ON CACHE INTERNAL "" FORCE)
    set(XI_COMPILER_STATIC OFF CACHE INTERNAL "" FORCE)
    set(XI_COMPILER_LINK_TYPE "SHARED" CACHE INTERNAL "" FORCE)
else()
    set(XI_COMPILER_STATIC ON CACHE INTERNAL "" FORCE)
    set(XI_COMPILER_SHARED OFF CACHE INTERNAL "" FORCE)
    set(XI_COMPILER_LINK_TYPE "STATIC" CACHE INTERNAL "" FORCE)
endif()

