# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_CONFIGURE_FILE)
    return()
endif()
set(CMAKE_XI_CONFIGURE_FILE TRUE)

xi_include(Xi/Log)

macro(xi_configure_file input output)
    if(EXISTS ${output})
        set(temp_output ${output}.temp)
        configure_file(${input} ${temp_output} ${ARGN})
        file(SHA256 ${temp_output} temp_hash)
        file(SHA256 ${output} out_hash)
        if(NOT temp_hash STREQUAL out_hash)
            file(RENAME ${temp_output} ${output})
            xi_status("Configuration file updated." ${output})
        else()
            file(REMOVE ${temp_output})
        endif()
    else()
        configure_file(${input} ${output} ${ARGN})
        xi_status("Configuration file initialized." ${output})
    endif()
endmacro()
