# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_MAKE_FILESEARCH)
    return()
endif()
set(CMAKE_XI_MAKE_FILESEARCH TRUE)

include(CMakeParseArguments)

xi_include(Xi/Compiler)

# xi_make_file_search(
#   <out>                       < Variable to store result to.
#   <dir>                       < Source directory to search at
#   [HEADERS]                   < Includes header file endings to search expression.
#   [SOURCES]                   < Includes source file endings to search expression.
#   [NO_RECURSE]                < Disables recusrive search.
# )
#
# Used to search for source files. By convention source files are categorized the following:
#   - .h            < C header files
#   - .hh           < Mixed c and c++ header files
#   - .hpp          < C++ header files
#   - .c            < C source files
#   - .cpp          < C++ source files
#
# If you search for source files consider this method instead of a plain cmake file search
# as conventions may change and this script will then be changed accordingly.
#
# Please not source file extensions are also used to determine additional source file compiler flags,
# see SetCompilerFlags.cmake .
function(xi_make_file_search out dir)
    cmake_parse_arguments(
        XI_MAKE_FILE_SEARCH
        "HEADERS;SOURCES;NO_RECURSE"
        ""
        ""
        ${ARGN}
    )

    if(NOT XI_MAKE_FILE_SEARCH_HEADERS AND NOT XI_MAKE_FILE_SEARCH_SOURCES)
        set(XI_MAKE_FILE_SEARCH_HEADERS ON)
        set(XI_MAKE_FILE_SEARCH_SOURCES ON)
    endif()

    set(extensions)
    if(XI_MAKE_FILE_SEARCH_HEADERS)
        list(APPEND extensions
            "*.h"                   # C header files
            "*.hh"                  # Mixed c and c++ header files
            "*.hpp"                 # C++ header files
        )
    endif()
    if(XI_MAKE_FILE_SEARCH_SOURCES)
        list(APPEND extensions
            "*.c"                   # C source files
            "*.cpp"                 # C++ source files
        )
    endif()

    set(recursion GLOB_RECURSE)
    if(XI_MAKE_FILE_SEARCH_NO_RECURSE)
        set(recursion GLOB)
    endif()

    set(${out})
    foreach(extension ${extensions})
        file(${recursion} iFiles LIST_DIRECTORIES OFF "${dir}/${extension}")
        list(APPEND ${out} ${iFiles})
    endforeach()

    set(${out} ${${out}} PARENT_SCOPE)
endfunction()

