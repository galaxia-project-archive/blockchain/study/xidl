/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <map>
#include <vector>
#include <utility>
#include <stack>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>
#include <Xi/ErrorCode.hpp>

#include "Xi/Idl/Parser/FileContent.hpp"
#include "Xi/Idl/Generator/Generator.hpp"
#include "Xi/Idl/Generator/Scope.hpp"
#include "Xi/Idl/Generator/Type.hpp"
#include "Xi/Idl/Generator/Namespace.hpp"
#include "Xi/Idl/Generator/Documentation.hpp"
#include "Xi/Idl/Generator/SerializationTag.hpp"

#include "Xi/Idl/Cache.hpp"

namespace Xi {
namespace Idl {

XI_ERROR_CODE_BEGIN(Processor)
XI_ERROR_CODE_VALUE(CircularDependency, 0x00001)
XI_ERROR_CODE_VALUE(ReferenceNotFound, 0x00002)
XI_ERROR_CODE_VALUE(NamespaceNotFound, 0x00003)
XI_ERROR_CODE_VALUE(ServeReferenceAmbigious, 0x00004)
XI_ERROR_CODE_END(Processor, "xidl file processing error")

class Processor {
 public:
  static Result<void> process(const std::string& file, Generator::Generator& gen);

 private:
  Result<void> doProcess(const std::string& file, Generator::Generator& gen);

  /// Performs an import operation of filepath, stated as import ... in currentFilepath
  /// Multiple imports of equivalent filepaths are persisted in a cache with the lifetime of this processor.
  Result<SharedCacheEntry> importFile(const std::string& filepath, const std::string& currentFilepath);
  Result<Generator::Scope> buildScope(const Parser::FileContent& content);
  Result<Generator::Scope> buildScope(const Parser::FileContent& content, Generator::Generator& gen);
  Result<Generator::Type> buildType(const Parser::Type& type, Generator::Scope& scope);
  Result<Generator::Documentation> buildDocumentation(const Parser::Documentation& doc);
  Result<Generator::SerializationTag> buildTag(const Parser::Tag& tag);

  Result<Generator::SharedConstNamespace> resolveNamespace(const Parser::Reference& ref, const Generator::Scope& scope);
  Result<Generator::Type> resolveTypeReference(const Parser::Reference& ref, const Generator::Scope& scope);
  Result<Generator::Contract> resolveContractReference(const Parser::Reference& ref, const Generator::Scope& scope);
  Result<Generator::Service> resolveServiceReference(const Parser::Reference& ref, const Generator::Scope& scope);

  Processor() = default;

 private:
  std::vector<std::string> m_importStack;
  Cache m_cache;
};

}  // namespace Idl
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Idl, Processor)
