/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Serialization/Yaml/OutputSerializer.hpp"

#include <stack>

#include <Xi/Extern/Push.hh>
#include <yaml-cpp/yaml.h>
#include <Xi/Extern/Pop.hh>

#include <Xi/Encoding/Base64.hh>

#include "Xi/Serialization/Yaml/YamlError.hpp"

namespace Xi {
namespace Serialization {
namespace Yaml {

struct OutputSerializer::_Impl {
  YAML::Emitter emitter;

  enum EntityType {
    OBJECT,
    ARRAY,
  };
  std::stack<EntityType> stack{};

  explicit _Impl(std::ostream& stream_) : emitter{stream_} {
    /* */
  }

  template <typename _T>
  Result<void> emit(_T& value, const Tag& nameTag) {
    XI_ERROR_TRY
    XI_FAIL_IF(stack.empty(), YamlError::Internal);
    if (stack.top() == _Impl::OBJECT) {
      emitter << YAML::Key << nameTag.text() << YAML::Value;
      emitter << value;
      XI_SUCCEED()
    } else if (stack.top() == _Impl::ARRAY) {
      emitter << value;
      XI_SUCCEED()
    } else {
      XI_SUCCEED()
    }
    XI_ERROR_CATCH
  }
};

OutputSerializer::OutputSerializer(std::ostream& stream) : m_impl{new _Impl{stream}} {
  /* */
}

OutputSerializer::~OutputSerializer() {
  /* */
}

Format Xi::Serialization::Yaml::OutputSerializer::format() const {
  return Format::HumanReadable;
}

Result<void> Xi::Serialization::Yaml::OutputSerializer::writeInt8(int8_t value, const Tag& nameTag) {
  int16_t _ = value;
  return m_impl->emit(_, nameTag);
}

Result<void> Xi::Serialization::Yaml::OutputSerializer::writeUInt8(uint8_t value, const Tag& nameTag) {
  uint16_t _ = value;
  return m_impl->emit(_, nameTag);
}

Result<void> Xi::Serialization::Yaml::OutputSerializer::writeInt16(int16_t value, const Tag& nameTag) {
  return m_impl->emit(value, nameTag);
}

Result<void> Xi::Serialization::Yaml::OutputSerializer::writeUInt16(uint16_t value, const Tag& nameTag) {
  return m_impl->emit(value, nameTag);
}

Result<void> Xi::Serialization::Yaml::OutputSerializer::writeInt32(int32_t value, const Tag& nameTag) {
  return m_impl->emit(value, nameTag);
}

Result<void> Xi::Serialization::Yaml::OutputSerializer::writeUInt32(uint32_t value, const Tag& nameTag) {
  return m_impl->emit(value, nameTag);
}

Result<void> Xi::Serialization::Yaml::OutputSerializer::writeInt64(int64_t value, const Tag& nameTag) {
  return m_impl->emit(value, nameTag);
}

Result<void> Xi::Serialization::Yaml::OutputSerializer::writeUInt64(uint64_t value, const Tag& nameTag) {
  return m_impl->emit(value, nameTag);
}

Result<void> Xi::Serialization::Yaml::OutputSerializer::writeBoolean(bool value, const Tag& nameTag) {
  return m_impl->emit(value, nameTag);
}

Result<void> Xi::Serialization::Yaml::OutputSerializer::writeFloat(float value, const Tag& nameTag) {
  return m_impl->emit(value, nameTag);
}

Result<void> Xi::Serialization::Yaml::OutputSerializer::writeDouble(double value, const Tag& nameTag) {
  return m_impl->emit(value, nameTag);
}

Result<void> Xi::Serialization::Yaml::OutputSerializer::writeTag(const Tag& value, const Tag& nameTag) {
  XI_FAIL_IF(value.text() == Tag::NoTextTag, YamlError::NullTag)
  return m_impl->emit(value.text(), nameTag);
}

Result<void> Xi::Serialization::Yaml::OutputSerializer::writeFlag(const TagVector& flag, const Tag& nameTag) {
  auto size = flag.size();
  XI_ERROR_PROPAGATE_CATCH(beginWriteVector(size, nameTag));
  for (auto& iFlag : flag) {
    XI_FAIL_IF(iFlag.text() == Tag::NoTextTag, YamlError::NullTag);
    XI_ERROR_PROPAGATE_CATCH(m_impl->emit(iFlag.text(), Tag::Null));
  }
  XI_ERROR_PROPAGATE_CATCH(endWriteVector());
  XI_SUCCEED()
}

Result<void> Xi::Serialization::Yaml::OutputSerializer::writeString(const std::string_view value, const Tag& nameTag) {
  std::string copy{value.data(), value.size()};
  return m_impl->emit(copy, nameTag);
}

Result<void> Xi::Serialization::Yaml::OutputSerializer::writeBinary(ConstByteSpan value, const Tag& nameTag) {
  return writeString(Encoding::Base64::encode(value), nameTag);
}

Result<void> Xi::Serialization::Yaml::OutputSerializer::writeBlob(ConstByteSpan value, const Tag& nameTag) {
  return writeString(Encoding::Base64::encode(value), nameTag);
}

Result<void> Xi::Serialization::Yaml::OutputSerializer::beginWriteComplex(const Tag& nameTag) {
  XI_ERROR_TRY
  if (m_impl->stack.empty()) {
    m_impl->emitter << YAML::BeginMap;
    m_impl->stack.push(_Impl::OBJECT);
    XI_SUCCEED()
  } else if (m_impl->stack.top() == _Impl::OBJECT) {
    m_impl->emitter << YAML::Key;
    m_impl->emitter << nameTag.text();
    m_impl->emitter << YAML::Value << YAML::BeginMap;
    m_impl->stack.push(_Impl::OBJECT);
    XI_SUCCEED()
  } else if (m_impl->stack.top() == _Impl::ARRAY) {
    m_impl->emitter << YAML::BeginMap;
    m_impl->stack.push(_Impl::OBJECT);
    XI_SUCCEED()
  } else {
    XI_FAIL(YamlError::TypeMissmatchContainer)
  }
  XI_ERROR_CATCH
}

Result<void> Xi::Serialization::Yaml::OutputSerializer::endWriteComplex() {
  XI_ERROR_TRY
  XI_FAIL_IF(m_impl->stack.empty(), YamlError::NoValue);
  XI_FAIL_IF_NOT(m_impl->stack.top() == _Impl::OBJECT, YamlError::TypeMissmatchObject);
  m_impl->stack.pop();
  m_impl->emitter << YAML::EndMap;
  XI_SUCCEED()
  XI_ERROR_CATCH
}

Result<void> Xi::Serialization::Yaml::OutputSerializer::beginWriteVector(size_t size, const Tag& nameTag) {
  XI_UNUSED(size)
  XI_ERROR_TRY
  if (m_impl->stack.empty()) {
    m_impl->emitter << YAML::BeginSeq;
    m_impl->stack.push(_Impl::ARRAY);
    XI_SUCCEED()
  } else if (m_impl->stack.top() == _Impl::OBJECT) {
    m_impl->emitter << YAML::Key;
    m_impl->emitter << nameTag.text();
    m_impl->emitter << YAML::Value << YAML::BeginSeq;
    m_impl->stack.push(_Impl::ARRAY);
    XI_SUCCEED()
  } else if (m_impl->stack.top() == _Impl::ARRAY) {
    m_impl->emitter << YAML::BeginSeq;
    m_impl->stack.push(_Impl::ARRAY);
    XI_SUCCEED()
  } else {
    XI_FAIL(YamlError::TypeMissmatchContainer)
  }
  XI_ERROR_CATCH
}

Result<void> Xi::Serialization::Yaml::OutputSerializer::endWriteVector() {
  XI_ERROR_TRY
  XI_FAIL_IF(m_impl->stack.empty(), YamlError::NoValue);
  XI_FAIL_IF_NOT(m_impl->stack.top() == _Impl::ARRAY, YamlError::TypeMissmatchArray);
  m_impl->stack.pop();
  m_impl->emitter << YAML::EndSeq;
  XI_SUCCEED()
  XI_ERROR_CATCH
}

Result<void> Xi::Serialization::Yaml::OutputSerializer::beginWriteArray(size_t size, const Tag& nameTag) {
  return beginWriteVector(size, nameTag);
}

Result<void> Xi::Serialization::Yaml::OutputSerializer::endWriteArray() {
  return endWriteVector();
}

Result<void> Xi::Serialization::Yaml::OutputSerializer::writeNull(const Tag& nameTag) {
  XI_FAIL_IF(m_impl->stack.empty(), YamlError::NoValue);
  return m_impl->emit(YAML::Null, nameTag);
}

Result<void> Xi::Serialization::Yaml::OutputSerializer::writeNotNull(const Tag& nameTag) {
  XI_UNUSED(nameTag)
  XI_SUCCEED()
}

}  // namespace Yaml
}  // namespace Serialization
}  // namespace Xi
