/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Serialization/Console/ConsoleError.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Serialization::Console, Console)
XI_ERROR_CODE_DESC(NoValue, "value not found")
XI_ERROR_CODE_DESC(Internal, "internal software error")
XI_ERROR_CODE_DESC(NullTag, "null type tag")

XI_ERROR_CODE_DESC(TypeMissmatchScalar, "type missmatch, expected scalar")
XI_ERROR_CODE_DESC(TypeMissmatchBoolean, "type missmatch, expected boolean")
XI_ERROR_CODE_DESC(TypeMissmatchFloating, "type missmatch, expected floating")
XI_ERROR_CODE_DESC(TypeMissmatchContainer, "type missmatch, expected container")
XI_ERROR_CODE_DESC(TypeMissmatchObject, "type missmatch, expected object")
XI_ERROR_CODE_DESC(TypeMissmatchArray, "type missmatch, expected array")
XI_ERROR_CODE_CATEGORY_END()
