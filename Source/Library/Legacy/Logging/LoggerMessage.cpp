﻿// Copyright (c) 2012-2017, The CryptoNote developers, The Bytecoin developers
//
// This file is part of Bytecoin.
//
// Bytecoin is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bytecoin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Bytecoin.  If not, see <http://www.gnu.org/licenses/>.

#include "LoggerMessage.h"

namespace Logging {

LoggerMessage::LoggerMessage(ILogger& logger, const std::string& category, Level level, const std::string& color)
    : message(color),
      category(category),
      logLevel(level),
      logger(logger),
      timestamp(boost::posix_time::microsec_clock::local_time()),
      gotText{false},
      buffer() {
  buffer << color;
}

LoggerMessage::~LoggerMessage() {
  if (gotText) {
    buffer << std::endl;
    logger(category, logLevel, timestamp, buffer.str());
  }
}

LoggerMessage::LoggerMessage(LoggerMessage&& other)
    : message(other.message),
      category(other.category),
      logLevel(other.logLevel),
      logger(other.logger),
      timestamp(boost::posix_time::microsec_clock::local_time()),
      gotText{other.gotText},
      buffer(std::move(other.buffer)) {
  other.gotText = false;
}

}  // namespace Logging
