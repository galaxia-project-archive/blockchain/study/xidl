/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * Previous Work                                                                                  *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * - <https://github.com/bitcoin/bitcoin/tree/master/src/test/data>                               *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <iostream>
#include <string>

#include <Xi/Byte.hh>
#include <Xi/Testing/Result.hpp>
#include <Xi/String/ToUpper.hpp>
#include <Xi/Encoding/Base58.hh>
#include <Xi/Encoding/Base16.hh>

#define XI_TESTSUITE T_Xi_Encoding_Base58

TEST(XI_TESTSUITE, Oracle) {
  using namespace ::testing;
  using namespace Xi;
  using namespace Xi::Testing;
  using namespace Xi::Encoding;

  const std::vector<std::pair<std::string, std::string>> oracles{
      {{"", ""},
       {"61", "2g"},
       {"626262", "a3gV"},
       {"636363", "aPEr"},
       {"73696d706c792061206c6f6e6720737472696e67", "2cFupjhnEsSn59qHXstmK2ffpLv2"},
       {"00eb15231dfceb60925886b67d065299925915aeb172c06647", "1NS17iag9jJgTHD1VXjvLCEnZuQ3rJDE9L"},
       {"516b6fcd0f", "ABnLTmg"},
       {"bf4f89001e670274dd", "3SEo3LWLoPntC"},
       {"572e4794", "3EFU7m"},
       {"ecac89cad93923c02321", "EJDM8drfXA6uyA"},
       {"10c8511e", "Rt5zm"},
       {"00000000000000000000", "1111111111"},
       {"000111d38e5fc9071ffcd20b4a763cc9ae4f252bb4e48fd66a835e252ada93ff480d6dd43dc62a641155a5",
        "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"},
       {"000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f202122232425262728292a2b2c2d2e2f30313233343536"
        "3738393a3b3c3d3e3f404142434445464748494a4b4c4d4e4f505152535455565758595a5b5c5d5e5f606162636465666768696a6b6c6d"
        "6e6f707172737475767778797a7b7c7d7e7f808182838485868788898a8b8c8d8e8f909192939495969798999a9b9c9d9e9fa0a1a2a3a4"
        "a5a6a7a8a9aaabacadaeafb0b1b2b3b4b5b6b7b8b9babbbcbdbebfc0c1c2c3c4c5c6c7c8c9cacbcccdcecfd0d1d2d3d4d5d6d7d8d9dadb"
        "dcdddedfe0e1e2e3e4e5e6e7e8e9eaebecedeeeff0f1f2f3f4f5f6f7f8f9fafbfcfdfeff",
        "1cWB5HCBdLjAuqGGReWE3R3CguuwSjw6RHn39s2yuDRTS5NsBgNiFpWgAnEx6VQi8csexkgYw3mdYrMHr8x9i7aEwP8kZ7vccXWqKDvGv3u1Gx"
        "FKPuAkn8JCPPGDMf3vMMnbzm6Nh9zh1gcNsMvH3ZNLmP5fSG6DGbbi2tuwMWPthr4boWwCxf7ewSgNQeacyozhKDDQQ1qL5fQFUW52QKUZDZ5f"
        "w3KXNQJMcNTcaB723LchjeKun7MuGW5qyCBZYzA1KjofN1gYBV3NqyhQJ3Ns746GNuf9N2pQPmHz4xpnSrrfCvy6TVVz5d4PdrjeshsWQwpZsZ"
        "GzvbdAdN8MKV5QsBDY"}}};

  for (const auto& oracle : oracles) {
    auto data = Base16::decode(toUpper(oracle.first));
    ASSERT_TRUE(data.isValue());
    auto encoded = Base58::encode(*data);
    EXPECT_EQ(oracle.second, encoded);
    auto decoded = Base58::decode(oracle.second);
    ASSERT_TRUE(decoded.isValue());
    EXPECT_THAT(*data, ContainerEq(*decoded));
  }
}
