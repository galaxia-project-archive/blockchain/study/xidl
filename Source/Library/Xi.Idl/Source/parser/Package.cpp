/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Parser/Package.hpp"

#include "Xi/Idl/Parser/Keyword.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

Result<void> PackageAutomaton::doProcessToken(const Token& token) {
  if (token.value == "extends") {
    m_result.inheritance = Reference{};
    chain(makeReferenceAutomaton(*m_result.inheritance));
  } else {
    m_result.inheritance = std::nullopt;
  }

  chain(makeKeywordAutomaton("{"));
  chain(makeFieldsAutomaton(m_result.fields));
  chain(makeKeywordAutomaton("}"));
  markFinished();

  if (token.value == "extends") {
    XI_SUCCEED();
  } else {
    return processToken(token);
  }
}

std::shared_ptr<PackageAutomaton> makePackageAutomaton(Package& result) {
  std::shared_ptr<PackageAutomaton> reval{new PackageAutomaton{result}};
  reval->chain(makeKeywordAutomaton("package"));
  reval->chain(makeIdentifierAutomaton(result.name));
  return reval;
}

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi
