# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_LIBRARY_SERIALIZATION_SERIALIZATION)
  return()
endif()
set(CMAKE_XI_LIBRARY_SERIALIZATION_SERIALIZATION TRUE)

xi_include(Xi/Make/Library)
xi_include(Xi/Make/UnitTest)
xi_include(Source/Library/Xi/Core)

if(XI_BUILD_UNITTEST)
  xi_include(Source/Library/Xi/Serialization/Binary)
  xi_include(Source/Library/Xi/Serialization/Json)
  xi_include(Source/Library/Xi/Serialization/Yaml)
endif()

xi_make_library(
  Xi.Serialization

  PUBLIC_LIBRARIES
    Xi::Core

  TEST_LIBRARIES
    Xi::Serialization::Binary
    Xi::Serialization::Json
    Xi::Serialization::Yaml
)
