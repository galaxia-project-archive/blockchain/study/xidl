﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Http/ContentType.h"

#include <Xi/Exceptions.hpp>

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Http, ContentType)
XI_ERROR_CODE_DESC(Unknown, "unknown content type")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Http {

std::string stringify(Xi::Http::ContentType status) {
  switch (status) {
    case Http::ContentType::Html:
      return "text/html";
    case Http::ContentType::Plain:
      return "text/plain";
    case Http::ContentType::Xml:
      return "application/xml";
    case Http::ContentType::Json:
      return "application/json";
    case Http::ContentType::Text:
      return "application/text";
    case Http::ContentType::Yaml:
      return "application/x-yaml";
    case Http::ContentType::Binary:
      return "application/octet-stream";
    case Http::ContentType::MultipartFormData:
      return "multipart/form-data";
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

Result<void> parse(const std::string &value, ContentType &out) {
  if (value == toString(Http::ContentType::Html))
    out = Http::ContentType::Html;
  else if (value == toString(Http::ContentType::Plain))
    out = Http::ContentType::Plain;
  else if (value == toString(Http::ContentType::Xml))
    out = Http::ContentType::Xml;
  else if (value == toString(Http::ContentType::Json))
    out = Http::ContentType::Json;
  else if (value == toString(Http::ContentType::Text))
    out = Http::ContentType::Text;
  else if (value == toString(Http::ContentType::Binary))
    out = Http::ContentType::Binary;
  else if (value == toString(Http::ContentType::Yaml))
    out = Http::ContentType::Yaml;
  else if (value == toString(Http::ContentType::MultipartFormData))
    out = Http::ContentType::MultipartFormData;
  else
    XI_FAIL(ContentTypeError::Unknown)
  XI_SUCCEED()
}

}  // namespace Http
}  // namespace Xi
