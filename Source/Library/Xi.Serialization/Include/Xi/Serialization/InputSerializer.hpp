/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string_view>
#include <cinttypes>
#include <system_error>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>
#include <Xi/Byte.hh>

#include "Xi/Serialization/Format.hpp"
#include "Xi/Serialization/Tag.hpp"

namespace Xi {
namespace Serialization {
class InputSerializer {
 public:
  virtual ~InputSerializer() = default;

  [[nodiscard]] virtual Format format() const = 0;

  [[nodiscard]] virtual Result<void> readInt8(std::int8_t& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> readUInt8(std::uint8_t& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> readInt16(std::int16_t& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> readUInt16(std::uint16_t& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> readInt32(std::int32_t& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> readUInt32(std::uint32_t& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> readInt64(std::int64_t& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> readUInt64(std::uint64_t& value, const Tag& nameTag) = 0;

  [[nodiscard]] virtual Result<void> readBoolean(bool& value, const Tag& nameTag) = 0;

  [[nodiscard]] virtual Result<void> readFloat(float& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> readDouble(double& value, const Tag& nameTag) = 0;

  [[nodiscard]] virtual Result<void> readString(std::string& string, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> readBinary(ByteVector& blob, const Tag& nameTag) = 0;

  [[nodiscard]] virtual Result<void> readBlob(ByteSpan out, const Tag& nameTag) = 0;

  [[nodiscard]] virtual Result<void> readTag(Tag& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> readFlag(TagVector& value, const Tag& nameTag) = 0;

  [[nodiscard]] virtual Result<void> beginReadComplex(const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> endReadComplex() = 0;

  [[nodiscard]] virtual Result<void> beginReadVector(size_t& size, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> endReadVector() = 0;

  [[nodiscard]] virtual Result<void> beginReadArray(size_t size, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> endReadArray() = 0;

  [[nodiscard]] virtual Result<void> checkValue(bool& isNull, const Tag& nameTag) = 0;
};

XI_DECLARE_SMART_POINTER(InputSerializer)

}  // namespace Serialization
}  // namespace Xi
