﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <memory>
#include <string>

#include <Xi/Global.hh>

#include "Xi/CrashHandler/Config.hpp"

namespace Xi {
namespace CrashHandler {

/*!
 * \brief The CrashHandler class handles crashes of the application by creating crash dumps and uploading them
 *
 * In order to create crash dumps you need to create an instance of this class and store it as long as possible. This
 * class itself will register for termination calls and try to create/upload crash dumps just in time before the
 * application exits.
 */
class CrashHandler {
 public:
  /*!
   * \brief CrashHandler constructs a new crash handler
   * \param config The configuration specifying where to upload crash dumps and wheter to upload them
   */
  CrashHandler(const Config& config);
  XI_DELETE_COPY(CrashHandler);
  XI_DEFAULT_MOVE(CrashHandler);
  ~CrashHandler() = default;

 public:
  struct _Impl;

 private:
  std::shared_ptr<_Impl> m_impl;
};

}  // namespace CrashHandler
}  // namespace Xi
