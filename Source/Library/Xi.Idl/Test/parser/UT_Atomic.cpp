/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/Testing/Result.hpp>
#include <Xi/Idl/Parser/Atomic.hpp>

#define XI_TEST_SUITE Xi_Idl_Parser_Atomic

class Xi_Idl_Parser_Atomic : public ::testing::Test {
 public:
  Xi::Idl::Parser::Atomic atomic;
  std::shared_ptr<Xi::Idl::Parser::AtomicAutomaton> automaton;

  Xi_Idl_Parser_Atomic() : atomic{}, automaton{Xi::Idl::Parser::makeAtomicAutomaton(atomic)} {
    /* */
  }

  template <typename _TypeT>
  void expectType() {
    EXPECT_TRUE(std::holds_alternative<_TypeT>(atomic));
  }

  void process(const std::string& content) {
    auto ec = automaton->process(content);
    ASSERT_THAT(ec, Xi::Testing::IsSuccess());
  }
};

TEST_F(XI_TEST_SUITE, UInt8) {
  using namespace Xi::Idl::Parser;
  process("uint8");
  expectType<UInt8>();
}

TEST_F(XI_TEST_SUITE, Int8) {
  using namespace Xi::Idl::Parser;
  process("int8");
  expectType<Int8>();
}

TEST_F(XI_TEST_SUITE, UInt16) {
  using namespace Xi::Idl::Parser;
  process("uint16");
  expectType<UInt16>();
}

TEST_F(XI_TEST_SUITE, Int16) {
  using namespace Xi::Idl::Parser;
  process("int16");
  expectType<Int16>();
}

TEST_F(XI_TEST_SUITE, UInt32) {
  using namespace Xi::Idl::Parser;
  process("uint32");
  expectType<UInt32>();
}

TEST_F(XI_TEST_SUITE, Int32) {
  using namespace Xi::Idl::Parser;
  process("int32");
  expectType<Int32>();
}

TEST_F(XI_TEST_SUITE, UInt64) {
  using namespace Xi::Idl::Parser;
  process("uint64");
  expectType<UInt64>();
}

TEST_F(XI_TEST_SUITE, Int64) {
  using namespace Xi::Idl::Parser;
  process("int64");
  expectType<Int64>();
}

TEST_F(XI_TEST_SUITE, Float16) {
  using namespace Xi::Idl::Parser;
  process("float16");
  expectType<Float16>();
}

TEST_F(XI_TEST_SUITE, Float32) {
  using namespace Xi::Idl::Parser;
  process("float32");
  expectType<Float32>();
}

TEST_F(XI_TEST_SUITE, Float64) {
  using namespace Xi::Idl::Parser;
  process("float64");
  expectType<Float64>();
}

TEST_F(XI_TEST_SUITE, Boolean) {
  using namespace Xi::Idl::Parser;
  process("boolean");
  expectType<Boolean>();
}

TEST_F(XI_TEST_SUITE, String) {
  using namespace Xi::Idl::Parser;
  process("string");
  expectType<String>();
}

TEST_F(XI_TEST_SUITE, Reference) {
  using namespace Xi::Idl::Parser;
  using namespace ::testing;
  process("#xi.byte");
  expectType<Reference>();
  const auto& ref = std::get<Reference>(atomic);
  ASSERT_THAT(ref.nested, SizeIs(Eq(2)));
  EXPECT_THAT(ref.nested[0], Eq("xi"));
  EXPECT_THAT(ref.nested[1], Eq("byte"));
}
