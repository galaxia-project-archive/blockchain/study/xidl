/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/ErrorCode.hpp>

namespace Xi {
namespace Rpc {

XI_ERROR_CODE_BEGIN(Command)
XI_ERROR_CODE_VALUE(Null, 0x0001)

XI_ERROR_CODE_VALUE(BinaryNull, 0x0101)
XI_ERROR_CODE_VALUE(BinaryOverflow, 0x0102)

XI_ERROR_CODE_VALUE(TextNull, 0x0201)
XI_ERROR_CODE_VALUE(TextOverflow, 0x0202)

XI_ERROR_CODE_VALUE(Overflow, 0x0301)
XI_ERROR_CODE_END(Command, "Rpc::CommandError")

}  // namespace Rpc
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Rpc, Command)
