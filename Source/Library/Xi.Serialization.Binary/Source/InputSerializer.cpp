/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Serialization/Binary/InputSerializer.hpp"

#include <limits>

#include <Xi/Global.hh>
#include <Xi/Byte.hh>
#include <Xi/Encoding/VarInt.hh>
#include <Xi/Endianess/Endianess.hh>

#include "Xi/Serialization/Binary/BinaryError.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Serialization::Binary, InputSerializer)
XI_ERROR_CODE_DESC(TrailingBytes, "deserialization did not read all bytes")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Serialization {
namespace Binary {
namespace {
template <typename _IntT>
[[nodiscard]] Result<void> readVarInt(_IntT& value, Stream::InputStream& stream) {
  ByteVector bytes{};
  bytes.reserve(Encoding::VarInt::maximumEncodingSize<_IntT>());
  do {
    const auto isEos = stream.isEndOfStream();
    XI_ERROR_PROPAGATE(isEos)
    XI_FAIL_IF(*isEos, Encoding::VarInt::DecodeError::OutOfMemory)
    const auto next = stream.take();
    XI_ERROR_PROPAGATE(next)
    bytes.push_back(*next);
    XI_FAIL_IF(bytes.size() > Encoding::VarInt::maximumEncodingSize<_IntT>(), Encoding::VarInt::DecodeError::Overflow);
  } while (Encoding::VarInt::hasSuccessor(bytes.back()));

  _IntT reval = 0;
  XI_ERROR_PROPAGATE_CATCH(Encoding::VarInt::decode(bytes, reval));
  value = reval;
  XI_SUCCEED()
}

template <typename _IntT>
[[nodiscard]] Result<void> readInt(_IntT& value, Stream::InputStream& stream, const Options& options) {
  XI_ERROR_PROPAGATE_CATCH(stream.readStrict(asByteSpan(&value, sizeof(_IntT))));
  value = Endianess::convert(value, options.endianess);
  XI_SUCCEED()
}
}  // namespace

InputSerializer::InputSerializer(Stream::InputStream& stream, Options options) : m_stream{stream}, m_options{options} {
  /* */
}

Format InputSerializer::format() const {
  return Format::Binary;
}

Result<void> InputSerializer::readInt8(std::int8_t& value, const Tag& nameTag) {
  XI_UNUSED(nameTag)
  const auto _ = m_stream.take();
  XI_ERROR_PROPAGATE(_)
  value = static_cast<int8_t>(*_);
  XI_SUCCEED()
}

Result<void> InputSerializer::readUInt8(std::uint8_t& value, const Tag& nameTag) {
  XI_UNUSED(nameTag)
  const auto _ = m_stream.take();
  XI_ERROR_PROPAGATE(_)
  value = static_cast<uint8_t>(*_);
  XI_SUCCEED()
}

Result<void> InputSerializer::readInt16(std::int16_t& value, const Tag& nameTag) {
  XI_UNUSED(nameTag);
  return hasFlag(m_options.varInt, VarIntUsage::Int16) ? readVarInt(value, m_stream)
                                                       : readInt(value, m_stream, m_options);
}

Result<void> InputSerializer::readUInt16(std::uint16_t& value, const Tag& nameTag) {
  XI_UNUSED(nameTag);
  return hasFlag(m_options.varInt, VarIntUsage::UInt16) ? readVarInt(value, m_stream)
                                                        : readInt(value, m_stream, m_options);
}

Result<void> InputSerializer::readInt32(std::int32_t& value, const Tag& nameTag) {
  XI_UNUSED(nameTag);
  return hasFlag(m_options.varInt, VarIntUsage::Int32) ? readVarInt(value, m_stream)
                                                       : readInt(value, m_stream, m_options);
}

Result<void> InputSerializer::readUInt32(std::uint32_t& value, const Tag& nameTag) {
  XI_UNUSED(nameTag);
  return hasFlag(m_options.varInt, VarIntUsage::UInt32) ? readVarInt(value, m_stream)
                                                        : readInt(value, m_stream, m_options);
}

Result<void> InputSerializer::readInt64(std::int64_t& value, const Tag& nameTag) {
  XI_UNUSED(nameTag);
  return hasFlag(m_options.varInt, VarIntUsage::Int64) ? readVarInt(value, m_stream)
                                                       : readInt(value, m_stream, m_options);
}

Result<void> InputSerializer::readUInt64(std::uint64_t& value, const Tag& nameTag) {
  XI_UNUSED(nameTag);
  return hasFlag(m_options.varInt, VarIntUsage::UInt64) ? readVarInt(value, m_stream)
                                                        : readInt(value, m_stream, m_options);
}

Result<void> InputSerializer::readBoolean(bool& value, const Tag& nameTag) {
  std::uint8_t boolValue = 0;
  XI_ERROR_PROPAGATE_CATCH(this->readUInt8(boolValue, nameTag));
  if (boolValue == 0b01010101) {
    value = true;
    XI_SUCCEED()
  } else if (boolValue == 0b00101010) {
    value = false;
    XI_SUCCEED()
  } else {
    XI_FAIL(BinaryError::InvalidBooleanRepresentation)
  }
}

Result<void> InputSerializer::readFloat(float& value, const Tag& nameTag) {
  static_assert(std::numeric_limits<float>::is_iec559,
                "binary floating serialization is not supported on this architecture");
  union {
    float asFloating;
    Byte asBlob[sizeof(float)];
  } data;
  XI_ERROR_PROPAGATE_CATCH(this->readBlob(data.asBlob, nameTag));
  value = data.asFloating;
  XI_SUCCEED()
}

Result<void> InputSerializer::readDouble(double& value, const Tag& nameTag) {
  static_assert(std::numeric_limits<double>::is_iec559,
                "binary floating serialization is not supported on this architecture");
  union {
    double asFloating;
    Byte asBlob[sizeof(double)];
  } data;
  XI_ERROR_PROPAGATE_CATCH(this->readBlob(data.asBlob, nameTag));
  value = data.asFloating;
  XI_SUCCEED()
}

Result<void> InputSerializer::readTag(Tag& value, const Tag& nameTag) {
  Tag::binary_type native = Tag::Null.binary();
  XI_ERROR_PROPAGATE_CATCH(readUInt64(native, nameTag));
  XI_FAIL_IF(native == Tag::Null.binary(), BinaryError::NullTag);
  value = Tag{native, Tag::NoTextTag};
  XI_SUCCEED()
}

Result<void> InputSerializer::readFlag(Serialization::TagVector& value, const Tag& nameTag) {
  uint16_t nativeFlag = 0;
  XI_ERROR_PROPAGATE_CATCH(readUInt16(nativeFlag, nameTag));
  XI_FAIL_IF(nativeFlag > (1 << 14), BinaryError::FlagOverflow);
  value.clear();
  for (size_t i = 0; (1 << i) <= nativeFlag; ++i) {
    if ((nativeFlag & (1 << i))) {
      value.emplace_back(i + 1, Tag::NoTextTag);
    }
  }
  XI_SUCCEED()
}

Result<void> InputSerializer::readString(std::string& value, const Tag& nameTag) {
  uint64_t size = 0;
  XI_ERROR_PROPAGATE_CATCH(readUInt64(size, nameTag));
  value.resize(size);
  XI_ERROR_PROPAGATE_CATCH(readBlob(asByteSpan(value), nameTag));
  XI_SUCCEED()
}

Result<void> InputSerializer::readBinary(ByteVector& out, const Tag& nameTag) {
  uint64_t size = 0;
  XI_ERROR_PROPAGATE_CATCH(readUInt64(size, nameTag));
  out.resize(size);
  return readBlob(out, nameTag);
}

Result<void> InputSerializer::readBlob(ByteSpan out, const Tag& nameTag) {
  XI_UNUSED(nameTag);
  return m_stream.readStrict(out);
}

Result<void> InputSerializer::beginReadComplex(const Tag& nameTag) {
  XI_UNUSED(nameTag);
  XI_SUCCEED();
}

Result<void> InputSerializer::endReadComplex() {
  XI_SUCCEED();
}

Result<void> InputSerializer::beginReadVector(size_t& size, const Tag& nameTag) {
  XI_ERROR_PROPAGATE_CATCH(readUInt64(size, nameTag));
  XI_SUCCEED()
}

Result<void> InputSerializer::endReadVector() {
  XI_SUCCEED();
}

Result<void> InputSerializer::beginReadArray(size_t size, const Tag& nameTag) {
  XI_UNUSED(size, nameTag);
  XI_SUCCEED();
}

Result<void> InputSerializer::endReadArray() {
  XI_SUCCEED();
}

Result<void> InputSerializer::checkValue(bool& value, const Tag& nameTag) {
  return readBoolean(value, nameTag);
}

}  // namespace Binary
}  // namespace Serialization
}  // namespace Xi
