/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <iostream>

#include <Xi/String/String.hpp>
#include <Xi/Encoding/Base16.hh>
#include <Xi/Wallet/Address.hpp>

#define XI_TEST_SUITE Xi_Wallet_Address

TEST(XI_TEST_SUITE, NoExtension) {
  using namespace Xi;
  using namespace Xi::Wallet;
  using namespace testing;

  std::cout << toString(fromString<AddressPrefix>("GLX1")) << std::endl;
  std::cout << Encoding::Base16::encode(fromString<AddressPrefix>("GLX1")->binary()) << std::endl;
}
