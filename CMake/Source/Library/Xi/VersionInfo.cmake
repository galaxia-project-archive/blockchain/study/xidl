# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_SOURCE_LIBRARY_XI_VERSIONINFO)
  return()
endif()
set(CMAKE_XI_SOURCE_LIBRARY_XI_VERSIONINFO TRUE)

set(
  XI_RELEASE_CHANNEL "clutter"
  CACHE STRING "Release channel this instance is built for [clutter|edge|beta|release]"
)

string(TOUPPER "${XI_RELEASE_CHANNEL}" XI_RELEASE_CHANNEL_UPPER)

xi_include(Xi/Make/Library)
xi_include(Xi/Version)
xi_include(Source/Library/Xi/Core)
xi_include(Source/Library/Xi/Serialization)

xi_make_library(
  Xi.VersionInfo

  PUBLIC_LIBRARIES
    Xi::Core
    Xi::Serialization

    Version::Project
)
