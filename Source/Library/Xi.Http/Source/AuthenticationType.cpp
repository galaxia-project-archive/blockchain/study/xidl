﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Http/AuthenticationType.h"

#include <Xi/Exceptions.hpp>

namespace Xi {
namespace Http {

std::string stringify(Xi::Http::AuthenticationType status) {
  switch (status) {
    case (Xi::Http::AuthenticationType::Basic):
      return "Basic";
    case (Xi::Http::AuthenticationType::Bearer):
      return "Bearer";
    case (Xi::Http::AuthenticationType::Unsupported):
      return "Unsupported";
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

Result<void> parse(const std::string& value, AuthenticationType& out) {
  if (value == toString(Xi::Http::AuthenticationType::Basic))
    out = Xi::Http::AuthenticationType::Basic;
  if (value == toString(Xi::Http::AuthenticationType::Bearer))
    out = Xi::Http::AuthenticationType::Bearer;
  else
    out = Xi::Http::AuthenticationType::Unsupported;
  XI_SUCCEED()
}

}  // namespace Http
}  // namespace Xi
