/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/Testing/Result.hpp>
#include <Xi/Idl/Parser/Reference.hpp>

#define XI_TEST_SUITE Xi_Idl_Parser_Reference

TEST(XI_TEST_SUITE, WellFormed) {
  using namespace Xi::Idl::Parser;
  using namespace Xi::Testing;
  using namespace ::testing;

  Reference ref{};
  {
    auto refa = makeReferenceAutomaton(ref);
    auto ec = refa->process("#byte");

    ASSERT_THAT(ec, IsSuccess());
    ASSERT_THAT(ref.nested, SizeIs(Eq(1)));
    EXPECT_THAT(ref.nested[0], Eq("byte"));
    ref.nested.clear();
  }

  {
    auto refa = makeReferenceAutomaton(ref);
    auto ec = refa->process("#io.xiproject.byte");

    ASSERT_THAT(ec, IsSuccess());
    ASSERT_THAT(ref.nested, SizeIs(Eq(3)));
    EXPECT_THAT(ref.nested[0], Eq("io"));
    EXPECT_THAT(ref.nested[1], Eq("xiproject"));
    EXPECT_THAT(ref.nested[2], Eq("byte"));
    ref.nested.clear();
  }
}

TEST(XI_TEST_SUITE, IllFormed) {
  using namespace Xi::Idl::Parser;
  using namespace Xi::Testing;
  using namespace ::testing;

  {
    Reference ref{};
    auto refa = makeReferenceAutomaton(ref);
    ASSERT_THAT(refa->process("byte"), IsFailure());
  }

  {
    Reference ref{};
    auto refa = makeReferenceAutomaton(ref);
    ASSERT_THAT(refa->process("#io.xiproject.?byte"), IsFailure());
  }
}
