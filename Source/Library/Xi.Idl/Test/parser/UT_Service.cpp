/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/Testing/Result.hpp>
#include <Xi/Idl/Parser/Service.hpp>

#define XI_TEST_SUITE Xi_Idl_Parser_Service

class XI_TEST_SUITE : public ::testing::Test {
 public:
  Xi::Idl::Parser::ServiceVector services;
  std::shared_ptr<Xi::Idl::Parser::ServicesAutomaton> automaton;

  XI_TEST_SUITE() : services{}, automaton{nullptr} {
    /* */
  }

  void process(const std::string& content) {
    services = Xi::Idl::Parser::ServiceVector{/* */};
    automaton = Xi::Idl::Parser::makeServicesAutomaton(services);
    auto ec = automaton->process(content);
    ASSERT_THAT(ec, Xi::Testing::IsSuccess());
  }
};

TEST_F(XI_TEST_SUITE, WellFormed) {
  using namespace Xi::Idl::Parser;
  using namespace ::testing;

  process(R"__(
service empty {}
service miner {
  serves #Miner.SetThreads as (1;set_threads);
  serves #Pause as (2;pause);
}
)__");

  ASSERT_THAT(services, SizeIs(Eq(2)));
  {
    const auto& service = services.at(0);
    EXPECT_THAT(service.name, Eq("empty"));
    EXPECT_THAT(service.commands, IsEmpty());
  }

  {
    const auto& service = services.at(1);
    EXPECT_THAT(service.name, Eq("miner"));
    EXPECT_THAT(service.commands, SizeIs(Eq(2)));
    {
      const auto& command = service.commands.at(0);
      EXPECT_THAT(command.tag.binary, Eq(1));
      EXPECT_THAT(command.tag.text, Eq("set_threads"));
      ASSERT_THAT(command.contract.nested, SizeIs(Eq(2)));
      EXPECT_THAT(command.contract.nested[0], Eq("Miner"));
      EXPECT_THAT(command.contract.nested[1], Eq("SetThreads"));
    }
    {
      const auto& command = service.commands.at(1);
      EXPECT_THAT(command.tag.binary, Eq(2));
      EXPECT_THAT(command.tag.text, Eq("pause"));
      ASSERT_THAT(command.contract.nested, SizeIs(Eq(1)));
      EXPECT_THAT(command.contract.nested[0], Eq("Pause"));
    }
  }
}
