/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Wallet/AddressPrefix.hpp"

#include <Xi/Exceptions.hpp>
#include <Xi/Serialization/Array.hpp>
#include <Xi/Encoding/VarInt.hh>
#include <Xi/Encoding/Base58.hh>

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Wallet, AddressPrefix)
XI_ERROR_CODE_DESC(Empty, "prefix is empty")
XI_ERROR_CODE_DESC(TooLong, "prefix is too long")
XI_ERROR_CODE_DESC(InvalidTermination, "invalid use of termination symbol 1")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Wallet {

Result<AddressPrefix> AddressPrefix::parse(const std::string &str) {
  auto bin = Encoding::Base58::decode(str);
  XI_ERROR_PROPAGATE(bin)
  for (size_t i = 0; i < bin->size(); ++i) {
    XI_FAIL_IF((*bin)[i] == 0, AddressPrefixError::InvalidTermination)
  }
  bin->push_back(0);
  XI_SUCCEED(AddressPrefix{bin.take()})
}

AddressPrefix::AddressPrefix() : m_binary{/* */}, m_text{"1"} {
  /* */
}

AddressPrefix::AddressPrefix(AddressPrefix::Binary binary) : m_binary{binary} {
  m_text = Encoding::Base58::encode(m_binary) + "1";
}

const AddressPrefix::Binary &AddressPrefix::binary() const {
  return m_binary;
}

const AddressPrefix::Text &AddressPrefix::text() const {
  return m_text;
}

bool AddressPrefix::isValid() const {
  return m_binary.size() > 0;
}

std::string AddressPrefix::stringify() const {
  return m_text;
}

Result<void> serialize(AddressPrefix &value, const Serialization::Tag &name, Serialization::Serializer &serializer) {
  if (serializer.isHumanReadableFromat()) {
    AddressPrefix::Text text{};
    if (serializer.isOutputMode()) {
      text = value.stringify();
    }
    XI_ERROR_PROPAGATE_CATCH(serializer(text, name));
    if (serializer.isInputMode()) {
      auto parsed = AddressPrefix::parse(text);
      XI_ERROR_PROPAGATE(parsed);
      value = parsed.take();
    }
    XI_SUCCEED();
  } else if (serializer.isBinaryFormat()) {
    AddressPrefix::Binary bin;
    if (serializer.isOutputMode()) {
      bin = value.binary();
    }
    XI_ERROR_PROPAGATE_CATCH(serializer(bin, name));
    if (serializer.isInputMode()) {
      value = AddressPrefix{bin};
    }
    XI_SUCCEED();
  } else {
    XI_EXCEPTIONAL(InvalidVariantTypeError)
  }
}

}  // namespace Wallet
}  // namespace Xi
