/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>
#include <Xi/Byte.hh>
#include <Xi/Ownership.hpp>

#include <ostream>

#include "Xi/Stream/OutputStream.hpp"

namespace Xi {
namespace Stream {
class OStream final : public OutputStream {
 public:
  explicit OStream(Ownership<std::ostream> dest);
  XI_DELETE_COPY(OStream);
  XI_DEFAULT_MOVE(OStream);
  ~OStream() override;

  [[nodiscard]] Result<size_t> write(ConstByteSpan buffer) override;
  [[nodiscard]] Result<void> flush() override;

 private:
  Ownership<std::ostream> m_dest;
};

XI_DECLARE_SMART_POINTER(OStream)
}  // namespace Stream
}  // namespace Xi
