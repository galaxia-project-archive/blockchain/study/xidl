/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Serialization/Json/InputSerializer.hpp"

#include <numeric>
#include <utility>
#include <stack>
#include <unordered_set>

#include <Xi/Extern/Push.hh>
#include <rapidjson/document.h>
#include <Xi/Extern/Pop.hh>

#include <Xi/Exceptions.hpp>
#include <Xi/Encoding/Base64.hh>
#include <Xi/Stream/InMemoryStreams.hpp>

#include "Xi/Serialization/Json/JsonError.hpp"

namespace Xi {
namespace Serialization {
namespace Json {

namespace {
struct JInputStreamWrapper {
  using Ch = char;

  Stream::InputStream &stream;

  JInputStreamWrapper(Stream::InputStream &_stream) : stream{_stream} {
    /* */
  }

  Ch Peek() const {
    if (stream.isEndOfStream().valueOrThrow()) {
      return '\0';
    } else {
      const auto byte = stream.peek().valueOrThrow();
      return static_cast<char>(byte);
    }
  }

  Ch Take() {
    if (stream.isEndOfStream().valueOrThrow()) {
      return '\0';
    } else {
      const auto byte = stream.take().valueOrThrow();
      return static_cast<char>(byte);
    }
  }

  size_t Tell() {
    return stream.tell().valueOrThrow();
  }

  Ch *PutBegin() {
    exceptional<NotImplementedError>();
  }
  [[noreturn]] void Put(Ch) {
    exceptional<NotImplementedError>();
  }
  [[noreturn]] void Flush() {
    exceptional<NotImplementedError>();
  }
  [[noreturn]] size_t PutEnd(Ch *) {
    exceptional<NotImplementedError>();
  }
};
}  // namespace

struct InputSerializer::_Impl {
  rapidjson::Document document;
  /// (value x arrayIndex))
  std::stack<std::pair<rapidjson::Value, size_t>> valueStack;

  _Impl(Stream::InputStream &stream) {
    JInputStreamWrapper is{stream};
    document.ParseStream(is);
  }

  [[nodiscard]] Result<void> readGenericMember(rapidjson::Value &out, const Tag &nameTag) {
    XI_FAIL_IF(valueStack.empty(), JsonError::NoValue);
    auto &top = valueStack.top();
    if (nameTag.isNull()) {
      XI_FAIL_IF_NOT(top.first.IsArray(), JsonError::TypeMissmatchArray);
      const auto index = top.second++;
      XI_FAIL_IF_NOT(index < top.first.Size(), JsonError::IndexOutOfRange);
      out = top.first[static_cast<rapidjson::SizeType>(index)];
      XI_SUCCEED()
    } else {
      XI_FAIL_IF_NOT(top.first.IsObject(), JsonError::TypeMissmatchObject);
      auto search = top.first.FindMember(rapidjson::Value{
          nameTag.text().data(), static_cast<rapidjson::SizeType>(nameTag.text().size()), document.GetAllocator()});
      XI_FAIL_IF(search == top.first.MemberEnd(), JsonError::NoValue);
      out = search->value;
      XI_SUCCEED()
    }
  }
};

InputSerializer::InputSerializer(Stream::InputStream &stream) : m_impl{new _Impl{stream}} {
  /* */
}

InputSerializer::~InputSerializer() {
  /* */
}

Format InputSerializer::format() const {
  return Format::HumanReadable;
}

Result<void> InputSerializer::readInt8(std::int8_t &value, const Tag &nameTag) {
  rapidjson::Value token;
  XI_ERROR_PROPAGATE_CATCH(m_impl->readGenericMember(token, nameTag))
  XI_FAIL_IF_NOT(token.IsInt(), JsonError::TypeMissmatchInteger);
  auto reval = token.GetInt();
  XI_FAIL_IF(reval > std::numeric_limits<int8_t>::max(), JsonError::IntegerOutOfBounds);
  XI_FAIL_IF(reval < std::numeric_limits<int8_t>::min(), JsonError::IntegerOutOfBounds);
  value = static_cast<int8_t>(reval);
  XI_SUCCEED()
}

Result<void> InputSerializer::readUInt8(std::uint8_t &value, const Tag &nameTag) {
  rapidjson::Value token;
  XI_ERROR_PROPAGATE_CATCH(m_impl->readGenericMember(token, nameTag))
  XI_FAIL_IF_NOT(token.IsUint(), JsonError::TypeMissmatchUnsignedInteger);
  auto reval = token.GetUint();
  XI_FAIL_IF(reval > std::numeric_limits<uint8_t>::max(), JsonError::IntegerOutOfBounds);
  value = static_cast<uint8_t>(reval);
  XI_SUCCEED()
}

Result<void> InputSerializer::readInt16(std::int16_t &value, const Tag &nameTag) {
  rapidjson::Value token;
  XI_ERROR_PROPAGATE_CATCH(m_impl->readGenericMember(token, nameTag))
  XI_FAIL_IF_NOT(token.IsInt(), JsonError::TypeMissmatchInteger);
  auto reval = token.GetInt();
  XI_FAIL_IF(reval > std::numeric_limits<int16_t>::max(), JsonError::IntegerOutOfBounds);
  XI_FAIL_IF(reval < std::numeric_limits<int16_t>::min(), JsonError::IntegerOutOfBounds);
  value = static_cast<int16_t>(reval);
  XI_SUCCEED()
}

Result<void> InputSerializer::readUInt16(std::uint16_t &value, const Tag &nameTag) {
  rapidjson::Value token;
  XI_ERROR_PROPAGATE_CATCH(m_impl->readGenericMember(token, nameTag))
  XI_FAIL_IF_NOT(token.IsUint(), JsonError::TypeMissmatchUnsignedInteger);
  auto reval = token.GetUint();
  XI_FAIL_IF(reval > std::numeric_limits<uint16_t>::max(), JsonError::IntegerOutOfBounds);
  value = static_cast<uint16_t>(reval);
  XI_SUCCEED()
}

Result<void> InputSerializer::readInt32(std::int32_t &value, const Tag &nameTag) {
  rapidjson::Value token;
  XI_ERROR_PROPAGATE_CATCH(m_impl->readGenericMember(token, nameTag))
  XI_FAIL_IF_NOT(token.IsInt(), JsonError::TypeMissmatchInteger);
  value = token.GetInt();
  XI_SUCCEED()
}

Result<void> InputSerializer::readUInt32(std::uint32_t &value, const Tag &nameTag) {
  rapidjson::Value token;
  XI_ERROR_PROPAGATE_CATCH(m_impl->readGenericMember(token, nameTag))
  XI_FAIL_IF_NOT(token.IsUint(), JsonError::TypeMissmatchUnsignedInteger);
  value = token.GetUint();
  XI_SUCCEED()
}

Result<void> InputSerializer::readInt64(std::int64_t &value, const Tag &nameTag) {
  rapidjson::Value token;
  XI_ERROR_PROPAGATE_CATCH(m_impl->readGenericMember(token, nameTag))
  XI_FAIL_IF_NOT(token.IsInt64(), JsonError::TypeMissmatchLongInteger);
  value = token.GetInt64();
  XI_SUCCEED()
}

Result<void> InputSerializer::readUInt64(std::uint64_t &value, const Tag &nameTag) {
  rapidjson::Value token;
  XI_ERROR_PROPAGATE_CATCH(m_impl->readGenericMember(token, nameTag))
  XI_FAIL_IF_NOT(token.IsUint64(), JsonError::TypeMissmatchUnsignedLongInteger);
  value = token.GetUint64();
  XI_SUCCEED()
}

Result<void> InputSerializer::readBoolean(bool &value, const Tag &nameTag) {
  rapidjson::Value token;
  XI_ERROR_PROPAGATE_CATCH(m_impl->readGenericMember(token, nameTag))
  XI_FAIL_IF_NOT(token.IsBool(), JsonError::TypeMissmatchBoolean);
  value = token.GetBool();
  XI_SUCCEED()
}

Result<void> InputSerializer::readFloat(float &value, const Tag &nameTag) {
  rapidjson::Value token;
  XI_ERROR_PROPAGATE_CATCH(m_impl->readGenericMember(token, nameTag))
  XI_FAIL_IF_NOT(token.IsFloat(), JsonError::TypeMissmatchNumber);
  value = token.GetFloat();
  XI_SUCCEED()
}

Result<void> InputSerializer::readDouble(double &value, const Tag &nameTag) {
  rapidjson::Value token;
  XI_ERROR_PROPAGATE_CATCH(m_impl->readGenericMember(token, nameTag))
  XI_FAIL_IF_NOT(token.IsDouble(), JsonError::TypeMissmatchLongNumber);
  value = token.GetDouble();
  XI_SUCCEED()
}

Result<void> InputSerializer::readTag(Tag &tag, const Tag &nameTag) {
  std::string stringTag = Tag::NoTextTag;
  XI_ERROR_PROPAGATE_CATCH(readString(stringTag, nameTag));
  XI_FAIL_IF(stringTag == Tag::NoTextTag, JsonError::NullTag);
  tag = Tag{Tag::NoBinaryTag, std::move(stringTag)};
  XI_SUCCEED()
}

Result<void> InputSerializer::readFlag(Serialization::TagVector &value, const Tag &nameTag) {
  bool hasFlag = true;
  XI_ERROR_PROPAGATE_CATCH(checkValue(hasFlag, nameTag));
  XI_SUCCEED_IF_NOT(hasFlag);

  size_t size = 0;
  XI_ERROR_PROPAGATE_CATCH(beginReadVector(size, nameTag));
  XI_FAIL_IF(size > Tag::maximumFlags(), JsonError::FlagOverflow);

  std::unordered_set<std::string> processedFlags{};
  for (size_t i = 0; i < size; ++i) {
    Tag iTag = Tag::Null;
    XI_ERROR_PROPAGATE_CATCH(readTag(iTag, Tag::Null));
    XI_FAIL_IF(iTag.text() == Tag::NoTextTag, JsonError::NullTag);
    XI_FAIL_IF_NOT(processedFlags.insert(iTag.text()).second, JsonError::DuplicateTag);
    value.push_back(iTag);
  }
  return endReadVector();
}

Result<void> InputSerializer::readString(std::string &out, const Tag &nameTag) {
  rapidjson::Value token;
  XI_ERROR_PROPAGATE_CATCH(m_impl->readGenericMember(token, nameTag))
  XI_FAIL_IF_NOT(token.IsString(), JsonError::TypeMissmatchString);
  out = std::string{token.GetString(), token.GetStringLength()};
  XI_SUCCEED()
}

Result<void> InputSerializer::readBinary(ByteVector &out, const Tag &nameTag) {
  rapidjson::Value token;
  XI_ERROR_PROPAGATE_CATCH(m_impl->readGenericMember(token, nameTag))
  XI_FAIL_IF_NOT(token.IsString(), JsonError::TypeMissmatchString);
  auto decoded = Encoding::Base64::decode(std::string_view{token.GetString(), token.GetStringLength()});
  XI_ERROR_PROPAGATE(decoded)
  out = decoded.take();
  XI_SUCCEED()
}

Result<void> InputSerializer::readBlob(ByteSpan out, const Tag &nameTag) {
  rapidjson::Value token;
  XI_ERROR_PROPAGATE_CATCH(m_impl->readGenericMember(token, nameTag))
  XI_FAIL_IF_NOT(token.IsString(), JsonError::TypeMissmatchString);
  auto ec = Encoding::Base64::decodeStrict(std::string_view{token.GetString(), token.GetStringLength()}, out);
  XI_ERROR_PROPAGATE(ec)
  XI_SUCCEED()
}

Result<void> InputSerializer::beginReadComplex(const Tag &nameTag) {
  if (m_impl->valueStack.empty()) {
    XI_FAIL_IF_NOT(m_impl->document.IsObject(), JsonError::TypeMissmatchObject);
    m_impl->valueStack.emplace(m_impl->document.GetObject(), 0);
    XI_SUCCEED()
  } else {
    rapidjson::Value token;
    XI_ERROR_PROPAGATE_CATCH(m_impl->readGenericMember(token, nameTag))
    XI_FAIL_IF_NOT(token.IsObject(), JsonError::TypeMissmatchObject);
    m_impl->valueStack.emplace(std::move(token), 0);
    XI_SUCCEED()
  }
}

Result<void> InputSerializer::endReadComplex() {
  XI_FAIL_IF(m_impl->valueStack.empty(), JsonError::NoValue);
  XI_FAIL_IF_NOT(m_impl->valueStack.top().first.IsObject(), JsonError::TypeMissmatchObject);
  m_impl->valueStack.pop();
  XI_SUCCEED()
}

Result<void> InputSerializer::beginReadVector(size_t &size, const Tag &nameTag) {
  if (m_impl->valueStack.empty()) {
    XI_FAIL_IF_NOT(m_impl->document.IsArray(), JsonError::TypeMissmatchArray);
    m_impl->valueStack.emplace(m_impl->document.GetArray(), 0);
    size = m_impl->valueStack.top().first.Size();
    XI_SUCCEED()
  } else {
    rapidjson::Value token;
    XI_ERROR_PROPAGATE_CATCH(m_impl->readGenericMember(token, nameTag))
    XI_FAIL_IF_NOT(token.IsArray(), JsonError::TypeMissmatchArray);
    size = token.Size();
    m_impl->valueStack.emplace(std::move(token), 0);
    XI_SUCCEED()
  }
}

Result<void> InputSerializer::endReadVector() {
  XI_FAIL_IF(m_impl->valueStack.empty(), JsonError::NoValue);
  XI_FAIL_IF_NOT(m_impl->valueStack.top().first.IsArray(), JsonError::TypeMissmatchArray);
  m_impl->valueStack.pop();
  XI_SUCCEED()
}

Result<void> InputSerializer::beginReadArray(size_t size, const Tag &nameTag) {
  size_t actualSize = ~size;
  XI_ERROR_PROPAGATE_CATCH(beginReadVector(actualSize, nameTag));
  XI_FAIL_IF_NOT(actualSize == size, JsonError::SizeMissmatch);
  XI_SUCCEED()
}

Result<void> InputSerializer::endReadArray() {
  XI_FAIL_IF(m_impl->valueStack.empty(), JsonError::NoValue);
  XI_FAIL_IF_NOT(m_impl->valueStack.top().first.IsArray(), JsonError::TypeMissmatchArray);
  m_impl->valueStack.pop();
  XI_SUCCEED()
}

Result<void> InputSerializer::checkValue(bool &value, const Tag &nameTag) {
  XI_FAIL_IF(m_impl->valueStack.empty(), JsonError::NoValue);
  auto &top = m_impl->valueStack.top();
  if (nameTag.isNull()) {
    XI_FAIL_IF_NOT(top.first.IsArray(), JsonError::TypeMissmatchArray);
    const auto index = top.second++;
    if (index >= top.first.Size()) {
      value = false;
      XI_SUCCEED()
    } else {
      value = !top.first[static_cast<rapidjson::SizeType>(index)].IsNull();
      XI_SUCCEED()
    }
  } else {
    XI_FAIL_IF_NOT(top.first.IsObject(), JsonError::TypeMissmatchObject);
    auto search = top.first.FindMember(rapidjson::Value{nameTag.text().data(),
                                                        static_cast<rapidjson::SizeType>(nameTag.text().size()),
                                                        m_impl->document.GetAllocator()});
    if (search == top.first.MemberEnd()) {
      value = false;
      XI_SUCCEED()
    } else {
      value = !search->value.IsNull();
      XI_SUCCEED()
    }
  }
}

}  // namespace Json
}  // namespace Serialization
}  // namespace Xi
