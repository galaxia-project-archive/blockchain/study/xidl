/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/FileSystem/SegmentGenerator.hpp"

namespace Xi {
namespace FileSystem {

SegmentGenerator::iterator SegmentGenerator::begin() const {
  return m_begin;
}

SegmentGenerator::iterator SegmentGenerator::end() const {
  return m_end;
}

SegmentGenerator::iterator SegmentGenerator::cbegin() const {
  return m_begin;
}

SegmentGenerator::iterator SegmentGenerator::cend() const {
  return m_end;
}

SegmentGenerator::SegmentGenerator(SegmentGenerator::iterator begin, SegmentGenerator::iterator end)
    : m_begin{begin}, m_end{end} {
  /* */
}

}  // namespace FileSystem
}  // namespace Xi
