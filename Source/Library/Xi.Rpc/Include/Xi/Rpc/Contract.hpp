/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <memory>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>

#include "Xi/Rpc/Command.hpp"
#include "Xi/Rpc/ContractPayload.hpp"
#include "Xi/Rpc/Request.hpp"
#include "Xi/Rpc/Response.hpp"

namespace Xi {
namespace Rpc {

XI_DECLARE_SMART_POINTER_CLASS(Contract)

class Contract : public std::enable_shared_from_this<Contract> {
 public:
  XI_DELETE_COPY(Contract);
  XI_DELETE_MOVE(Contract);
  virtual ~Contract() = default;

 public:
  ContractPayload createRequestPayload() const;
  ContractPayload createResponsePayload() const;
  Result<void> invoke(const Request& request, Response& response);

 protected:
  virtual ContractPayload doCreateRequestPayload() const = 0;
  virtual ContractPayload doCreateResponsePayload() const = 0;
  virtual Result<void> doInvoke(const Request& request, Response& response) = 0;

 protected:
  Contract() = default;
};

}  // namespace Rpc
}  // namespace Xi
