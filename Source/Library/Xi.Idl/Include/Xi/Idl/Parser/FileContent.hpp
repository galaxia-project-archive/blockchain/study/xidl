/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <vector>

#include "Xi/Idl/Parser/Namespace.hpp"
#include "Xi/Idl/Parser/Import.hpp"
#include "Xi/Idl/Parser/TypeDeclaration.hpp"
#include "Xi/Idl/Parser/Contract.hpp"
#include "Xi/Idl/Parser/Service.hpp"
#include "Xi/Idl/Parser/Documentation.hpp"
#include "Xi/Idl/Parser/Automaton.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

struct FileContent {
  Namespace namespace_;
  std::vector<Import> imports;
  std::vector<TypeDeclaration> typeDeclarations;
  std::vector<Contract> contracts;
  std::vector<Service> services;
};

class FileContentAutomaton final : public ResultAutomaton<FileContent> {
 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<FileContentAutomaton> makeFileContentAutomaton(FileContent&);
};

std::shared_ptr<FileContentAutomaton> makeFileContentAutomaton(FileContent& result);

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi
