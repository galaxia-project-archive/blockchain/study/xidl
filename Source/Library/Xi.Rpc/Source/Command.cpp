/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rpc/Command.hpp"

#include <algorithm>
#include <numeric>

#include <Xi/Exceptions.hpp>
#include <Xi/String/String.hpp>
#include <Xi/Serialization/Vector.hpp>

#include "Xi/Rpc/CommandError.hpp"

namespace Xi {
namespace Rpc {

const Command Command::Null{0U, "", nullptr};

Result<SharedCommand> Command::parse(const std::string &raw) {
  XI_FAIL_IF(raw.size() > Command::MaximumTextPathSize, CommandError::TextOverflow)
  auto path = split(raw, "./: ", make_copy_v);
  const auto ec = validateCommand(path);
  XI_FAIL_IF(isFailure(ec), ec)
  XI_SUCCEED(makeCommand(path))
}

Command::Binary Command::binary() const {
  return m_binary;
}

const Command::Text &Command::text() const {
  return m_text;
}

SharedConstCommand Command::prefix() const {
  return m_prefix;
}

Command::Path Command::path() const {
  Path reval{};
  reval.reserve(depth());
  path(reval);
  return reval;
}

Command::BinaryPath Command::binaryPath() const {
  BinaryPath reval{};
  reval.reserve(depth());
  path(reval);
  return reval;
}

Command::TextPath Command::textPath() const {
  TextPath reval{};
  reval.reserve(depth());
  path(reval);
  return reval;
}

size_t Command::depth() const {
  auto prefixCommand = prefix();
  if (prefixCommand) {
    return 1 + prefixCommand->depth();
  } else {
    return 1;
  }
}

std::string Command::stringify() const {
  auto path_ = textPath();
  return join(path_, ".");
}

Command::Command(Command::Binary binary_, const Command::Text &text_, SharedConstCommand prefix_)
    : m_binary{binary_}, m_text{text_}, m_prefix{prefix_} {
  /* */
}

void Command::path(Command::Path &out) const {
  auto prefixCommand = this->prefix();
  if (prefixCommand) {
    prefixCommand->path(out);
  }
  out.emplace_back(shared_from_this());
}

void Command::path(Command::BinaryPath &out) const {
  auto prefixCommand = this->prefix();
  if (prefixCommand) {
    prefixCommand->path(out);
  }
  out.emplace_back(binary());
}

void Command::path(Command::TextPath &out) const {
  auto prefixCommand = this->prefix();
  if (prefixCommand) {
    prefixCommand->path(out);
  }
  out.emplace_back(text());
}

Result<void> serialize(SharedCommand &command, const Serialization::Tag &name, Serialization::Serializer &serializer) {
  XI_EXCEPTIONAL_IF_NOT(NullArgumentError, command)
  if (serializer.isBinaryFormat()) {
    if (serializer.isInputMode()) {
      Command::BinaryPath path{};
      XI_ERROR_PROPAGATE_CATCH(serializer(path, name));
      const auto ec = validateCommand(path);
      XI_FAIL_IF(isFailure(ec), ec)
      command = makeCommand(path);
      XI_SUCCEED()
    } else if (serializer.isOutputMode()) {
      auto path = command->binaryPath();
      const auto ec = validateCommand(path);
      XI_FAIL_IF(isFailure(ec), ec)
      return serializer(path, name);
    } else {
      XI_EXCEPTIONAL(InvalidVariantTypeError)
    }
  } else if (serializer.isHumanReadableFromat()) {
    if (serializer.isInputMode()) {
      std::string rawPath{};
      XI_ERROR_PROPAGATE_CATCH(serializer(rawPath, name));
      auto maybeCommand = Command::parse(rawPath);
      XI_ERROR_PROPAGATE(maybeCommand)
      command = *maybeCommand;
      XI_SUCCEED()
    } else if (serializer.isOutputMode()) {
      auto path = command->binaryPath();
      const auto ec = validateCommand(path);
      XI_FAIL_IF(isFailure(ec), ec)
      return serializer(path, name);
    } else {
      XI_EXCEPTIONAL(InvalidVariantTypeError)
    }
  } else {
    XI_EXCEPTIONAL(InvalidVariantTypeError)
  }
}

SharedCommand makeCommand(const Command::Text &text, SharedConstCommand parent) {
  return makeCommand(Command::Null.binary(), text, parent);
}

SharedCommand makeCommand(const Command::Binary binary, SharedConstCommand parent) {
  return makeCommand(binary, Command::Null.text(), parent);
}

SharedCommand makeCommand(const Command::Binary binary, const Command::Text &text, SharedConstCommand parent) {
  return SharedCommand{new Command{binary, text, parent}};
}

SharedCommand makeCommand(Command::ConstTextSpan path) {
  if (path.empty()) {
    return nullptr;
  }
  XI_EXCEPTIONAL_IF(NullArgumentError, path.back() == Command::Null.text())
  return makeCommand(path.back(), makeCommand(path.slice(0, path.size() - 1)));
}

SharedCommand makeCommand(Command::ConstBinarySpan path) {
  if (path.empty()) {
    return nullptr;
  }
  XI_EXCEPTIONAL_IF(NullArgumentError, path.back() == Command::Null.binary())
  return makeCommand(path.back(), makeCommand(path.slice(0, path.size() - 1)));
}

CommandError validateCommand(Command::ConstTextSpan path) {
  XI_RETURN_EC_IF(path.empty(), CommandError::Null);
  XI_RETURN_EC_IF(path.size() > Command::MaximumDepth, CommandError::Overflow);
  size_t acc = path.size();
  for (const auto &entry : path) {
    XI_RETURN_EC_IF(entry == Command::Null.text(), CommandError::TextNull);
    XI_RETURN_EC_IF(entry.size() > Command::MaximumTextTag, CommandError::TextOverflow);
    acc += entry.size();
    XI_RETURN_EC_IF(acc > Command::MaximumTextPathSize, CommandError::TextOverflow);
  }
  XI_RETURN_SC(CommandError::Success);
}

CommandError validateCommand(Command::ConstBinarySpan path) {
  XI_RETURN_EC_IF(path.empty(), CommandError::Null);
  XI_RETURN_EC_IF(path.size() > Command::MaximumDepth, CommandError::Overflow);
  XI_RETURN_EC_IF(std::any_of(path.begin(), path.end(), [](const auto i) { return i == Command::Null.binary(); }),
                  CommandError::BinaryNull);
  XI_RETURN_EC_IF(std::any_of(path.begin(), path.end(), [](const auto i) { return i > Command::MaximumBinaryTag; }),
                  CommandError::BinaryOverflow);
  XI_RETURN_SC(CommandError::Success);
}

Result<void> parse(const std::string &str, SharedCommand &command) {
  auto reval = Command::parse(str);
  XI_ERROR_PROPAGATE(reval)
  command = reval.take();
  XI_SUCCEED()
}

}  // namespace Rpc
}  // namespace Xi
