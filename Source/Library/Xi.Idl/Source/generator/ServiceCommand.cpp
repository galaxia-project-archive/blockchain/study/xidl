/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/ServiceCommand.hpp"

#include <Xi/Exceptions.hpp>

#include "Xi/Idl/Generator/Service.hpp"
#include "Xi/Idl/Generator/Contract.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

struct ServiceCommand::_StorageConcept {
  virtual ~_StorageConcept() = default;

  virtual const std::type_info& type() const = 0;
  virtual const void* data() const = 0;
  virtual std::unique_ptr<_StorageConcept> clone() const = 0;
};

template <typename _DataT>
struct ServiceCommand::_Storage final : ServiceCommand::_StorageConcept {
  _DataT dataStorage;

  _Storage(const _DataT& data) : dataStorage{data} {
    /* */
  }

  const std::type_info& type() const {
    return typeid(dataStorage);
  }

  const void* data() const {
    return std::addressof(dataStorage);
  }

  std::unique_ptr<_StorageConcept> clone() const {
    return std::make_unique<_Storage<_DataT>>(dataStorage);
  }
};

ServiceCommand::ServiceCommand(const ServiceCommand& other)
    : m_tag{other.tag()}, m_doc{other.m_doc}, m_kind{other.kind()}, m_data{nullptr} {
  if (other.m_data) {
    m_data = other.m_data->clone();
  }
}

ServiceCommand& ServiceCommand::operator=(const ServiceCommand& other) {
  setTag(other.tag());
  setDocumentation(other.documentation());
  setKind(other.kind());
  if (other.m_data) {
    m_data = other.m_data->clone();
  }
  return *this;
}

ServiceCommand::ServiceCommand(ServiceCommand&& other)
    : m_tag{other.tag()}, m_doc{std::move(other.m_doc)}, m_kind{other.kind()}, m_data{std::move(other.m_data)} {
  /* */
}

ServiceCommand& ServiceCommand::operator=(ServiceCommand&& other) {
  setTag(other.tag());
  setDocumentation(other.documentation());
  setKind(other.kind());
  m_data = std::move(other.m_data);
  return *this;
}

ServiceCommand::~ServiceCommand() {
  /* */
}

const SerializationTag& ServiceCommand::tag() const {
  return m_tag;
}

const Documentation& ServiceCommand::documentation() const {
  return m_doc;
}

ServiceCommand::Kind ServiceCommand::kind() const {
  return m_kind;
}

bool ServiceCommand::isService() const {
  return kind() == Kind::Service;
}

bool ServiceCommand::isContract() const {
  return kind() == Kind::Contract;
}

const Service& ServiceCommand::asService() const {
  XI_EXCEPTIONAL_IF_NOT(RuntimeError, m_data)
  XI_EXCEPTIONAL_IF_NOT(InvalidVariantTypeError, m_data)
  return *static_cast<const Service*>(m_data->data());
}

const Contract& ServiceCommand::asContract() const {
  XI_EXCEPTIONAL_IF_NOT(RuntimeError, m_data)
  XI_EXCEPTIONAL_IF_NOT(InvalidVariantTypeError, m_data)
  return *static_cast<const Contract*>(m_data->data());
}

ServiceCommand::ServiceCommand() {
  /* */
}

void ServiceCommand::setTag(const SerializationTag& tag_) {
  m_tag = tag_;
}

void ServiceCommand::setDocumentation(const Documentation& doc) {
  m_doc = doc;
}

void ServiceCommand::setKind(const ServiceCommand::Kind kind_) {
  m_kind = kind_;
}

void ServiceCommand::setData(const Service& service) {
  m_data = std::make_unique<_Storage<Service>>(service);
}

void ServiceCommand::setData(const Contract& contract) {
  m_data = std::make_unique<_Storage<Contract>>(contract);
}

ServiceCommandBuilder::ServiceCommandBuilder() : m_command{result_success, ServiceCommand{/* */}} {
  /* */
}

ServiceCommandBuilder& ServiceCommandBuilder::withTag(const SerializationTag& tag) {
  if (!m_command.isError()) {
    m_command->setTag(tag);
  }
  return *this;
}

ServiceCommandBuilder& ServiceCommandBuilder::withDocumentation(const Documentation& doc_) {
  if (!m_command.isError()) {
    m_command->setDocumentation(doc_);
  }
  return *this;
}

ServiceCommandBuilder& ServiceCommandBuilder::withKind(const ServiceCommand::Kind kind_) {
  if (!m_command.isError()) {
    m_command->setKind(kind_);
  }
  return *this;
}

ServiceCommandBuilder& ServiceCommandBuilder::withData(const Service& service) {
  if (!m_command.isError()) {
    m_command->setData(service);
  }
  return *this;
}

ServiceCommandBuilder& ServiceCommandBuilder::withData(const Contract& contract) {
  if (!m_command.isError()) {
    m_command->setData(contract);
  }
  return *this;
}

Result<ServiceCommand> ServiceCommandBuilder::build() {
  return m_command;
}

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi
