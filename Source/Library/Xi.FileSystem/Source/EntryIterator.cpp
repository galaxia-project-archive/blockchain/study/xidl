/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/FileSystem/EntryIterator.hpp"

namespace Xi {
namespace FileSystem {

EntryIterator::value_type EntryIterator::operator*() const {
  return Entry{*m_iter};
}

EntryIterator &EntryIterator::operator++() {
  m_iter++;
  return *this;
}

bool EntryIterator::operator==(const EntryIterator &rhs) const {
  return m_iter == rhs.m_iter;
}

bool EntryIterator::operator!=(const EntryIterator &rhs) const {
  return m_iter != rhs.m_iter;
}

EntryIterator::EntryIterator(std::filesystem::directory_iterator iter) : m_iter{iter} {
  /* */
}

}  // namespace FileSystem
}  // namespace Xi
