/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "SpdLogger.hpp"

#include <Xi/Extern/Push.hh>
#include <spdlog/sinks/sink.h>
#include <Xi/Extern/Pop.hh>

#include <Xi/Exceptions.hpp>

namespace Xi {
namespace Log {

SpdLogger::SpdLogger(spdlog::sink_ptr sink) : m_sink{sink} {
  XI_EXCEPTIONAL_IF_NOT(NullArgumentError, sink)
}

bool SpdLogger::isFiltered(const Level level) const {
  XI_UNUSED(level)
  return false;
}

void SpdLogger::print(Context context, std::string_view message) {
  const std::string* categoryName = nullptr;
  const auto category = context.category().lock();
  if (category) {
    categoryName = std::addressof(category->name());
  }

  try {
    m_sink->log(spdlog::details::log_msg(categoryName, toSpdLog(context.level()), message));
  } catch (...) {
    /* swallow */
  }
}

spdlog::level::level_enum toSpdLog(const Level level) {
  using spdlog::level::level_enum;

  switch (level) {
    case Level::None:
      return level_enum::off;
    case Level::Fatal:
      return level_enum::critical;
    case Level::Error:
      return level_enum::err;
    case Level::Warn:
      return level_enum::warn;
    case Level::Info:
      return level_enum::info;
    case Level::Debug:
      return level_enum::debug;
    case Level::Trace:
      return level_enum::trace;
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

}  // namespace Log
}  // namespace Xi
