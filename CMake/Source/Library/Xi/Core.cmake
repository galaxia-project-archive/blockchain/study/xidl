# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_LIBRARY_XI)
  return()
endif()
set(CMAKE_XI_LIBRARY_XI TRUE)

xi_include(Xi/Make/Library)

xi_include(Package/Leathers)
xi_include(Package/Boost)
xi_include(Package/Boost/FileSystem)
xi_include(Package/Boost/Thread)
xi_include(Package/Fmt)
xi_include(Source/Library/Legacy/System)

xi_make_library(
  Xi.Core

  PUBLIC_LIBRARIES
    Leathers::leathers
    fmt::fmt
    Boost::boost
    Boost::filesystem
    Boost::thread

    # Only required for the system dispatcher
    Legacy::System
)
