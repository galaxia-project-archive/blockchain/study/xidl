/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Parser/TypeDeclaration.hpp"

#include <set>

#include <Xi/Log/Log.hpp>

#include "Xi/Idl/Parser/ParserError.hpp"

XI_LOGGER("Idl/Parser")

namespace Xi {
namespace Idl {
namespace Parser {

namespace {
static const std::set<std::string> TypeDeclarationKeywords{{
    "package",
    "variant",
    "enum",
    "flag",
    "alias",
    "typedef",
}};
}

Result<void> TypeDeclarationValueAutomaton::doProcessToken(const Token& token) {
  SharedAutomaton typeAutomaton{nullptr};
  if (token.value == "package") {
    m_result = Package{};
    typeAutomaton = makePackageAutomaton(std::get<Package>(m_result));
  } else if (token.value == "variant") {
    m_result = Variant{};
    typeAutomaton = makeVariantAutomaton(std::get<Variant>(m_result));
  } else if (token.value == "enum") {
    m_result = Enum{};
    typeAutomaton = makeEnumAutomaton(std::get<Enum>(m_result));
  } else if (token.value == "flag") {
    m_result = Flag{};
    typeAutomaton = makeFlagAutomaton(std::get<Flag>(m_result));
  } else if (token.value == "alias") {
    m_result = Alias{};
    typeAutomaton = makeAliasAutomaton(std::get<Alias>(m_result));
  } else if (token.value == "typedef") {
    m_result = Typedef{};
    typeAutomaton = makeTypedefAutomaton(std::get<Typedef>(m_result));
  } else {
    XI_ERROR("Unknown type declaration '{}' at {}", token.value, token.begin)
    XI_FAIL(ParserError::UnexpectedToken)
  }
  chain(typeAutomaton);
  markFinished();
  return typeAutomaton->processToken(token);
}

std::shared_ptr<TypeDeclarationValueAutomaton> makeTypeDeclarationValueAutomaton(TypeDeclarationValue& result) {
  std::shared_ptr<TypeDeclarationValueAutomaton> reval{new TypeDeclarationValueAutomaton{result}};
  return reval;
}

Result<void> TypeDeclarationsAutomaton::doProcessToken(const Token& token) {
  auto search = TypeDeclarationKeywords.find(token.value);
  if (search != TypeDeclarationKeywords.end()) {
    m_result.emplace_back();
    emplaceDocumentation(m_result.back().doc);
    auto inner = makeTypeDeclarationValueAutomaton(m_result.back().value);
    chain(inner);
    chainDocumentation();
    return inner->processToken(token);
  } else {
    return propagateBack(token);
  }
}

std::shared_ptr<TypeDeclarationsAutomaton> makeTypeDeclarationsAutomaton(TypeDeclarationVector& result) {
  std::shared_ptr<TypeDeclarationsAutomaton> reval{new TypeDeclarationsAutomaton{result}};
  reval->chainDocumentation();
  return reval;
}

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi
