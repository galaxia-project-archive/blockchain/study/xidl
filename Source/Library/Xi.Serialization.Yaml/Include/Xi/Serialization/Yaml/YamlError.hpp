/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/ErrorCode.hpp>

namespace Xi {
namespace Serialization {
namespace Yaml {

XI_ERROR_CODE_BEGIN(Yaml)
XI_ERROR_CODE_VALUE(NoValue, 0x0001)
XI_ERROR_CODE_VALUE(Internal, 0x0002)

XI_ERROR_CODE_VALUE(TypeMissmatchScalar, 0x0010)
XI_ERROR_CODE_VALUE(TypeMissmatchBoolean, 0x0011)
XI_ERROR_CODE_VALUE(TypeMissmatchFloating, 0x0012)
XI_ERROR_CODE_VALUE(TypeMissmatchContainer, 0x0013)
XI_ERROR_CODE_VALUE(TypeMissmatchObject, 0x0014)
XI_ERROR_CODE_VALUE(TypeMissmatchArray, 0x0015)

XI_ERROR_CODE_VALUE(ScalarOutOfBoundary, 0x0020)
XI_ERROR_CODE_VALUE(FlagOverflow, 0x0021)
XI_ERROR_CODE_VALUE(SizeMissmatch, 0x0022)

XI_ERROR_CODE_VALUE(NullTag, 0x0030)
XI_ERROR_CODE_VALUE(DuplicateTag, 0x0031)
XI_ERROR_CODE_END(Yaml, "Serialization::YamlError")

}  // namespace Yaml
}  // namespace Serialization
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Serialization::Yaml, Yaml)
