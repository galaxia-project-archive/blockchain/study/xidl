/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/FileSystem/SymbolicLink.hpp"

#include "Xi/FileSystem/SymbolicLinkError.hpp"

namespace fs = std::filesystem;

namespace Xi {
namespace FileSystem {

Result<fs::path> resolveSymbolicLink(const fs::path &path, size_t redirectionsThreshold) {
  std::error_code ec{/* */};
  const auto exists = fs::exists(path, ec);
  XI_ERROR_CODE_PROPAGATE(ec);
  XI_SUCCEED_IF_NOT(exists, path)
  const auto isSymlink = fs::is_symlink(path, ec);
  XI_ERROR_CODE_PROPAGATE(ec);
  XI_SUCCEED_IF_NOT(isSymlink, path)
  XI_FAIL_IF(redirectionsThreshold == 0, SymbolicLinkError::TooManyRedirections)
  auto redirect = fs::read_symlink(path, ec);
  XI_ERROR_CODE_PROPAGATE(ec);
  return resolveSymbolicLink(redirect, redirectionsThreshold - 1);
}

}  // namespace FileSystem
}  // namespace Xi
