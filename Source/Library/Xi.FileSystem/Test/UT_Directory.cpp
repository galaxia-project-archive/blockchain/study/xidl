/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/Testing/Result.hpp>
#include <Xi/FileSystem/File.hpp>
#include <Xi/FileSystem/Directory.hpp>

#define XI_TEST_SUITE Xi_FileSystem_Directory

TEST(XI_TEST_SUITE, CreateTemporary) {
  using namespace Xi::Testing;
  using namespace Xi::FileSystem;

  auto tempDir = makeDirectory(SpecialDirectory::Temporary);
  ASSERT_THAT(tempDir, IsSuccess());

  auto tempName = makeTemporaryFilename();
  ASSERT_THAT(tempName, IsSuccess());

  auto dir = tempDir->relativeDirectory(*tempName);
  ASSERT_THAT(dir, IsSuccess());
}
