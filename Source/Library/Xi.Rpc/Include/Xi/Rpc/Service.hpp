/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <memory>
#include <unordered_map>
#include <unordered_set>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>
#include <Xi/Serialization/InputSerializer.hpp>

#include "Xi/Rpc/Command.hpp"
#include "Xi/Rpc/Contract.hpp"
#include "Xi/Rpc/Request.hpp"
#include "Xi/Rpc/Response.hpp"

namespace Xi {
namespace Rpc {

XI_DECLARE_SMART_POINTER_CLASS(Service)

/*!
 * \brief The Service class stores a graph of services and contracts identified by commands.
 *
 * In order to resolve a command name it is required to query the underyling contract. With nested commands
 * a specifc command identifer may point to another service as well.
 */
class Service final : public std::enable_shared_from_this<Service> {
 public:
  XI_DELETE_COPY(Service);
  XI_DELETE_MOVE(Service);
  ~Service() = default;

 public:
  /// Returns all commands currently recognized by this service.
  const std::unordered_set<SharedConstCommand>& commands() const;

  Result<void> addService(SharedConstCommand command, SharedService service);
  Result<void> addContract(SharedConstCommand command, SharedContract contract);

  [[nodiscard]] SharedContract queryContract(SharedConstCommand command) const;
  [[nodiscard]] SharedContract queryContract(Command::PathSpan path) const;
  [[nodiscard]] SharedContract queryContract(Command::Binary binary) const;
  [[nodiscard]] SharedContract queryContract(Command::ConstBinarySpan binary) const;
  [[nodiscard]] SharedContract queryContract(const Command::Text& text) const;
  [[nodiscard]] SharedContract queryContract(Command::ConstTextSpan text) const;

  [[nodiscard]] SharedService queryService(SharedConstCommand command) const;
  [[nodiscard]] SharedService queryService(Command::Binary binary) const;
  [[nodiscard]] SharedService queryService(Command::ConstBinarySpan binary) const;
  [[nodiscard]] SharedService queryService(const Command::Text& text) const;
  [[nodiscard]] SharedService queryService(Command::ConstTextSpan text) const;

 private:
  Service() = default;

  friend SharedService makeService();

 private:
  std::unordered_set<SharedConstCommand> m_commands;

  std::unordered_map<Command::Binary, SharedService> m_serviceByBinary;
  std::unordered_map<Command::Text, SharedService> m_serviceByText;

  std::unordered_map<Command::Binary, SharedContract> m_contractByBinary;
  std::unordered_map<Command::Text, SharedContract> m_contractByText;
};

SharedService makeService();

}  // namespace Rpc
}  // namespace Xi
