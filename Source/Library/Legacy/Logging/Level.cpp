﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Logging/Level.h"

#include <string>
#include <cassert>

#include <Xi/Global.hh>
#include <Xi/Exceptions.hpp>
#include <Xi/String/String.hpp>

XI_ERROR_CODE_CATEGORY_BEGIN(Logging, Level)
XI_ERROR_CODE_DESC(Unknown, "unknown logging level")
XI_ERROR_CODE_CATEGORY_END()

namespace Logging {

std::string stringify(Level level) {
  switch (level) {
    case Level::None:
      return std::string{"none"};
    case Level::Fatal:
      return std::string{"fatal"};
    case Level::Error:
      return std::string{"error"};
    case Level::Warning:
      return std::string{"warning"};
    case Level::Info:
      return std::string{"info"};
    case Level::Debugging:
      return std::string{"debugging"};
    case Level::Trace:
      return std::string{"trace"};
  }
  XI_EXCEPTIONAL(Xi::InvalidEnumValueError)
}

Xi::Result<void> parse(const std::string &str, Logging::Level &out) {
  using namespace ::Xi;
  auto lower = toLower(str);
  if (lower == toLower(toString(Level::None))) {
    out = Level::None;
  } else if (lower == toLower(toString(Level::Fatal))) {
    out = Level::Fatal;
  } else if (lower == toLower(toString(Level::Error))) {
    out = Level::Error;
  } else if (lower == toLower(toString(Level::Warning))) {
    out = Level::Warning;
  } else if (lower == toLower(toString(Level::Info))) {
    out = Level::Info;
  } else if (lower == toLower(toString(Level::Debugging))) {
    out = Level::Debugging;
  } else if (lower == toLower(toString(Level::Trace))) {
    out = Level::Trace;
  } else {
    XI_FAIL(LevelError::Unknown)
  }
  XI_SUCCEED()
}

}  // namespace Logging
