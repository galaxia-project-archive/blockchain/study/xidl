/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <map>

#include <Xi/Result.hpp>

#include "Xi/Idl/Generator/TypeCollection.hpp"
#include "Xi/Idl/Generator/ContractCollection.hpp"
#include "Xi/Idl/Generator/ServiceCollection.hpp"
#include "Xi/Idl/Generator/NamespaceResolver.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

class Scope {
 public:
  static const Scope EmptyScope;

 public:
  explicit Scope() = default;
  XI_DEFAULT_COPY(Scope);
  XI_DEFAULT_MOVE(Scope);
  ~Scope() = default;

  SharedConstNamespace currentNamespace() const;
  const NamespaceResolver namespaces() const;
  NamespaceResolver& namespaces();

  const TypeCollection& types(SharedConstNamespace ns) const;
  const TypeCollection& types() const;
  TypeCollection& types(SharedConstNamespace ns);
  TypeCollection& types();

  const ContractCollection& contracts(SharedConstNamespace ns) const;
  const ContractCollection& contracts() const;
  ContractCollection& contracts(SharedConstNamespace ns);
  ContractCollection& contracts();

  const ServiceCollection& services(SharedConstNamespace ns) const;
  const ServiceCollection& services() const;
  ServiceCollection& services(SharedConstNamespace ns);
  ServiceCollection& services();

  Result<void> import(const Scope& other);

 private:
  mutable std::map<SharedConstNamespace, TypeCollection> m_types;
  mutable std::map<SharedConstNamespace, ContractCollection> m_contracts;
  mutable std::map<SharedConstNamespace, ServiceCollection> m_services;
  NamespaceResolver m_namespaces;
};

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi
