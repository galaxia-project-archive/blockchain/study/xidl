/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rpc/ContractPayload.hpp"

namespace Xi {
namespace Rpc {

ContractPayload::ContractPayload() : m_storage{nullptr} {
  /* */
}

Serialization::Object ContractPayload::object() {
  if (m_storage) {
    return m_storage->object();
  } else {
    return Serialization::Object{/* */};
  }
}

template <>
ContractPayload makeContractPayload<void>() {
  return ContractPayload{/* */};
}

Result<void> serialize(ContractPayload &value, const Serialization::Tag &name, Serialization::Serializer &serializer) {
  auto object = value.object();
  return serializer(object, name);
}

}  // namespace Rpc
}  // namespace Xi
