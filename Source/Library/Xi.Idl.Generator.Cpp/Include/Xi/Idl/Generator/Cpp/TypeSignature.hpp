/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string>
#include <vector>
#include <variant>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>
#include <Xi/Idl/Generator/Type.hpp>

#include "Xi/Idl/Generator/Cpp/Constant.hpp"

namespace Xi {
namespace Idl {
namespace Generator {
namespace Cpp {

class TypeSignature {
 public:
  enum struct Decorator {
    None,
    Const,
    Reference,
    ConstReference,
    Pointer,
    ConstPointer,
  };

  using GenericArgument = std::variant<TypeSignature, Constant>;
  using GenericContainer = std::vector<GenericArgument>;

 public:
  XI_DEFAULT_COPY(TypeSignature);
  XI_DEFAULT_MOVE(TypeSignature);

  /// maybe nullptr, if no namespace is required
  SharedConstNamespace namespace_() const;
  const std::string& name() const;
  Decorator decorator() const;
  const GenericContainer& genericArguments() const;

  TypeSignature decorate(Decorator dec) const;
  TypeSignature toOptional() const;
  TypeSignature toArray(std::size_t size) const;
  TypeSignature toVector() const;

 private:
  explicit TypeSignature(SharedConstNamespace ns, const std::string& name_);

  TypeSignature toOptional(bool isOptional);

  friend TypeSignature makeTypeSignature(const Type&);

 private:
  SharedConstNamespace m_namespace;
  std::string m_name;
  Decorator m_decorator;
  std::vector<GenericArgument> m_genericArguments;
};
using TypeSignatureVector = std::vector<TypeSignature>;

TypeSignature makeTypeSignature(const Type& type);

}  // namespace Cpp
}  // namespace Generator
}  // namespace Idl
}  // namespace Xi
